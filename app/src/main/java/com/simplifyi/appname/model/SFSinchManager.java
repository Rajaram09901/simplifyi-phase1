package com.simplifyi.appname.model;

import android.Manifest;
import android.content.Context;

import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.utility.SFDynamicPermissionManager;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;
import com.sinch.android.rtc.AudioController;
import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallClient;
import com.sinch.android.rtc.calling.CallClientListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SFSinchManager {
    public static final String APP_KEY = "b3e96712-5ff9-4e21-9b1b-561d51331933";
    public static final String APP_SECRET = "9YZeS6yLzkKlkI4ATsogAA==";
    public static final String ENVIRONMENT = "sandbox.sinch.com";
    private static SFSinchManager sInstance;
    private SinchClient mSinchClient;
    private Context mAppContext;
    private Call mCurrentCall;
    private List<SFIncomingCallListener> mSFIncomingCallListenerQueue;

    //help:https://www.sinch.com/docs/voice/android/index.html
    private SFSinchManager() {
    }

    public static synchronized SFSinchManager getInstance() {
        if (sInstance == null) {
            sInstance = new SFSinchManager();
        }
        return sInstance;
    }

    public boolean switchOnSinch(Context context, boolean enableIncoming, boolean enableOutgoing) {
        mAppContext = context;
        List<String> permissionsToBeGranted = getPermissionsThatAreNotGrantedYet();
        if (!permissionsToBeGranted.isEmpty()) {
            for (String permission : permissionsToBeGranted) {
                SFLogger.debugLog("switchOnSinch()", "Missing permission:" + permission);
            }
            showToast("switchOnSinch():Missing permissions.check log for details..");
            return false;

        }
        String currentUser = SFSharedPrefs.getInstance(mAppContext).getUser().getNick();
        if (SFUtility.isStringNullOrEmpty(currentUser)) {
            showToast("switchOnSinch(): current username/caller id is null or empty");
            return false;
        }

        mSinchClient = Sinch.getSinchClientBuilder()
                .context(mAppContext)
                .userId(currentUser)
                .applicationKey(SFSinchManager.APP_KEY)
                .applicationSecret(SFSinchManager.APP_SECRET)
                .environmentHost(SFSinchManager.ENVIRONMENT)
                .build();

        mSinchClient.setSupportCalling(true);//supports calling. other types are messaging,Managed push etc..
        mSinchClient.setSupportActiveConnectionInBackground(true);
        if (enableIncoming) {
            mSinchClient.startListeningOnActiveConnection();// pull mode, not push mode.call is notified via pull. there is push as well.
            mSinchClient.getCallClient().addCallClientListener(new SinchCallClientListener());
        }
        mSinchClient.start();
        return true;
    }

    public boolean wasSinchSwitchedOn() {
        return mSinchClient != null && mSinchClient.isStarted();
    }

    public void shutDownSinch() {
        if (!wasSinchSwitchedOn()) {
            // showToast("shutDownSinch(): either sinch client is null or not started");
            return;
        }
        mSinchClient.stopListeningOnActiveConnection();
        mSinchClient.terminate();
    }


    public boolean callUser(String recipientId) {
        if (SFUtility.isStringNullOrEmpty(recipientId)) {
            showToast("callUser(): recipientId is null or empty");
            return false;
        }
        SFUser currentUser = SFSharedPrefs.getInstance(mAppContext).getUser();
        if (SFUtility.isStringNullOrEmpty(currentUser.getNick())) {
            showToast("callUser(): current user is null or empty");
            return false;
        }
        if (currentUser.getNick().equals(recipientId)) {
            showToast("callUser(): recipientId and caller id are same");
            return false;
        }
        if (!wasSinchSwitchedOn()) {
            showToast("callUser(): Switch on the Sinch before attempting to make any call");
            return false;
        }
        //TODO: pass the caller info to callee. The header size should be less than 1024 bytes. if not throws exception..

        HashMap<String, String> callerInfo = new HashMap<>();
        if (!SFUtility.isStringNullOrEmpty(currentUser.getId()))
            callerInfo.put(SFCall.SF_CALLER_USER_ID, currentUser.getId());
        SFLogger.debugLog("CALLER", "The caller id I am passing =" + callerInfo.get(SFCall.SF_CALLER_USER_ID));
        mCurrentCall = mSinchClient.getCallClient().callUser(recipientId, callerInfo);
        return (mCurrentCall != null);
    }

    public void enableOutGoingCall(Context context) {
        if (!wasSinchSwitchedOn()) {
            switchOnSinch(context.getApplicationContext(), false, true);
        }
    }

    public void disableOutGoingCall() {
        if (wasSinchSwitchedOn()) {
            shutDownSinch();
        }
    }

    public void answerIncomingCall() {
        if (mCurrentCall == null) {
            showToast("answerIncomingCall(): Call object is null");
            return;
        }
        mCurrentCall.answer();

    }

    public Call getCurrentCall() {
        return mCurrentCall;
    }

    public AudioController getAudioController() {
        if (mSinchClient != null && mSinchClient.isStarted() && mCurrentCall != null) {
            return mSinchClient.getAudioController();
        }
        return null;
    }

    /**
     * cut the call. this can be called by either caller  or callee
     */
    public void hangupCall() {
        if (mCurrentCall == null) {
            showToast("hangupCall(): Call object is null");
            return;
        }
        mCurrentCall.hangup();
    }

    /**
     * Notify the incoming call so that the callee can launch the  {@link com.simplifyi.appname.ui.activity.SFInCallActivity} and automatically answer the call there.
     * Any Activity can implement this interface and register with {@link SFSinchManager} to receive incoming call alert.
     */
    private class SinchCallClientListener implements CallClientListener {
        @Override
        public void onIncomingCall(CallClient callClient, final Call incomingCall) {
            mCurrentCall = incomingCall;
            if (mSFIncomingCallListenerQueue != null && !mSFIncomingCallListenerQueue.isEmpty()) {
                for (SFIncomingCallListener notifier : mSFIncomingCallListenerQueue) {
                    notifier.onIncomingCall();

                }
            }
        }
    }

    /**
     * check if any permission is left out from granting. if so, list all the permissions that are not granted yet and return;
     *
     * @return list of permissions that are not granted yet
     */
    private List<String> getPermissionsThatAreNotGrantedYet() {
        String[] permissions = new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.RECORD_AUDIO,
                Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.MODIFY_AUDIO_SETTINGS};
        List<String> needPermissions = new ArrayList();
        int i = 0;
        for (String permission : permissions) {
            if (!SFDynamicPermissionManager.wasThePermissionGranted(mAppContext, permission)) {
                needPermissions.add(permission);
            }
        }
        return needPermissions;
    }

    /**
     * If any class is interested in listening for incoming calls, it has to implement {@link SFIncomingCallListener} interface and register with the {@link SFSinchManager}
     * this method checks if the Sinch manager has been already started and running before registering for incoming call alert.it calls {@link SFSinchManager#wasSinchSwitchedOn()} to know
     * if the sinch was already started. if sinch is not started already, it starts it by calling {@link SFSinchManager#switchOnSinch(Context, boolean, boolean)}
     *
     * @param notifier a incoming call notified that the caller has to implement and pass it as argument to register for incoming call alert.
     */
    public void registerForIncomingCallNotification(SFIncomingCallListener notifier, Context context) {
        if (notifier == null || context == null) {
            showToast("Registering for incoming calls failed.");
            return;
        }
        if (!wasSinchSwitchedOn()) {
            switchOnSinch(context.getApplicationContext(), true, false);
        }
        if (mSFIncomingCallListenerQueue == null) {
            mSFIncomingCallListenerQueue = new ArrayList<>();
        }
        mSFIncomingCallListenerQueue.add(notifier);
    }

    /**
     * remove the incoming call listener from the listener queue. if the queue is empty after removing this listener, shut down sinch
     *
     * @param listener incoming call listener to be removed
     */
    public void unRegisterForIncomingCallNotification(SFIncomingCallListener listener) {
        if (listener != null && mSFIncomingCallListenerQueue != null) {
            mSFIncomingCallListenerQueue.remove(listener);
        }
        if (mSFIncomingCallListenerQueue != null && mSFIncomingCallListenerQueue.isEmpty() && wasSinchSwitchedOn()) {
            shutDownSinch();
        }


    }

    public interface SFIncomingCallListener {
        void onIncomingCall();
    }

    /**
     * TODO:Remove this method & all the calls to this method once the call functionality is thoroughly tested
     */
    private void showToast(String message) {
        SFLogger.showToast(mAppContext, message);
    }
}
