package com.simplifyi.appname.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SFForum implements Serializable {
    @SerializedName("_id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("createdBy")
    private String createdBy;
    @SerializedName("dateCreated")
    private String dateCreated;
    @SerializedName("queue")
    private boolean queue;

    @SerializedName("users")
    private List<ForumUser> forumUsers;
    @SerializedName("answers")
    private List<Answer> answers;
    @SerializedName("follows")
    private int follows;
    @SerializedName("replies")
    private int replies;
    @SerializedName("tags")
    private List<String> tags;

    public void setForumImageUrl(String forumImageUrl) {
        this.forumImageUrl = forumImageUrl;
    }

    //TODO:dummy url for fourm image
    @SerializedName("image")
    private String forumImageUrl;

    //TODO: delete this patch later. this is to avoid a mistake done at server side: some unknown text is sent inside data[] when callable forum is empty
    @SerializedName("results")
    public String results;


    public SFForum() {

    }

    public String getForumImageUrl() {
        return forumImageUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public boolean isQueue() {
        return queue;
    }

    public void setQueue(boolean queue) {
        this.queue = queue;
    }

    public List<ForumUser> getForumUsers() {
        return forumUsers;
    }

    public void setForumUsers(List<ForumUser> forumUsers) {
        this.forumUsers = forumUsers;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public int getFollows() {
        return follows;
    }

    public void setFollows(int follows) {
        this.follows = follows;
    }

    public int getReplies() {
        return replies;
    }

    public void setReplies(int replies) {
        this.replies = replies;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public static class ForumUser implements Serializable {
        @SerializedName("userId")
        private String userId;
        @SerializedName("nick")
        private String nickName;
        @SerializedName("mock_response/call")
        private boolean call;
        @SerializedName("date")
        private String date;
        @SerializedName("_id")
        private String id;
        @SerializedName("status")
        private String callStatus;
        @SerializedName("image")
        private String profileImageUrl;
        @SerializedName("receivedTime")
        private String receivedTime;
        @SerializedName("receivedBy")
        private String receivedBy;
        @SerializedName("endTime")
        private String endTime;

        public String getId() {
            return id;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public boolean isCall() {
            return call;
        }

        public String getDate() {
            return date;
        }

        public String getProfileImageUrl() {
            return profileImageUrl;
        }

        public void setProfileImageUrl(String profileImageUrl) {
            this.profileImageUrl = profileImageUrl;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getCallStatus() {
            return callStatus;
        }

        public void setCallStatus(String callStatus) {
            this.callStatus = callStatus;
        }


    }

    public static class Answer implements Serializable {
        @SerializedName("user")
        private ForumUser userDetails;
        @SerializedName("text")
        private String answer;
        @SerializedName("date")
        private String date;

        public ForumUser getUserDetails() {
            return userDetails;
        }

        public void setUserDetails(ForumUser userDetails) {
            this.userDetails = userDetails;
        }

        public String getAnswer() {
            return answer;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public void setAnswer(String answer) {
            this.answer = answer;
        }
    }

    @Override
    public boolean equals(Object obj) {
        SFForum forum = (SFForum) obj;
        if (forum == null) return false;
        if (forum.id == null || forum.id.isEmpty()) return false;
        if (this.id == null || this.id.isEmpty()) return false;
        return this.id.equals(forum.id);
    }
}