package com.simplifyi.appname.ui.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

public class SFLocationActivity extends SFBaseActivity implements OnMapReadyCallback, PlaceSelectionListener {

    public static final String KEY_LATLONG = "key_latlong";
    public static final String KEY_ADDRESS = "key_address";
    private GoogleMap googleMap;
    private TextView textLatLong;
    private TextView textAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_location);

        textLatLong = findViewById(R.id.text_latlong);
        textAddress = findViewById(R.id.text_address);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.maps);
        mapFragment.getMapAsync(this);

        // Retrieve the PlaceAutocompleteFragment.
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        // Register a listener to receive callbacks when a place has been selected or an error has
        // occurred.
        autocompleteFragment.setOnPlaceSelectedListener(this);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.getUiSettings().setZoomControlsEnabled(true);
        googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng target = googleMap.getCameraPosition().target;
                double targetLat = target.latitude, targetLong = target.longitude;
                if (target.latitude < 0.5) {
                    targetLat = 0.0;
                }
                if (target.longitude < 0.5) {
                    targetLong = 0.0;
                }
             //   googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(target, calculateZoomLevel()));
                String addressString = SFUtility.getCompleteAddressString(SFLocationActivity.this, targetLat, targetLong);
                textLatLong.setText(targetLat + ", " + targetLong);
                textAddress.setText(addressString);
            }
        });
    }


    private int calculateZoomLevel() {
        double equatorLength = 10075004; // in meters
        Display display = this.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        double metersPerPixel = equatorLength / 2000;
        int zoomLevel = 1;
        while ((metersPerPixel * width) > 2000) {
            metersPerPixel /= 2;
            ++zoomLevel;
        }
        return zoomLevel;
    }

    public void onSelectLocation(View view) {
        Intent intent = new Intent();
        intent.putExtra(KEY_ADDRESS, textAddress.getText().toString());
        intent.putExtra(KEY_LATLONG, textLatLong.getText().toString());
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onPlaceSelected(Place place) {
        // Format the returned place's details and display them in the TextView.
        textAddress.setText(formatPlaceDetails(getResources(), place.getName(), place.getId(),
                place.getAddress(), place.getPhoneNumber(), place.getWebsiteUri()));
        CharSequence attributions = place.getAttributions();
//        if (!TextUtils.isEmpty(attributions)) {
//            mPlaceAttribution.setText(Html.fromHtml(attributions.toString()));
//        } else {
//            mPlaceAttribution.setText("");
//        }
    }

    /**
     * Callback invoked when PlaceAutocompleteFragment encounters an error.
     */
    @Override
    public void onError(Status status) {
    }

    /**
     * Helper method to format information about a place nicely.
     */
    private static Spanned formatPlaceDetails(Resources res, CharSequence name, String id,
                                              CharSequence address, CharSequence phoneNumber, Uri websiteUri) {
        return Html.fromHtml(res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));

    }
}
