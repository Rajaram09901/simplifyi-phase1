package com.simplifyi.appname.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.simplifyi.appname.ui.fragment.SFReceivedFragment;
import com.simplifyi.appname.ui.fragment.SFDialedFragment;
import com.simplifyi.appname.ui.fragment.SFFollowFragment;
import com.simplifyi.appname.ui.fragment.SFReplysFragment;
import com.simplifyi.simplifyi_phase2.R;

import java.util.ArrayList;
import java.util.List;


public class SFActivitiesActivity extends SFBaseAppBarActivity {
    private TabLayout mTabLayout;
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activities);
        configureActionBar(true, "Activity");
        mViewPager = findViewById(R.id.viewpager);
        setupViewPager(mViewPager);

        mTabLayout = enableTabLayout();
        mTabLayout.setupWithViewPager(mViewPager);
        setupTabIcons();
    }

    /**
     * expose the showProgress bar of the Base Activity to the Fragments that get attached this activity
     *
     * @param show
     */
    public void showProgress(boolean show) {
        super.showProgress(show);
    }

    public void showNoNetworkScreen(){
        super.showNoNetworkScreen();
    }

    public void login(SFBaseActivity.LoginListener listener) {
        super.login(listener);
    }

    private void setupTabIcons() {
        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText("Received Calls");
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.received, 0, 0);
        mTabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText("Dialled Calls");
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.dialled, 0, 0);
        mTabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText("Follows");
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.follow1, 0, 0);
        mTabLayout.getTabAt(2).setCustomView(tabThree);

        TextView tabFour = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFour.setText("Reply");
        tabFour.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.reply1, 0, 0);
        mTabLayout.getTabAt(3).setCustomView(tabFour);
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new SFReceivedFragment(), "Received Calls");
        adapter.addFrag(new SFDialedFragment(), "Dialled Calls");
        adapter.addFrag(new SFFollowFragment(), "Follows");
        adapter.addFrag(new SFReplysFragment(), "Reply");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public void back(View v) {
        Intent in = new Intent(SFActivitiesActivity.this, SFHomeActivity.class);
        startActivity(in);
    }

}