package com.simplifyi.appname.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.simplifyi.appname.model.SFTag;
import com.simplifyi.simplifyi_phase2.R;

import java.util.ArrayList;
import java.util.List;

public class SFTagAdapter extends RecyclerView.Adapter<SFTagAdapter.MyViewHolder> {
    public static int ITEM_TYPE_ODD = 1;
    public static int ITEM_TYPE_EVEN = 2;
    private List<Group> mTagGroups;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public Button firstView;
        public Button secondView;
        public Button thirdView;
        View rootView;

        public MyViewHolder(View v) {
            super(v);
            rootView = v;
            firstView = rootView.findViewById(R.id.first_view);
            secondView = rootView.findViewById(R.id.second_view);
            thirdView = rootView.findViewById(R.id.third_view);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public SFTagAdapter(List<SFTag> tags) {
        setData(tags);
    }

    public void setData(List<SFTag> tags) {
        if (mTagGroups == null) {
            mTagGroups = new ArrayList<>();
        }
        parseGroup(tags);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SFTagAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == ITEM_TYPE_ODD) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.tag_item_template_odd, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.tag_item_template_even, parent, false);
        }

        MyViewHolder vh = new MyViewHolder(itemView);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Group group = mTagGroups.get(position);
        if (group == null) return;
        if (group.first != null) {
            updateSelectionStatus(holder.firstView, group.first.isSelected());

            if (group.first.getName() != null) {
                holder.firstView.setVisibility(View.VISIBLE);
                holder.firstView.setText(group.first.getName());
                holder.firstView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onSelected(group.first, v);
                    }
                });
            }
        } else {
            holder.firstView.setVisibility(View.INVISIBLE);
        }
        if (group.second != null) {
            updateSelectionStatus(holder.secondView, group.second.isSelected());

            if (group.second.getName() != null) {
                holder.secondView.setVisibility(View.VISIBLE);
                holder.secondView.setText(group.second.getName());
                holder.secondView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onSelected(group.second, v);
                    }
                });
            }
        } else {
            holder.secondView.setVisibility(View.INVISIBLE);
        }
        if (group.third != null) {
            updateSelectionStatus(holder.thirdView, group.third.isSelected());

            if (group.third.getName() != null) {
                holder.thirdView.setVisibility(View.VISIBLE);
                holder.thirdView.setText(group.third.getName());
                holder.thirdView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onSelected(group.third, v);
                    }
                });
            }
        } else {
            holder.thirdView.setVisibility(View.INVISIBLE);
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mTagGroups.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 2 == 0) {
            return ITEM_TYPE_EVEN;
        } else {
            return ITEM_TYPE_ODD;
        }
    }

    void parseGroup(List<SFTag> tags) {
        Group group;
        for (int i = 0; i < tags.size(); i = i + 3) {
            group = new Group();
            group.first = tags.get(i);
            if ((i + 1) < tags.size()) {
                group.second = tags.get(i + 1);
            }
            if ((i + 2) < tags.size()) {
                group.third = tags.get(i + 2);
            }
            mTagGroups.add(group);
        }

    }

    class Group {
        SFTag first;
        SFTag second;
        SFTag third;
    }

    void onSelected(SFTag tag, View view) {
        tag.setSelected(!tag.isSelected());
        updateSelectionStatus(view, tag.isSelected());
    }

    void updateSelectionStatus(View view, boolean selected) {
        if (selected) {
            view.setBackgroundResource(R.drawable.button_background_bluecorners);
        } else {
            view.setBackgroundResource(R.drawable.button_background_grey);/**/
        }
    }
}
