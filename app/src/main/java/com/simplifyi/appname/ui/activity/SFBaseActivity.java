package com.simplifyi.appname.ui.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.graphics.drawable.Drawable;

import com.bumptech.glide.request.Request;
import com.bumptech.glide.request.target.SizeReadyCallback;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.network.request.SFLoginRequestBody;
import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.network.response.SFLoginToken;
import com.simplifyi.appname.network.restclient.SFLoginRestClient;
import com.simplifyi.appname.ui.viewmodel.SFUserViewModel;
import com.simplifyi.appname.utility.SFBuildSettings;
import com.simplifyi.appname.utility.SFDynamicPermissionManager;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.load.resource.drawable.DrawableResource;

import java.util.ArrayList;
import java.util.List;

public class SFBaseActivity extends AppCompatActivity {
    private PermissionCallback mPermissionCallback;
    private boolean mLoginCallIsInProgress = false;
    private List<LoginListener> mLoginListenerList;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoginListenerList = new ArrayList<>();
    }

    @Override
    public void setContentView(int childLayout) {
        super.setContentView(R.layout.activity_base);
        ViewGroup childContainer = findViewById(R.id.child_layout_container);
        getLayoutInflater().inflate(childLayout, childContainer);
    }


    protected void hideProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    protected void showProgress(boolean toBeShown) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading..");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
        }
        if (toBeShown) {
            if (!progressDialog.isShowing()) {
                progressDialog.show();
            }
        } else {
            hideProgress();
        }

//        final ImageView imageView = findViewById(R.id.splace_image_view);
////        Glide.with(this).load(R.drawable.ripple_loading)
////                .apply(new RequestOptions().placeholder(R.drawable.ripple_loading))
////                .into(new GlideBuilder(imageView));
//
////        Glide
////        .with(this)
////        .load(R.drawable.ripple_loading)
////        .into(imageViewTarget);
//
//        Glide.with(this)
////                .load("https://simplifyiapp.s3.us-east-2.amazonaws.com/Ripple-1s-200px.gif")
////                .into(imageView);
//                .load(R.drawable.ripple_loading)
//                .apply(new RequestOptions())
//                .into(new Target<Drawable>() {
//                    @Override
//                    public void onStart() {
//
//                    }
//
//                    @Override
//                    public void onStop() {
//
//                    }
//
//                    @Override
//                    public void onDestroy() {
//
//                    }
//
//
//                    @Override
//                    public void onLoadStarted(@Nullable Drawable placeholder) {
//
//                    }
//
//                    @Override
//                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
//
//                    }
//
//                    @Override
//                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
//                        imageView.setImageDrawable(resource);
//                    }
//
//                    @Override
//                    public void onLoadCleared(@Nullable Drawable placeholder) {
//
//                    }
//
//                    @Override
//                    public void getSize(@NonNull SizeReadyCallback cb) {
//
//                    }
//
//                    @Override
//                    public void removeCallback(@NonNull SizeReadyCallback cb) {
//
//                    }
//
//                    @Override
//                    public void setRequest(@Nullable Request request) {
//
//                    }
//
//                    @Nullable
//                    @Override
//                    public Request getRequest() {
//                        return null;
//                    }
//                });
//
//
//        if (toBeShown) {
//            findViewById(R.id.progress_layout).setVisibility(View.GONE);
//        } else {
//            findViewById(R.id.progress_layout).setVisibility(View.GONE);
//        }
    }

    protected void showNoNetworkScreen() {
        goToNextScreen(SFNoInternetActivity.class);
    }

    protected void goToNextScreen(Intent intent) {
        startActivity(intent);
    }

    protected void goToNextScreen(Class destination) {
        Intent intent = new Intent(this, destination);
        startActivity(intent);
    }

    protected void goToNextScreen(Class destination, String key, Bundle extras) {
        Intent intent = new Intent(this, destination);
        if (key != null && !key.isEmpty() && extras != null && !extras.isEmpty()) {
            intent.putExtra(key, extras);
        }
        startActivity(intent);
    }

    protected void requestPermission(List<String> permissions, PermissionCallback callback) {
        mPermissionCallback = callback;
        if (!SFDynamicPermissionManager.isAndroidMOrAbove() || permissions == null || permissions.isEmpty()) {
            mPermissionCallback.onNotRequired();
            return;
        }

        String[] array = new String[permissions.size()];
        for (int i = 0; i < permissions.size(); i++) {
            array[i] = permissions.get(i);
        }
        SFDynamicPermissionManager.requestPermission(this, array, 100);

    }

    protected void getCurrentUser(final Runnable onFinished) {
        final SFUserViewModel userViewModel = ViewModelProviders.of(this).get(SFUserViewModel.class);
        Observer<SFUser> forumUserObserver = new Observer<SFUser>() {
            @Override
            public void onChanged(@Nullable SFUser forumUser) {
                if (forumUser != null) {
                    if (SFBuildSettings.BUILD_TYPE == SFBuildSettings.BuildType.DEBUG_WITH_MOCK_BACKEND) {
                        if (!SFUtility.isStringNullOrEmpty(forumUser.getNick())) {
                            double rand = Math.random();
                            forumUser.setName(forumUser.getName() + rand);
                            forumUser.setNick(forumUser.getNick() + rand);
                        }
                    }
                    SFSharedPrefs.getInstance(SFBaseActivity.this).putUser(forumUser);
                }

                if (onFinished != null) {
                    onFinished.run();
                }
            }
        };
        Observer<Integer> forumUserErrorObserver = new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer error) {
                if (error == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    login(new LoginListener() {
                        @Override
                        public void onSuccess() {
                            String token = SFSharedPrefs.getInstance(SFBaseActivity.this).getSessionToken();
                            userViewModel.getCurrentUser(token);
                        }

                        @Override
                        public void onFail() {
                            SFLogger.showToast(SFBaseActivity.this, "fetching user name failed");
                        }
                    });

                } else {
                    SFLogger.showToast(SFBaseActivity.this, "unknown error while trying to fetch current user name");
                }
            }
        };
        userViewModel.getCurrentUserContainer().observe(this, forumUserObserver);
        userViewModel.getErrorContainer().observe(this, forumUserErrorObserver);
        String token = SFSharedPrefs.getInstance(this).getSessionToken();
        userViewModel.getCurrentUser(token);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        List<String> grantedList = new ArrayList<>();
        List<String> deniedList = new ArrayList<>();
        if (requestCode == 100) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    grantedList.add(permissions[i]);
                } else if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    deniedList.add(permissions[i]);
                }

            }
            if (!grantedList.isEmpty() && mPermissionCallback != null) {
                mPermissionCallback.onAllowTapped(grantedList);
            }
            if (!deniedList.isEmpty() && mPermissionCallback != null) {
                mPermissionCallback.onDenyTapped(deniedList);
            }
            mPermissionCallback = null;
        }
    }

    interface PermissionCallback {
        void onAllowTapped(List<String> permissions);

        void onDenyTapped(List<String> permissions);

        void onDontAskChecked(List<String> permissions);

        void onNotRequired();
    }

    protected void login(final LoginListener loginListener) {
        long mobileNumber = SFSharedPrefs.getInstance(this).getPhoneNumber();
        login(loginListener, mobileNumber);
    }

    protected void login(final LoginListener loginListener, final long mobileNumber) {

        String imei = null;
        if (SFDynamicPermissionManager.isAndroidMOrAbove()) {
            if (!SFDynamicPermissionManager.wasThePermissionGranted(this, Manifest.permission.READ_PHONE_STATE)) {
                List<String> permissions = new ArrayList<>();
                permissions.add(Manifest.permission.READ_PHONE_STATE);
                requestPermission(permissions, new PermissionCallback() {
                    @Override
                    public void onAllowTapped(List<String> permissions) {
                        String imei1 = SFUtility.getDeviceIMEI(SFBaseActivity.this);
                        doLogin(loginListener, mobileNumber, imei1);
                    }

                    @Override
                    public void onDenyTapped(List<String> permissions) {
                        if (loginListener != null) {
                            loginListener.onFail();
                        }
                    }

                    @Override
                    public void onDontAskChecked(List<String> permissions) {
                        if (loginListener != null) {
                            loginListener.onFail();
                        }
                    }

                    @Override
                    public void onNotRequired() {
                        if (loginListener != null) {
                            loginListener.onFail();
                        }
                    }
                });
            } else {
                imei = SFUtility.getDeviceIMEI(this);
                doLogin(loginListener, mobileNumber, imei);
            }
        } else {
            imei = SFUtility.getDeviceIMEI(this);
            doLogin(loginListener, mobileNumber, imei);
        }


    }

    protected void doLogin(final LoginListener loginListener, long mobileNumber, String imei) {
        mLoginListenerList.add(loginListener);
        if (mLoginCallIsInProgress) {
            return;
        }
        mLoginCallIsInProgress = true;
        if (mobileNumber != SFSharedPrefs.INVALID_PHONE_NUMBER) {
            SFLoginRequestBody body = new SFLoginRequestBody(imei, mobileNumber);
            SFLoginRestClient loginRestClient = new SFLoginRestClient();
            loginRestClient.login(body, new SFLoginRestClient.NetworkCallListener() {
                @Override
                public void onCallFinished(SFGenericResponse genericResponse) {
                    if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                        SFGenericResponse.SFBody<SFLoginToken> genericResponseBody = genericResponse.getBody();
                        SFLoginToken loginToken = genericResponseBody.getData().get(0);//only one login token exist
//                        SFLogger.showToast(SFBaseActivity.this, "Login successful");
                        if (!SFUtility.isStringNullOrEmpty(loginToken.getToken())) {
                            SFSharedPrefs.getInstance(SFBaseActivity.this).putSessionToken(loginToken.getToken());
                            SFLogger.debugLog("Login", "got new Token=" + loginToken.getToken());
                        }
                        publishLoginResult(true);
                        mLoginCallIsInProgress = false;
                    } else {
                        mLoginCallIsInProgress = false;
                        String error = "Login fail";
                        if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.LOGIN_FAIL_USER_NOT_EXIST) {
                            error = "Login onFail.User doesn't exist";
                            mLoginListenerList.clear();
                            SFSharedPrefs.getInstance(SFBaseActivity.this).clearPrefs();
                            goToNextScreen(SFMobileNumberActivity.class);
                            finish();
                        }
                        SFLogger.showToast(SFBaseActivity.this, error);
                        publishLoginResult(false);
                    }

                }

                @Override
                public void onCallFinishedWithError(String error) {
                    mLoginCallIsInProgress = false;
                    publishLoginResult(false);
                }
            });
        } else {
            mLoginCallIsInProgress = false;
            SFLogger.debugLog("Login:", "Invalid phone number");
        }
    }

    private void publishLoginResult(boolean loginSuccess) {
        for (LoginListener listener : mLoginListenerList) {
            if (loginSuccess) {
                listener.onSuccess();
            } else {
                listener.onFail();
            }
        }
        mLoginListenerList.clear();
    }

    public interface LoginListener {
        void onSuccess();

        void onFail();
    }

    protected void resolveNavigateToDestination() {
        SFUser currentUser = SFSharedPrefs.getInstance(this).getUser();
        Class destination;
        if (currentUser == null || !currentUser.isMobileVerified()) {
            destination = SFMobileNumberActivity.class;
        } else if (SFUtility.isStringNullOrEmpty(currentUser.getNick())) {
            destination = SFEditUserActivity.class;
        } else if (currentUser.getLanguages() == null || currentUser.getLanguages().isEmpty()) {
            destination = SFTagSelectionActivity.class;
        } else {
            destination = SFHomeActivity.class;
        }
        goToNextScreen(destination);
        finish();
    }


    protected void screenTransition() {
        // Check if we're running on Android 5.0 or higher
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Apply activity transition
        } else {
            // Swap without transition
        }
    }
}

