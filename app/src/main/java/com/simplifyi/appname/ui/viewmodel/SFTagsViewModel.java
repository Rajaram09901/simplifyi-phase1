package com.simplifyi.appname.ui.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.widget.ArrayAdapter;

import com.simplifyi.appname.model.SFTag;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.network.restclient.SFTagRestClient;
import com.simplifyi.appname.utility.SFLogger;

import java.util.ArrayList;
import java.util.List;

public class SFTagsViewModel extends ViewModel {
    //Data to be observed. i.e live data that keep changing & hence UI needs to change
    private MutableLiveData<List<SFTag>> mTagLisContainer;
    private MutableLiveData<Boolean> mSelectionStatusContainer;
    private MutableLiveData<Long> mMobileNumber;
    private MutableLiveData<List<String>> mLanguageContainer;
    private MutableLiveData<Context> mContextContainer;
    private MutableLiveData<Boolean> mTagsCallStatusContainer;
    private MutableLiveData<ArrayAdapter<String>> mSpinnerAdapterContainer;


    public MutableLiveData<ArrayAdapter<String>> getSpinnerAdapterContainer() {
        if (mSpinnerAdapterContainer == null) {
            mSpinnerAdapterContainer = new MutableLiveData<>();
        }
        return mSpinnerAdapterContainer;
    }

    //create the empty container
    public MutableLiveData<List<SFTag>> getTagListContainer() {
        if (mTagLisContainer == null) {
            mTagLisContainer = new MutableLiveData<>();
        }
        return mTagLisContainer;
    }


    public MutableLiveData<Long> getMobileNumberContainer() {
        if (mMobileNumber == null) {
            mMobileNumber = new MutableLiveData<>();
        }
        return mMobileNumber;
    }

    public MutableLiveData<List<String>> getLanguageContainer() {
        if (mLanguageContainer == null) {
            mLanguageContainer = new MutableLiveData<>();//we can get rid off 'new' usage  by adding dagger 2 to the project later
        }
        return mLanguageContainer;
    }

    public void setLanguage(List<String> languages) {
        getLanguageContainer().setValue(languages);
    }

    public void setMobileNumber(long mobileNumber) {
        getMobileNumberContainer().setValue(mobileNumber);
    }

    public MutableLiveData<Boolean> getSelectionStatusContainer() {
        if (mSelectionStatusContainer == null) {
            mSelectionStatusContainer = new MutableLiveData<>();
        }
        return mSelectionStatusContainer;
    }

    public MutableLiveData<Context> getContextContainer() {
        if (mContextContainer == null) {
            mContextContainer = new MutableLiveData<>();
        }
        return mContextContainer;
    }

    public void setSpinnerAdapterContainer(ArrayAdapter<String> spinnerAdapter) {
        getSpinnerAdapterContainer().setValue(spinnerAdapter);
    }

    public void setContext(Context context) {
        getContextContainer().setValue(context);
    }

    public MutableLiveData<Boolean> getTagsCallStatusContainer() {
        if (mTagsCallStatusContainer == null) {
            mTagsCallStatusContainer = new MutableLiveData<>();
        }
        return mTagsCallStatusContainer;
    }

    public void loadTags() {
        loadTags(1, null);//default level
    }

    public void loadTags(int level) {
        loadTags(level, null);//default level
    }

    //call this from activity
    public void loadTags(int level, String parentTag) {
        //showProgress(true);
        SFTagRestClient manager = new SFTagRestClient();
        manager.getTags(new SFTagRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                //showProgress(false);
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    SFGenericResponse.SFBody<SFTag> forumsResponseBody = genericResponse.getBody();
                    List<SFTag> tags = forumsResponseBody.getData();
                    if (tags != null && !tags.isEmpty()) {
                        updateSelectedStatus(tags);
                        getTagListContainer().setValue(tags);
                        SFLogger.debugLog("Size of the Tags fetched:", "Size=" + tags.size());
                    }
                    SFLogger.debugLog("tag fetch successful");
                } else {
                    SFLogger.debugLog("tag fetch un-successful");
                    getTagListContainer().setValue(new ArrayList<SFTag>());
                }

            }

            @Override
            public void onCallFinishedWithError(String error) {
                getTagListContainer().setValue(new ArrayList<SFTag>());
            }
        }, level, parentTag);

    }

    private void updateSelectedStatus(List<SFTag> tags) {
        //TODO: update the selected status of the Tags for this user
        for (SFTag tag : tags) {
            tag.setSelected(true);// initialize all of them to as selected for now..
        }
    }

    public void addTagsToUser() {
        SFUser user = new SFUser();
        user.setPhone(getMobileNumberContainer().getValue());
        user.setLanguages(getLanguageContainer().getValue());
        List<SFTag> selectedTags = new ArrayList<>();
        for (SFTag tag : getTagListContainer().getValue()) {
            if (tag.isSelected()) {
                selectedTags.add(tag);
            }
        }
        SFTagRestClient manager = new SFTagRestClient();
        manager.addTagsToUser(selectedTags, user, new SFTagRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    getTagsCallStatusContainer().setValue(true);
                } else {
                    getTagsCallStatusContainer().setValue(false);
                }

            }

            @Override
            public void onCallFinishedWithError(String error) {
                getTagsCallStatusContainer().setValue(false);
            }
        });

    }
}
