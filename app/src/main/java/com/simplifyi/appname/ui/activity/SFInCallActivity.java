package com.simplifyi.appname.ui.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.model.SFCall;
import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.model.SFSinchManager;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.ui.viewmodel.SFCallViewModel;
import com.simplifyi.appname.ui.viewmodel.SFUserViewModel;
import com.simplifyi.appname.utility.SFCallTimer;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallListener;

import java.util.List;
import java.util.Map;

public class SFInCallActivity extends SFBaseActivity {
    public static final String CALL_TYPE = "call_type";
    public static final String CALLABLE_FORUM = "forum_id";
    public static final int CALL_TYPE_INCOMING = 1;
    public static final int CALL_TYPE_OUTGOING = 2;
    private static final long MAX_CALL_DURATION = 10 * 60 * 1000;//mins * secs * milli
    private static final long COUNT_DOWN_INTERVAL = 1 * 1000;//1 sec
    private TextView mTimerTv;
    private SFSinchManager mSinchManager;
    private int mCallType;
    private boolean mWasCallConnected;
    private Call mSinchOnGoingCall;
    private SFForum mCallableForum;
    private boolean mCurrentUserDisconnected = false;
    private SFCallTimer mCallTimer;
    private SFUser mRemoteUser;
    private boolean mCallDisconnectionHandled;
    private boolean mIsMicMuted;
    private boolean mIsSpeakerEnabled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_call);
        mSinchManager = SFSinchManager.getInstance();
        mCallType = getIntent().getIntExtra(CALL_TYPE, -1);
        mCallableForum = (SFForum) getIntent().getSerializableExtra(CALLABLE_FORUM);
        if (mCallableForum == null || mCallableForum.getForumUsers() == null || mCallableForum.getForumUsers().isEmpty() ||
                SFUtility.isStringNullOrEmpty(mCallableForum.getForumUsers().get(0).getNickName())) {
            SFLogger.showToast(this, "Forum object is null. hence cannot make or receive call");
            finish();
            return;
        }
        getRemoteUserToRate();
        initializeUI();
        setUpSinch();
    }

    private void getRemoteUserToRate() {
        if (mCallType == CALL_TYPE_INCOMING) {
            mSinchManager.answerIncomingCall();
            Map<String, String> callHeader = SFSinchManager.getInstance().getCurrentCall().getHeaders();
            final String userId = callHeader.get(SFCall.SF_CALLER_USER_ID);
            getRemoteUser(userId);
        } else if (mCallType == CALL_TYPE_OUTGOING) {//called from dashboard
           SFForum.ForumUser forumUser = mCallableForum.getForumUsers().get(0);
           if(forumUser!=null && !SFUtility.isStringNullOrEmpty(forumUser.getUserId())) {
               getRemoteUser(forumUser.getUserId());
           }
            String nickName = mCallableForum.getForumUsers().get(0).getNickName();
            boolean TheCallPlacedSuccessfully = SFSinchManager.getInstance().callUser(nickName);
            if (TheCallPlacedSuccessfully) {
                //TODO: call CallInitiated()..
                updateServerAsCallInitiated();
            } else {
                SFLogger.showToast(this, "Sinch failed to call the user " + nickName);
                finish();
                return;

            }

        }
    }

    private void getRemoteUser(final String userId) {
        if (SFUtility.isStringNullOrEmpty(userId)) {
            SFLogger.showToast(this, "remote caller id is null or empty");
            return;
        }

        final SFUserViewModel userViewModel = ViewModelProviders.of(this).get(SFUserViewModel.class);
        Observer<SFUser> forumUserObserver = new Observer<SFUser>() {
            @Override
            public void onChanged(@Nullable SFUser user) {
                if (user != null) {
                    mRemoteUser = user;
                    initializeUI();
                }
            }
        };
        Observer<Integer> forumUserErrorObserver = new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer error) {
                if (error == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    login(new LoginListener() {
                        @Override
                        public void onSuccess() {
                            String token = SFSharedPrefs.getInstance(SFInCallActivity.this).getSessionToken();
                            userViewModel.getUserById(userId, token);
                        }

                        @Override
                        public void onFail() {
                            SFLogger.showToast(SFInCallActivity.this, "fetching user by id failed");
                        }
                    });

                } else {
                    SFLogger.showToast(SFInCallActivity.this, "unknown error while trying to fetch user by id");
                }
            }
        };
        userViewModel.getUserByIdContainer().observe(this, forumUserObserver);
        userViewModel.getErrorContainer().observe(this, forumUserErrorObserver);
        String token = SFSharedPrefs.getInstance(this).getSessionToken();
        userViewModel.getUserById(userId, token);

    }

    private void setUpSinch() {
        mSinchOnGoingCall = mSinchManager.getCurrentCall();
        if (mSinchOnGoingCall != null) {
            mSinchOnGoingCall.addCallListener(new CallListener() {
                @Override
                public void onCallProgressing(Call call) {
                    SFLogger.showToast(SFInCallActivity.this, "Call Progressing");
                }

                @Override
                public void onCallEstablished(Call call) {
                    mWasCallConnected = true;
                    SFLogger.showToast(SFInCallActivity.this, "Call Established");
                    mTimerTv = findViewById(R.id.timerText);
                    mTimerTv.setVisibility(View.VISIBLE);
                    mCallTimer = new SFCallTimer();
                    mCallTimer.start();
                    mCallTimer.setTickerListener(new SFCallTimer.OnTickerListener() {
                        @Override
                        public void onTick(int hours, int minutes, int seconds) {
                            SFLogger.debugLog("SFCallTimer", hours + ":" + minutes + ":" + seconds);
                            mTimerTv.setText(hours + ":" + minutes + ":" + seconds + "");
                        }
                    });
                }

                @Override
                public void onCallEnded(Call call) {
                    SFLogger.showToast(SFInCallActivity.this, "Call Ended");
                    if (mCallTimer != null) {
                        mCallTimer.stop();
                        SFLogger.debugLog("Call Summary", mCallTimer.toString());
                    }

                    if (!mCurrentUserDisconnected) { //mHasCAllConnected is true, then call hangup with force=false , else, call decline.
                        if (mWasCallConnected) {
                            onCallDisconnected();
                        } else {
                            finish();
                        }
                    }
                }

                @Override
                public void onShouldSendPushNotification(Call call, List<PushPair> list) {

                }
            });
        } else {
            SFLogger.showToast(this, "Call object is null in sinch");
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initializeUI() {

        TextView forumQuestionTv = findViewById(R.id.forum_question_tv);
        forumQuestionTv.setText(mCallableForum.getName());
        TextView primaryTagTv = findViewById(R.id.textViewPrimaryTag);
        primaryTagTv.setText(mCallableForum.getTags().get(0));
        TextView secondaryTagTv = findViewById(R.id.textViewSecondaryTag);
        secondaryTagTv.setText(mCallableForum.getTags().get(1));

        if (mRemoteUser != null) {
            ImageView profileIv = findViewById(R.id.owner_profile_pic);
            if (!SFUtility.isStringNullOrEmpty(mRemoteUser.getProfileImageUrl())) {
                Glide.with(this).load(mRemoteUser.getProfileImageUrl()).into(profileIv);
            }
            if (!SFUtility.isStringNullOrEmpty(mRemoteUser.getNick())) {
                TextView userNameTv = findViewById(R.id.forum_owner_name_tv);
                userNameTv.setText(mRemoteUser.getNick());
            }
            StringBuffer buffer = new StringBuffer();
            if (mRemoteUser.getLanguages() != null && !mRemoteUser.getLanguages().isEmpty()) {
                for (String language : mRemoteUser.getLanguages()) {
                    buffer.append(" ");
                    buffer.append(language);
                }
            }
            String languageText = getString(R.string.user_language_text) + (SFUtility.isStringNullOrEmpty(buffer.toString()) ? " none" : buffer.toString());
            TextView userLanguageTv = findViewById(R.id.user_language_tv);
            userLanguageTv.setText(languageText.trim());
        }
        findViewById(R.id.call_hangup_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentUserDisconnected = true;
                if (mWasCallConnected) {
                    mSinchManager.hangupCall();
                    onCallDisconnected();
                } else {
                    finish();//simply close this activity
                }
            }
        });

        findViewById(R.id.mute_unmute_mic_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onMicTapped();
            }
        });
        findViewById(R.id.loud_speaker_on_off_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoudSpeakerTapped();
            }
        });
    }

    private void onCallDisconnected() {//if the current user presses hangup button.
        if (mCallDisconnectionHandled) {
            return;
        }
        mCallDisconnectionHandled = false;
        if (mCallType == CALL_TYPE_OUTGOING) {
            updateServerAsCallEnded();
        } else {
            launchFeedBackScreen();
            finish();
        }
    }


    private void updateServerAsCallInitiated() {
        final SFCallViewModel callViewModel = ViewModelProviders.of(this).get(SFCallViewModel.class);
        Observer<Boolean> callInitiatedObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isCallInitiatedSuccessful) {
                if (isCallInitiatedSuccessful) {
                    SFLogger.showToast(SFInCallActivity.this, "Call_initiated call is successful");
                } else {
                    SFLogger.showToast(SFInCallActivity.this, "Call_initiated is failed");
                }
            }
        };
        Observer<Integer> callInitiatedErrorObserver = new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer error) {
                if (error == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    login(new LoginListener() {
                        @Override
                        public void onSuccess() {
                            String token = SFSharedPrefs.getInstance(SFInCallActivity.this).getSessionToken();
                            if (mCallableForum != null && mCallableForum.getForumUsers() != null && !mCallableForum.getForumUsers().isEmpty()) {
                                callViewModel.markCallInitiated(token, mCallableForum.getId(), mCallableForum.getForumUsers().get(0).getUserId());
                            }
                        }

                        @Override
                        public void onFail() {
                            SFLogger.showToast(SFInCallActivity.this, "Mark CallInitiated call is failed");
                        }
                    });

                } else {
                    SFLogger.showToast(SFInCallActivity.this, "unknown error while trying to call CallInitiated");
                }
            }
        };
        callViewModel.getMarkCallInitiatedOrEndedContainer().observe(this, callInitiatedObserver);
        callViewModel.getErrorContainer().observe(this, callInitiatedErrorObserver);
        String token = SFSharedPrefs.getInstance(this).getSessionToken();
        if (mCallableForum != null && mCallableForum.getForumUsers() != null && !mCallableForum.getForumUsers().isEmpty()) {
            callViewModel.markCallInitiated(token, mCallableForum.getId(), mCallableForum.getForumUsers().get(0).getUserId());
        }
    }

    private void updateServerAsCallEnded() {
        final SFCallViewModel callViewModel = ViewModelProviders.of(this).get(SFCallViewModel.class);
        Observer<Boolean> callInitiatedObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isCallInitiatedSuccessful) {
                if (isCallInitiatedSuccessful) {
                    SFLogger.showToast(SFInCallActivity.this, "CallEnded call is successful");
                    launchFeedBackScreen();
                    finish();
                } else {
                    SFLogger.showToast(SFInCallActivity.this, "CallEnded is failed");
                }
            }
        };
        Observer<Integer> callInitiatedErrorObserver = new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer error) {
                if (error == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    login(new LoginListener() {
                        @Override
                        public void onSuccess() {
                            String token = SFSharedPrefs.getInstance(SFInCallActivity.this).getSessionToken();
                            if (mCallableForum != null && mCallableForum.getForumUsers() != null && !mCallableForum.getForumUsers().isEmpty()) {
                                callViewModel.markCallEnded(token, mCallableForum.getId(), mCallableForum.getForumUsers().get(0).getUserId());
                            }
                        }

                        @Override
                        public void onFail() {
                            SFLogger.showToast(SFInCallActivity.this, "Mark CallEnded call is failed");
                        }
                    });

                } else {
                    SFLogger.showToast(SFInCallActivity.this, "unknown error while trying to call CallEnded");
                }
            }
        };
        callViewModel.getMarkCallInitiatedOrEndedContainer().observe(this, callInitiatedObserver);
        callViewModel.getErrorContainer().observe(this, callInitiatedErrorObserver);
        String token = SFSharedPrefs.getInstance(this).getSessionToken();
        if (mCallableForum != null && mCallableForum.getForumUsers() != null && !mCallableForum.getForumUsers().isEmpty()) {
            callViewModel.markCallEnded(token, mCallableForum.getId(), mCallableForum.getForumUsers().get(0).getUserId());
        }
    }

    private void launchFeedBackScreen() {
        if (!SFUtility.isStringNullOrEmpty(mRemoteUser.getId())) {
            SFLogger.debugLog("CALLEE", "The caller id=" + mRemoteUser.getId());
            Intent intent = new Intent(this, SFCallFeedbackActivity.class);
            intent.putExtra(SFInCallActivity.CALLABLE_FORUM, mCallableForum);
            intent.putExtra(SFCall.SF_REMOTE_CALLER, mRemoteUser);
            goToNextScreen(intent);
        } else {
            SFLogger.showToast(this, "Call Header is Null. so not launching feedback Activity");
        }
    }

    private void onLoudSpeakerTapped() {
        SFLogger.showToast(this, "Loud Speaker tapped");
        if (mSinchManager.getAudioController() != null) {
            if (mIsSpeakerEnabled) {
                mSinchManager.getAudioController().disableSpeaker();
                ((ImageView) findViewById(R.id.loud_speaker_on_off_iv)).setImageResource(R.drawable.loud_speaker_off);
            } else {
                mSinchManager.getAudioController().enableSpeaker();
                ((ImageView) findViewById(R.id.loud_speaker_on_off_iv)).setImageResource(R.drawable.loud_speaker_on);
            }
            mIsSpeakerEnabled = !mIsSpeakerEnabled;
        }
    }

    private void onMicTapped() {
        SFLogger.showToast(this, "Mic tapped");
        if (mSinchManager.getAudioController() != null) {
            if (mIsMicMuted) {
                mSinchManager.getAudioController().unmute();
                ((ImageView) findViewById(R.id.mute_unmute_mic_iv)).setImageResource(R.drawable.mic_on);
            } else {
                mSinchManager.getAudioController().mute();
                ((ImageView) findViewById(R.id.mute_unmute_mic_iv)).setImageResource(R.drawable.mic_off);
            }
            mIsMicMuted = !mIsMicMuted;

        }
    }

    @Override
    public void onBackPressed() {
        //TODO: don't close the activity as the user is still in call
    }

    public void back(View v) {
        //TODO: don't close the activity as the user is still in call
    }


}
