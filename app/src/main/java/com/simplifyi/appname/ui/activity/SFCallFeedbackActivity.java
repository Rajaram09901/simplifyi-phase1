package com.simplifyi.appname.ui.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.model.SFCall;
import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.ui.viewmodel.SFCallViewModel;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

public class SFCallFeedbackActivity extends SFBaseActivity {
    private TextView mRatingDescriptionTv;
    private SFForum mCallableForum;
    private int mRating;
    private SFUser mRemoteUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_feedback);
        mRating = -1;
        mCallableForum = (SFForum) getIntent().getSerializableExtra(SFInCallActivity.CALLABLE_FORUM);
        mRemoteUser = (SFUser) getIntent().getSerializableExtra(SFCall.SF_REMOTE_CALLER);
        if (mCallableForum == null || SFUtility.isStringNullOrEmpty(mCallableForum.getId()) || mRemoteUser == null || SFUtility.isStringNullOrEmpty(mRemoteUser.getId())) {
            SFLogger.showToast(this, "Forum object is null. hence cannot call rateUser()");
            finish();
            return;
        }
        initializeUI();

    }

    private void initializeUI() {
        String rateUser = "Rate User";
        if (mRemoteUser != null && !SFUtility.isStringNullOrEmpty(mRemoteUser.getName())) {
            rateUser = "Rate " + mRemoteUser.getName();
        }
        ImageView mProfilePicIv = findViewById(R.id.profile_pic_iv);
        if (mRemoteUser != null && !SFUtility.isStringNullOrEmpty(mRemoteUser.getProfileImageUrl())) {
            Glide.with(this).load(mRemoteUser.getProfileImageUrl()).into(mProfilePicIv);
        }

        ((TextView) findViewById(R.id.user_name_tv)).setText(rateUser);
        mRatingDescriptionTv = findViewById(R.id.tvRatingScale);
        ((RatingBar) findViewById(R.id.ratingBar)).setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                mRatingDescriptionTv.setText(String.valueOf(v));
                String[] ratingDescriptions = getResources().getStringArray(R.array.rating);
                switch ((int) ratingBar.getRating()) {
                    case 1:
                        mRating = 1;
                        mRatingDescriptionTv.setText(ratingDescriptions[0]);
                        break;
                    case 2:
                        mRating = 2;
                        mRatingDescriptionTv.setText(ratingDescriptions[1]);
                        break;
                    case 3:
                        mRating = 3;
                        mRatingDescriptionTv.setText(ratingDescriptions[2]);
                        break;
                    case 4:
                        mRating = 4;
                        mRatingDescriptionTv.setText(ratingDescriptions[3]);
                        break;
                    case 5:
                        mRating = 5;
                        mRatingDescriptionTv.setText(ratingDescriptions[4]);
                        break;
                    default:
                        mRatingDescriptionTv.setText("");
                }
                onRatingTapped();
            }
        });

    }

    private void onRatingTapped() {
        Button continueButton = findViewById(R.id.done_button);
        if (mRating > 0) {
            continueButton.setBackgroundResource(R.drawable.button_background_orange);
            continueButton.setEnabled(true);
            continueButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mRating != -1) {
                        rateUser();
                    }
                }
            });
        } else {
            continueButton.setBackgroundResource(R.drawable.button_bg_rounded_corners);
            continueButton.setEnabled(false);

        }
    }

    private void rateUser() {
        final SFCallViewModel callViewModel = ViewModelProviders.of(this).get(SFCallViewModel.class);
        Observer<Boolean> rateUserCallStatusObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean rateUserCallSuccessful) {
                if (rateUserCallSuccessful) {
                    SFLogger.showToast(SFCallFeedbackActivity.this, "rateUser() call is successful");
                    goToNextScreen(SFCallFeedbackActivity.class);
                    finish();
                } else {
                    SFLogger.showToast(SFCallFeedbackActivity.this, "rateUser() call is failed");
                }
            }
        };
        Observer<Integer> rateUserCallErrorObserver = new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer error) {
                if (error == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    //TODO: 404 is thrown on both user doesn't exist and token expired cases which is misleading. can we have dif error thrown here?
                    login(new LoginListener() {
                        @Override
                        public void onSuccess() {
                            String token = SFSharedPrefs.getInstance(SFCallFeedbackActivity.this).getSessionToken();
                            callViewModel.rateUser(mCallableForum.getId(), mRemoteUser.getId(), mRating, token);
                        }

                        @Override
                        public void onFail() {
                            SFLogger.showToast(SFCallFeedbackActivity.this, "rateUser() is failed");
                        }
                    });

                } else {
                    SFLogger.showToast(SFCallFeedbackActivity.this, "unknown error while trying to call rateUser()");
                }
            }
        };
        callViewModel.getRateUserContainer().observe(this, rateUserCallStatusObserver);
        callViewModel.getErrorContainer().observe(this, rateUserCallErrorObserver);
        String token = SFSharedPrefs.getInstance(this).getSessionToken();
        callViewModel.rateUser(mCallableForum.getId(), mRemoteUser.getId(), mRating, token);
    }

}


