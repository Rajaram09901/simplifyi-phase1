package com.simplifyi.appname.ui.activity;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.simplifyi.appname.ui.fragment.SFProfileFragment;
import com.simplifyi.simplifyi_phase2.R;

public class SFUserProfileActivity extends AppCompatActivity {

    public static Fragment newInstance() {
        return new SFProfileFragment();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_user_profile);
    }
}
