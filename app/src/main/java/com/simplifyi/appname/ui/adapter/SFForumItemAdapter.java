package com.simplifyi.appname.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.network.SFRetrofit;
import com.simplifyi.appname.network.apiinterface.SFForumAPIInterface;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.ui.activity.SFForumAnswersActivity;
import com.simplifyi.appname.ui.activity.SFForumListActivity;
import com.simplifyi.appname.ui.activity.SFHomeActivity;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.simplifyi_phase2.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SFForumItemAdapter extends RecyclerView.Adapter<SFForumItemAdapter.MyViewHolder> {
    private List<SFForum> mForumUsers;
    private Context mContext;
    private SFForumAPIInterface mApiInterface;




    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView forumUserNameTv;
        public TextView forumNameTv;
        public TextView replyTv;
        public TextView followerTv;
        public TextView primaryTagTv;
        public TextView secondaryTagTv;
        public ImageView options_image_button;
        public ImageView reject_iv;
        public ImageButton follow_iv;
        public CircleImageView owner_profile_pic;
        View rootView;

        public MyViewHolder(View v) {
            super(v);
            rootView = v;
            forumUserNameTv = rootView.findViewById(R.id.forum_owner_name_tv);
            forumNameTv = rootView.findViewById(R.id.forum_question_tv);
            followerTv = rootView.findViewById(R.id.follower_tv);
            replyTv = rootView.findViewById(R.id.reply_tv);
            primaryTagTv = rootView.findViewById(R.id.textViewPrimaryTag);
            secondaryTagTv = rootView.findViewById(R.id.textViewSecondaryTag);
            options_image_button = rootView.findViewById(R.id.options_image_button);
            reject_iv = rootView.findViewById(R.id.reject_iv);
            follow_iv = rootView.findViewById(R.id.follow_iv);
            owner_profile_pic = rootView.findViewById(R.id.owner_profile_pic);
        }
    }

    public void setData(List<SFForum> forumUsers) {
        if (forumUsers != null && forumUsers.size() > 0) {
            mForumUsers.clear();
            mForumUsers.addAll(forumUsers);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public SFForumItemAdapter(Context context, List<SFForum> forumUsers) {
        mForumUsers = forumUsers;
        mContext = context;
        mApiInterface = SFRetrofit.getClient().create(SFForumAPIInterface.class);
    }


    // Create new views (invoked by the layout manager)
    @Override
    public SFForumItemAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reusable_forum_list_item_template, parent, false);

        MyViewHolder vh = new MyViewHolder(itemView);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final SFForum forum = mForumUsers.get(position);
        if (forum != null) {
            if (forum.getForumUsers() != null && !forum.getForumUsers().isEmpty() && forum.getForumUsers().get(0) != null) {
                holder.forumUserNameTv.setText(forum.getForumUsers().get(0).getNickName());
            }
            if (forum.getName() != null && !forum.getName().isEmpty()) {
                holder.forumNameTv.setText(forum.getName());
                holder.forumNameTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goToAnswersScreen(forum);
                    }
                });
            }
            holder.followerTv.setText(forum.getFollows() + " Followers");
            holder.replyTv.setText(forum.getAnswers().size() + " Replies");
            holder.replyTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  SFLogger.debugLog("SFForumItemAdapter", "Replies Tapped");
                    goToAnswersScreen(forum);
                }
            });

            holder.followerTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.setActivated(!v.isActivated());
                }
            });

            if(forum.getForumUsers().get(0).getProfileImageUrl() != null) {

                    String url = forum.getForumUsers().get(0).getProfileImageUrl();
                  Glide.with(mContext).load(url).into(holder.owner_profile_pic);

            }


            holder.reject_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToAnswersScreen(forum);
                }
            });

            holder.follow_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //ImageView followIv = (ImageView) v;
                    v.setActivated(!v.isActivated());
                    //   SFLogger.showToast(mContext, "follow tapped");
                    //TODO: how do we know if the current user is following a forum & hence the UI is updated accordingly?"
                }
            });
            holder.options_image_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PopupMenu dropDownMenu = new PopupMenu(mContext, holder.options_image_button);
                    dropDownMenu.getMenuInflater().inflate(R.menu.drop_down_menu, dropDownMenu.getMenu());
                    //    showMenu.setText("DropDown Menu");
                    dropDownMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {


                            Map queryParamsMap = new HashMap();
                            queryParamsMap.put("forumId", forum.getId());
                            Call<SFGenericResponse<SFForum>> call = mApiInterface.doDeleteForum(queryParamsMap);
                            call.enqueue(new Callback<SFGenericResponse<SFForum>>() {
                                @Override
                                public void onResponse(Call<SFGenericResponse<SFForum>> call, Response<SFGenericResponse<SFForum>> response) {
                                }

                                @Override
                                public void onFailure(Call<SFGenericResponse<SFForum>> call, Throwable t) {
                                                                    }
                            });


                            mForumUsers.remove(position);
                            ((SFHomeActivity)mContext).refreshForumList();

                            //   Toast.makeText(mContext, "You have clicked " + menuItem.getTitle(), Toast.LENGTH_LONG).show();
                            return true;
                        }
                    });
                    dropDownMenu.show();


                }
            });



            if (forum.getTags() != null && !forum.getTags().isEmpty()) {
                holder.primaryTagTv.setText(forum.getTags().get(0));
                if (forum.getTags().size() > 1) {
                    holder.secondaryTagTv.setText(forum.getTags().get(1));
                }

            }
        }
    }




private void goToAnswersScreen(SFForum forum) {
        Intent in = new Intent(mContext, SFForumAnswersActivity.class);
        in.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        in.putExtra(SFForumAnswersActivity.SELECTED_FORUM, forum);
        mContext.startActivity(in);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mForumUsers.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

}
