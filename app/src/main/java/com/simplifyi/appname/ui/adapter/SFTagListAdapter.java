package com.simplifyi.appname.ui.adapter;


import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.PictureDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou;
import com.simplifyi.appname.SFApplication;
import com.simplifyi.appname.model.SFTag;
import com.simplifyi.appname.ui.activity.SFLevel2TagListActivity;
import com.simplifyi.simplifyi_phase2.R;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class SFTagListAdapter extends RecyclerView.Adapter<SFTagListAdapter.MyViewHolder> {
    private OnTagItemClickListener mOnTagItemClickListener;
    RequestOptions options;
    private Context mContext;
    private List<SFTag> mData;
    RequestBuilder<PictureDrawable> requestBuilder;

    public SFTagListAdapter(Context mContext, List<SFTag> lst) {
        this.mContext = mContext;
        if (lst != null) {
            mData = lst;
        } else {
            mData = new ArrayList<>();
        }
         requestBuilder = GlideToVectorYou
                .init()
                .with((Activity) mContext)
                .getRequestBuilder();

       /* options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.loading_shape)
                .error(R.drawable.loading_shape);*/

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        view = mInflater.inflate(R.layout.list_tag_item_template, parent, false);
        // click listener here
        return new MyViewHolder(view);
    }


    public void setData(List<SFTag> tags) {
        mData = tags;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tagName.setText(mData.get(position).getName());
        // load image from the internet using Glide
        //TODO: Glide image library-loading images
        requestBuilder
                .load(mData.get(position).getImage())
                .transition(DrawableTransitionOptions.withCrossFade())
                .apply(new RequestOptions()
                        .centerCrop())
                .into(holder.tagIcon);
     /*   Glide.with(mContext).load(mData.get(position).getImage().toString()).
                apply(options).into(holder.tagIcon);*/
        holder.mItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mOnTagItemClickListener!=null){
                    mOnTagItemClickListener.onTagItemClick(mData.get(position));
                }
                /*SFLogger.showToast(((Activity) mContext), "go to next sub category");
                Intent intent = new Intent(((Activity) mContext), SFLevel3TagsListActivity.class);
                intent.putExtra(SFLevel3TagsListActivity.LEVEL_TWO_TAG_NAME, mData.get(position).getName());
                mContext.startActivity(intent);*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        View mItemView;
        TextView tagName;
        CircleImageView tagIcon;


        public MyViewHolder(View itemView) {
            super(itemView);
            mItemView = itemView;
            tagName = itemView.findViewById(R.id.rowname);

            tagIcon = itemView.findViewById(R.id.thumbnail);
        }
    }

    public void setOnTagItemClickListener(OnTagItemClickListener listener) {
        mOnTagItemClickListener = listener;
    }

    public interface OnTagItemClickListener {
        void onTagItemClick(SFTag tag);
    }
}