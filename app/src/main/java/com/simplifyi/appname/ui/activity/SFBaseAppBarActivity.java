package com.simplifyi.appname.ui.activity;

import android.support.design.widget.TabLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.simplifyi.simplifyi_phase2.R;

public class SFBaseAppBarActivity extends SFBottomNavigationActivity {
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    protected void setCustomViewForActionBar(View view) {
        ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
//        actionBar.setCustomView(view);
    }

    @Override
    public void setContentView(int childLayout) {
        super.setContentView(R.layout.activity_base_appbar);
        ViewGroup childContainer = findViewById(R.id.appbar_child_layout_container);
        getLayoutInflater().inflate(childLayout, childContainer);
        initializeActionBar();
    }

    protected void configureActionBar(boolean showOptionsMenu, String title) {
//        View view = LayoutInflater.from(this).inflate(R.layout.reusable_layout_actionbar, null);
        setCustomViewForActionBar(null);
        TextView toolbarTitle = mToolbar.findViewById(R.id.title_tv);
        toolbarTitle.setText(title);
//        if (showOptionsMenu) {
//            view.findViewById(R.id.options_button).setVisibility(View.VISIBLE);
//            view.findViewById(R.id.options_button).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    openOptionsMenu();
//                }
//            });
//        } else {
//            view.findViewById(R.id.options_button).setVisibility(View.INVISIBLE);
//        }
//        ((TextView) view.findViewById(R.id.title_tv)).setText(title);
//        view.findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void showTitle(String title) {
        getSupportActionBar().setTitle(title);

    }

    private void initializeActionBar() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        findViewById(R.id.tablayout).setVisibility(View.GONE);
    }

    protected TabLayout enableTabLayout() {
        TabLayout tabLayout = findViewById(R.id.tablayout);
        tabLayout.setVisibility(View.VISIBLE);
        return tabLayout;
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return false;
    }
}
