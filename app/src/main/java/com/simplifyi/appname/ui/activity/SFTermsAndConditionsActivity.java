package com.simplifyi.appname.ui.activity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import com.simplifyi.simplifyi_phase2.R;

public class SFTermsAndConditionsActivity extends SFBaseAppBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tnc);
        configureActionBar(false, "Terms & Conditions");
        TextView textView = findViewById(R.id.tnc_content_tv);
        textView.setMovementMethod(new ScrollingMovementMethod());
    }

    @Override
    public void onBackPressed() {
       finish();
    }
}
