package com.simplifyi.appname.ui.activity;

import android.os.Bundle;
import android.os.Handler;

import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.ui.fragment.SFFirstTimeOneFragment;
import com.simplifyi.appname.utility.SFAnalytics;
import com.simplifyi.appname.utility.SFBuildSettings;
import com.simplifyi.appname.utility.SFMockDataProvider;
import com.simplifyi.simplifyi_phase2.R;


public class SFSplashActivity extends SFBaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        SFAnalytics.getInstance(this).setCurrentScreen(this, "Home Screen");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isFirstTimeUser()) {
                    //TODO: goto first time user screens
                    goToNextScreen(SFPagerActivity.class);
                    finish();
                } else {
                    resolveNavigateToDestination();
                }
            }

        }, ((SFBuildSettings.BUILD_TYPE == SFBuildSettings.BuildType.DEBUG_WITH_MOCK_BACKEND) ? 1000 : 3000));
        SFMockDataProvider.getInstance(this);
    }

    private boolean isFirstTimeUser() {
        return SFSharedPrefs.getInstance(this).isFirstTimeUser();
    }

}
