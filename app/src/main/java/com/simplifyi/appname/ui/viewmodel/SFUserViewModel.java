package com.simplifyi.appname.ui.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.network.request.SFUserSettingsRequest;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.network.restclient.SFUserRestClient;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;

import java.util.List;

public class SFUserViewModel extends ViewModel {

    private MutableLiveData<SFUser> mCurrentUserContainer;
    private MutableLiveData<Boolean> mUserSettingsUpdatedContainer;
    private MutableLiveData<SFUser> mUserByIdContainer;
    private MutableLiveData<Integer> mErrorContainer;

    public MutableLiveData<Boolean> getmUserSettingsUpdatedContainer() {
        if (mUserSettingsUpdatedContainer == null) {
            mUserSettingsUpdatedContainer = new MutableLiveData<>();
        }
        return mUserSettingsUpdatedContainer;
    }

    public MutableLiveData<SFUser> getUserByIdContainer() {
        if (mUserByIdContainer == null) {
            mUserByIdContainer = new MutableLiveData<>();
        }
        return mUserByIdContainer;
    }

    public MutableLiveData<SFUser> getCurrentUserContainer() {
        if (mCurrentUserContainer == null) {
            mCurrentUserContainer = new MutableLiveData<>();
        }
        return mCurrentUserContainer;
    }

    public MutableLiveData<Integer> getErrorContainer() {
        if (mErrorContainer == null) {
            mErrorContainer = new MutableLiveData<>();
        }
        return mErrorContainer;
    }

    public void getCurrentUser(String token) {
        if (SFUtility.isStringNullOrEmpty(token)) {
            getErrorContainer().setValue(SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED);
            return;
        }
        final SFUserRestClient userRestClient = new SFUserRestClient();
        userRestClient.getCurrentUser(token, new SFUserRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    List<SFUser> users = genericResponse.getBody().getData();
                    if (users != null && !users.isEmpty()) {
                        SFUser currentUser = users.get(0);
                        getCurrentUserContainer().setValue(currentUser);
                        SFLogger.debugLog("Current User fetch successful");
                    }
                } else if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    getErrorContainer().setValue(SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED);
                }

            }

            @Override
            public void onCallFinishedWithError(String error) {
                mErrorContainer.setValue(SFAPIResponseCode.Error.UN_KNOWN);
            }
        });
    }

    public void getUserById(String userId, String token) {
        if (SFUtility.isStringNullOrEmpty(token)) {
            getErrorContainer().setValue(SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED);
            return;
        }
        if (SFUtility.isStringNullOrEmpty(userId)) {
            SFLogger.debugLog("getUserById(): user id is null or empty");
            mErrorContainer.setValue(SFAPIResponseCode.Error.UN_KNOWN);
            return;
        }
        final SFUserRestClient userRestClient = new SFUserRestClient();
        userRestClient.getUserById(userId, token, new SFUserRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    List<SFUser> users = genericResponse.getBody().getData();
                    if (users != null && !users.isEmpty()) {
                        SFUser currentUser = users.get(0);
                        getUserByIdContainer().setValue(currentUser);
                        SFLogger.debugLog("User by id fetch successful");
                    }
                } else if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    getErrorContainer().setValue(SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED);
                }

            }

            @Override
            public void onCallFinishedWithError(String error) {
                mErrorContainer.setValue(SFAPIResponseCode.Error.UN_KNOWN);
            }
        });
    }

    public void updateUserSettings(SFUserSettingsRequest request, String token) {
        if (SFUtility.isStringNullOrEmpty(token)) {
            getErrorContainer().setValue(SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED);
            return;
        }
        final SFUserRestClient userRestClient = new SFUserRestClient();
        userRestClient.postUserSettings(request, token, new SFUserRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    //TODO: update UI
                    getmUserSettingsUpdatedContainer().setValue(true);
                } else if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    getErrorContainer().setValue(SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED);
                }
            }

            @Override
            public void onCallFinishedWithError(String error) {
                mErrorContainer.setValue(SFAPIResponseCode.Error.UN_KNOWN);
            }
        });
    }
}
