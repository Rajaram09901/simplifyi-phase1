package com.simplifyi.appname.ui.customview;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.simplifyi.appname.utility.SFLogger;

/**
 * Created by dinesh m on 22-01-2019.
 */
public class SFGestureHandler implements GestureDetector.OnGestureListener {
    private static final int SWIPE_MIN_DISTANCE = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private View mGestureView;
    private Context mContext;
    private GestureDetector mGestureDetector;
    private SwipeGestureListener mSwipeGestureListener;
    public static final int SWIPE_UP = 1;
    public static final int SWIPE_DOWN = 2;
    public static final int SWIPE_LEFT = 3;
    public static final int SWIPE_RIGHT = 4;

    public SFGestureHandler(Context context, View gestureView) {
        mGestureView = gestureView;
        mContext = context;
        mGestureDetector = new GestureDetector(mContext, this);
        registerTouchEvent(mGestureView);
    }

    @Override
    public boolean onFling(MotionEvent initialTouch, MotionEvent finalTouch, float velocityX,
                           float velocityY) {
        if (initialTouch.getY() - finalTouch.getY() > SWIPE_MIN_DISTANCE
                && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
            if (mSwipeGestureListener != null) {
                mSwipeGestureListener.onSwipe(SWIPE_UP);
            }
        }
        // left to right swipe
        else if (finalTouch.getY() - initialTouch.getY() > SWIPE_MIN_DISTANCE
                && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
            if (mSwipeGestureListener != null) {
                mSwipeGestureListener.onSwipe(SWIPE_DOWN);
            }
        } else if (initialTouch.getX() - finalTouch.getX() > SWIPE_MIN_DISTANCE
                && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
            if (mSwipeGestureListener != null) {
                mSwipeGestureListener.onSwipe(SWIPE_LEFT);
            }
        } else if (finalTouch.getX() - initialTouch.getX() > SWIPE_MIN_DISTANCE
                && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
            if (mSwipeGestureListener != null) {
                mSwipeGestureListener.onSwipe(SWIPE_RIGHT);
            }
        }

        SFLogger.debugLog("CustomGestureHandler", "OnFling() called");
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
                            float distanceY) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    public void setSwipeGestureListener(SwipeGestureListener swipeGestureListener) {
        mSwipeGestureListener = swipeGestureListener;
    }

    public interface SwipeGestureListener {
        void onSwipe(int direction);

    }

    public void registerTouchEvent(View parentView) {
        if (parentView == null) {
            return;
        }
        parentView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                mGestureDetector.onTouchEvent(event);
                return false;
            }
        });
        if (parentView instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) parentView).getChildCount(); i++) {
                View innerView = ((ViewGroup) parentView).getChildAt(i);
                registerTouchEvent(innerView);
            }
        }
    }
}



