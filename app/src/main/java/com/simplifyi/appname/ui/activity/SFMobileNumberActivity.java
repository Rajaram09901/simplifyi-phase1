package com.simplifyi.appname.ui.activity;

import android.Manifest;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.network.restclient.SFUserRestClient;
import com.simplifyi.appname.ui.viewmodel.MobileViewModel;
import com.simplifyi.appname.utility.SFDynamicPermissionManager;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

import java.util.ArrayList;
import java.util.List;

public class SFMobileNumberActivity extends SFBaseActivity {
    //public static final String EXTRAS_KEY_MOBILE_NO = "extras_key_mobile_no";
    //public static final String MOBILE_NO = "mobile_no";
    private AppCompatButton mContinueButton;
    public EditText mMobileNoEditText;
    public String s;
    public int c = 0;
    private int MY_PERMISSIONS_REQUEST_SMS_RECEIVE = 10;
    private MobileViewModel mMobileViewModel;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_number);
        checkPermission();
       ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.RECEIVE_SMS},
                MY_PERMISSIONS_REQUEST_SMS_RECEIVE);
        mMobileViewModel = ViewModelProviders.of(this).get(MobileViewModel.class);

        //what needs to happen when data changed or updated? ex: set text. write your logic to be executed on data changes
        //we can create as many no.of.observers as we want. all observing the same 'mobile number' (called as live data).
        //it is called live data coz, the live data object always ensures that the observers receive live data changes.
        //ex: the mobile no. is used 2 or more Views in the current screen, all of them can observe this data with separator observer.
        final Observer<String> mobileNumberObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //set the UI with this changes mobile number.
                //ex: mobTv.setText(s);
            }
        };

        //get an observable object that the view model contains.
        MutableLiveData<String> mobileNumberObservable = mMobileViewModel.getMobileNumber();

        mobileNumberObservable.observe(this, mobileNumberObserver);//both activity life cycle & data change is observed here.
        initializeUI();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_SMS_RECEIVE) {
            // YES!!
            //   Log.i("TAG", "MY_PERMISSIONS_REQUEST_SMS_RECEIVE --> YES");
        }
    }

    String imei = null;
    private void checkPermission() {

        if (SFDynamicPermissionManager.isAndroidMOrAbove()) {
            if (!SFDynamicPermissionManager.wasThePermissionGranted(this, Manifest.permission.READ_PHONE_STATE)) {
                List<String> permissions = new ArrayList<>();
                permissions.add(Manifest.permission.READ_PHONE_STATE);
                requestPermission(permissions, new PermissionCallback() {
                    @Override
                    public void onAllowTapped(List<String> permissions) {
                         imei = SFUtility.getDeviceIMEI(SFMobileNumberActivity.this);
                       // doLogin(loginListener, mobileNumber, imei1);
                    }

                    @Override
                    public void onDenyTapped(List<String> permissions) {

                    }

                    @Override
                    public void onDontAskChecked(List<String> permissions) {

                    }

                    @Override
                    public void onNotRequired() {

                    }
                });
            } else {
                imei = SFUtility.getDeviceIMEI(this);
             //   doLogin(loginListener, mobileNumber, imei);
            }
        } else {
            imei = SFUtility.getDeviceIMEI(this);
          //  doLogin(loginListener, mobileNumber, imei);
        }
    }

    private void initializeUI() {

        mMobileNoEditText = findViewById(R.id.enter_mobileNo);
        mContinueButton = findViewById(R.id.btn_continue);
        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SFUtility.isInternetAvailable(SFMobileNumberActivity.this)) {
                    createUser();
                } else {
                    showNoNetworkScreen();
                }
            }
        });


        mMobileNoEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mContinueButton.setEnabled(false);

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                disableContinue(!(charSequence != null && charSequence.length() == 10));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void disableContinue(boolean toBeDisabled) {
        if (toBeDisabled) {
            mContinueButton.setEnabled(false);
        } else {
            mContinueButton.setEnabled(true);
        }
    }

    /**getDeviceIMEI
     * create the user with the phone number & Imei
     */
    private void createUser() {
        showProgress(true);
        final String mobileNumber = mMobileNoEditText.getText().toString();
        /*final Bundle extras = new Bundle();
        extras.putString(MOBILE_NO, mobileNumber);*/

        //ask for read phone state permission
      //  String imei = SFUtility.getDeviceIMEI(this);
        SFUserRestClient userRestClient = new SFUserRestClient();
        final SFUser newUser = new SFUser();
        newUser.setPhone(Long.parseLong(mobileNumber));
        SFSharedPrefs.getInstance(SFMobileNumberActivity.this).putPhoneNumber(newUser.getPhone());
        newUser.setImeiNumber(imei);
        userRestClient.createUser(newUser, new SFUserRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                showProgress(false);
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    SFSharedPrefs.getInstance(SFMobileNumberActivity.this).putPhoneNumber(newUser.getPhone());
                    SFLogger.showToast(SFMobileNumberActivity.this, "Create user successful");
                    goToNextScreen(SFVerifyOTPActivity.class);
                    finish();
                } else if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.CREATE_USER_USER_ALREADY_EXIST) {
                    // SFLogger.showToast(SFMobileNumberActivity.this, "This user already exist. Trying to logging in!!");
                    // TODO: fetch current user info
                     goToNextScreen(SFVerifyOTPActivity.class);
                     finish();
                    /*getCurrentUser(new Runnable() {
                        @Override
                        public void run() {
                            SFUser currentUser = SFSharedPrefs.getInstance(SFMobileNumberActivity.this).getUser();
                            if (currentUser != null && currentUser.isMobileVerified()) {
                                login(new LoginListener() {
                                    @Override
                                    public void onSuccess() {
                                        //SFSharedPrefs.getInstance(SFMobileNumberActivity.this).putIsRegistered(true);
                                    }

                                    @Override
                                    public void onFail() {
                                        SFLogger.showToast(SFMobileNumberActivity.this, "Failed to login..Try later");
                                    }
                                }, newUser.getPhone());

                                resolveNavigateToDestination();
                            } else {
                                goToNextScreen(SFVerifyOTPActivity.class);
                            }
                            finish();
                        }
                    });*/
                } else {
                    SFLogger.showToast(SFMobileNumberActivity.this, "Create User unsuccessful");
                }

            }

            @Override
            public void onCallFinishedWithError(String error) {

            }
        });

    }
}
