package com.simplifyi.appname.ui.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.network.request.SFOTPRequestBody;
import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.network.restclient.SFOTPRestClient;
import com.simplifyi.appname.network.restclient.SFUserRestClient;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

public class SFVerifyOTPActivity extends SFBaseActivity {
    private AppCompatButton mContinueButton;
    private long mMobileNumber;
    private EditText mOTPEditText;
    private TextView mTimerTv;
    private CountDownTimer mCountDownTimer;
    private int MY_PERMISSIONS_REQUEST_SMS_RECEIVE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_otp);
        if (SFSharedPrefs.getInstance(this).getPhoneNumber() != SFSharedPrefs.INVALID_PHONE_NUMBER) {
            mMobileNumber = SFSharedPrefs.getInstance(this).getPhoneNumber();
        }
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.RECEIVE_SMS},
                MY_PERMISSIONS_REQUEST_SMS_RECEIVE);
        initializeUI();
        mTimerTv = findViewById(R.id.timerText);
        mCountDownTimer = new CountDownTimer(40 * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mTimerTv.setText("00:" + (millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                mTimerTv.setText("00:00");

            }
        };
        mCountDownTimer.start();

        findViewById(R.id.resend_otp_tv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: make create User API call as in Previous screen.
                createUser();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_SMS_RECEIVE) {
            // YES!!
            //   Log.i("TAG", "MY_PERMISSIONS_REQUEST_SMS_RECEIVE --> YES");
        }
    }

    private void initializeUI() {

        mOTPEditText = findViewById(R.id.eterotp);

        mContinueButton = findViewById(R.id.btn_continue);

        mContinueButton = findViewById(R.id.btn_continue);
        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SFUtility.isInternetAvailable(SFVerifyOTPActivity.this)) {
                    verifyOTP();
                } else {
                    showNoNetworkScreen();
                }
            }
        });


        mOTPEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                disableContinue(!(charSequence != null && charSequence.length() >= 4));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void disableContinue(boolean toBeDisabled) {
        if (toBeDisabled) {
            mContinueButton.setBackgroundResource(R.drawable.button_bg_rounded_corners);
            mContinueButton.setEnabled(false);
        } else {
            mContinueButton.setBackgroundResource(R.drawable.button_background_orange);
            mContinueButton.setEnabled(true);
        }
    }

    private void verifyOTP() {
        showProgress(true);
        String otp = mOTPEditText.getText().toString();
        if (otp != null && !otp.isEmpty()) {

            SFOTPRequestBody registrationBody = new SFOTPRequestBody(Integer.parseInt(otp), mMobileNumber);
            SFOTPRestClient manager = new SFOTPRestClient();
            manager.verifyOTP(registrationBody, new SFOTPRestClient.NetworkCallListener() {
                @Override
                public void onCallFinished(SFGenericResponse genericResponse) {
                    showProgress(false);
                    if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                        //     SFLogger.showToast(SFVerifyOTPActivity.this, "OTP verification successful");
                        //Call login API
                        login(new LoginListener() {
                            @Override
                            public void onSuccess() {
                                //  SFLogger.debugLog("Login successful");
                                //   SFLogger.showToast(SFVerifyOTPActivity.this, "Login successful");
                                getCurrentUser(new Runnable() {
                                    @Override
                                    public void run() {
                                        resolveNavigateToDestination();
                                        finish();
                                    }
                                });
                            }

                            @Override
                            public void onFail() {
                                SFLogger.debugLog("Login onFail");
                                SFLogger.showToast(SFVerifyOTPActivity.this, "Login onFail");
                            }
                        });
                    } else if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.VERIFY_OTP_ERROR_TIME_EXCEEDED) {
                        SFLogger.showToast(SFVerifyOTPActivity.this, "Time Exceeded. OTP verification unsuccessful");

                    } else {
                        SFLogger.showToast(SFVerifyOTPActivity.this, "Unknown Error. OTP verification unsuccessful");
                    }

                }

                @Override
                public void onCallFinishedWithError(String error) {
                    showProgress(false);
                    SFLogger.showToast(SFVerifyOTPActivity.this, "Unknown Error. OTP verification unsuccessful");
                }
            });
        }
    }

    private void createUser() {
        showProgress(true);
        //ask for read phone state permission
        String imei = SFUtility.getDeviceIMEI(this);
        SFUserRestClient userRestClient = new SFUserRestClient();
        final SFUser newUser = new SFUser();
        newUser.setPhone(mMobileNumber);
        SFSharedPrefs.getInstance(this).putPhoneNumber(newUser.getPhone());
        newUser.setImeiNumber(imei);

        userRestClient.createUser(newUser, new SFUserRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                showProgress(false);
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    SFSharedPrefs.getInstance(SFVerifyOTPActivity.this).putPhoneNumber(newUser.getPhone());
                    SFLogger.showToast(SFVerifyOTPActivity.this, "Resent OTP Successfully");
                    mCountDownTimer.start();
                } else if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.CREATE_USER_USER_ALREADY_EXIST) {
                    // SFLogger.showToast(SFVerifyOTPActivity.this, "This user already exist. Trying to logging in!!");
                    // TODO: fetch current user info
                    // login(newUser);

                } else {
                    SFLogger.showToast(SFVerifyOTPActivity.this, "Create User unsuccessful");
                }

            }

            @Override
            public void onCallFinishedWithError(String error) {

            }
        });
    }

    void login(SFUser newUser) {
        login(new LoginListener() {
            @Override
            public void onSuccess() {
                getCurrentUser(new Runnable() {
                    @Override
                    public void run() {
                        SFUser currentUser = SFSharedPrefs.getInstance(SFVerifyOTPActivity.this).getUser();
                        if (currentUser != null && currentUser.isMobileVerified()) {
                            resolveNavigateToDestination();
                        } else {
                            goToNextScreen(SFVerifyOTPActivity.class);
                        }
                        finish();
                    }
                });
            }

            @Override
            public void onFail() {
                SFLogger.showToast(SFVerifyOTPActivity.this, "Failed to login..Try later");
            }
        }, newUser.getPhone());
    }


    @Override
    public void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase("otp")) {
                final String message = intent.getStringExtra("message");
                mOTPEditText.setText(message);
                mContinueButton.performClick();
                //Do whatever you want with the code here
            }
        }
    };
}

