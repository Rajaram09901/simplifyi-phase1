package com.simplifyi.appname.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

public class SFProfileActivity extends SFBaseAppBarActivity {
    private SFUser mCurrentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        configureActionBar(true, "Profile");
        loadUser();
        initializeUI();
    }

    private void loadUser() {
        showProgress(true);
        getCurrentUser(new Runnable() {
            @Override
            public void run() {
                showProgress(false);
                // TODO: load user object from sharedprefs
                mCurrentUser = SFSharedPrefs.getInstance(SFProfileActivity.this).getUser();
                initializeUI();
            }
        });
    }

    private void initializeUI() {
        findViewById(R.id.edit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SFProfileActivity.this, SFEditUserActivity.class);
                intent.putExtra(SFEditUserActivity.EDIT_PROFILE_FLOW, SFEditUserActivity.EDIT_PROFILE_FLOW_EDIT);
                goToNextScreen(intent);
            }
        });
        ImageView mProfilePicIv = findViewById(R.id.profile_pic_iv);
        if (mCurrentUser != null && !SFUtility.isStringNullOrEmpty(mCurrentUser.getProfileImageUrl())) {
            Glide.with(this).load(mCurrentUser.getProfileImageUrl()).into(mProfilePicIv);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_screen_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                goToNextScreen(SFUserSettingsActivity.class);
                break;
            case R.id.tnc:
                goToNextScreen(SFTermsAndConditionsActivity.class);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}


