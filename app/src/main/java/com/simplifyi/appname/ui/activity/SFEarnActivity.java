package com.simplifyi.appname.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.simplifyi.appname.utility.SFUIUtility;
import com.simplifyi.simplifyi_phase2.R;
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

public class SFEarnActivity extends SFBaseAppBarActivity {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earn);

        configureActionBar(true, "Earn");
        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setClipToPadding(false);
        viewPager.setPadding((int) SFUIUtility.convertDpToPixel(40, this), 0, (int) SFUIUtility.convertDpToPixel(40, this), 0);
        viewPager.setPageMargin((int) SFUIUtility.convertDpToPixel(10, this));
        viewPager.setAdapter(new CustomPagerAdapter(this));

        SpringDotsIndicator dotsIndicator = findViewById(R.id.spring_dots_indicator);
        dotsIndicator.setViewPager(viewPager);

        initToolbar();
    }

    private void initToolbar() {
        getSupportActionBar().setTitle("Earn");
    }

    public class CustomPagerAdapter extends PagerAdapter {

        private Context mContext;

        public CustomPagerAdapter(Context context) {
            mContext = context;
        }

        @Override
        public Object instantiateItem(ViewGroup collection, final int position) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.item_earn_layout, collection, false);

            ImageView imgEarnMoney = layout.findViewById(R.id.img_earn_money);
            imgEarnMoney.setImageResource(position == 0 ? R.drawable.ic_money_earn : R.drawable.ic_group_earn);

            TextView textEarnTitle = layout.findViewById(R.id.text_earn_title);
            textEarnTitle.setText(position == 0 ? R.string.consultant_title : R.string.lead_business_title);

            TextView textEarnDesc = layout.findViewById(R.id.text_earn_desc);
            textEarnDesc.setText(position == 0 ? R.string.consultant_desc : R.string.lead_business_desc);

//            layout.findViewById(R.id.btn_register_now).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(SFEarnActivity.this, RegisterBusinessActivity.class);
//                    intent.putExtra(RegisterBusinessActivity.KEY_EXTRA_REGISTER, position + 1);
//                    startActivity(intent);
//                }
//            });
            collection.addView(layout);
            return layout;
        }

        @Override
        public void destroyItem(ViewGroup collection, int position, Object view) {
            collection.removeView((View) view);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }

}