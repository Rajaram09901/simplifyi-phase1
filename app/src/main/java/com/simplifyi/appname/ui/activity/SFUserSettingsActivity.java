package com.simplifyi.appname.ui.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;

import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.network.request.SFUserSettingsRequest;
import com.simplifyi.appname.ui.viewmodel.SFUserViewModel;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.simplifyi_phase2.R;

public class SFUserSettingsActivity extends SFBaseAppBarActivity implements CompoundButton.OnCheckedChangeListener {
    private SFUser mCurrentUser;
    private boolean mSettingsModified;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        configureActionBar(false, "Privacy Settings");
        showProgress(true);
        getCurrentUser(new Runnable() {
            @Override
            public void run() {
                initializeUI();
                showProgress(false);
            }
        });
        findViewById(R.id.tnc_tv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToNextScreen(SFTermsAndConditionsActivity.class);

            }
        });

        findViewById(R.id.logout_tv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SFSharedPrefs.getInstance(getApplicationContext()).clearPrefs();

                Intent mIntent = new Intent(SFUserSettingsActivity.this, SFMobileNumberActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(mIntent);
            }
        });
    }

    private void initializeUI() {
        mCurrentUser = SFSharedPrefs.getInstance(this).getUser();
        SwitchCompat newCallSwitch = findViewById(R.id.notification_new_call_switch);
        SwitchCompat newRepliesSwitch = findViewById(R.id.notification_new_replies_switch);
        SwitchCompat newPostsSwitch = findViewById(R.id.notification_new_posts_switch);
        SwitchCompat antiSpamSwitch = findViewById(R.id.spam_anti_spam_switch);
        SwitchCompat sameGenderSwitch = findViewById(R.id.spam_same_gender_switch);
        if (mCurrentUser != null) {
            if (mCurrentUser.getSettings() != null && mCurrentUser.getSettings().getNotifications() != null) {
                newCallSwitch.setChecked(mCurrentUser.getSettings().getNotifications().isCalls());
                newRepliesSwitch.setChecked(mCurrentUser.getSettings().getNotifications().isMessages());
                newPostsSwitch.setChecked(mCurrentUser.getSettings().getNotifications().isNewforums());
            }
            if (mCurrentUser.getSettings() != null) {
                antiSpamSwitch.setChecked(mCurrentUser.getSettings().isAntiSpam());
                sameGenderSwitch.setChecked(mCurrentUser.getSettings().isSamegender());
            }
        }

        newCallSwitch.setOnCheckedChangeListener(this);
        newRepliesSwitch.setOnCheckedChangeListener(this);
        newPostsSwitch.setOnCheckedChangeListener(this);
        antiSpamSwitch.setOnCheckedChangeListener(this);
        sameGenderSwitch.setOnCheckedChangeListener(this);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        mSettingsModified = true;
        updateSettings();
    }

    private void updateSettings() {
        final SFUserSettingsRequest request = new SFUserSettingsRequest();
        if (mCurrentUser != null) {
            SwitchCompat newCallSwitch = findViewById(R.id.notification_new_call_switch);
            request.setCalls(newCallSwitch.isChecked());
            SwitchCompat newRepliesSwitch = findViewById(R.id.notification_new_replies_switch);
            request.setMessages(newRepliesSwitch.isChecked());
            SwitchCompat newPostsSwitch = findViewById(R.id.notification_new_posts_switch);
            request.setNewforums(newPostsSwitch.isChecked());
            SwitchCompat antiSpamSwitch = findViewById(R.id.spam_anti_spam_switch);
            request.setAntiSpam(antiSpamSwitch.isChecked());
            SwitchCompat sameGenderSwitch = findViewById(R.id.spam_same_gender_switch);
            request.setSamegender(sameGenderSwitch.isChecked());
        }

        final SFUserViewModel userViewModel = ViewModelProviders.of(this).get(SFUserViewModel.class);
        Observer<Boolean> forumUserObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean successful) {

            }
        };
        Observer<Integer> forumUserErrorObserver = new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer error) {
                if (error == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    login(new LoginListener() {
                        @Override
                        public void onSuccess() {
                            String token = SFSharedPrefs.getInstance(SFUserSettingsActivity.this).getSessionToken();
                            userViewModel.updateUserSettings(request, token);
                        }

                        @Override
                        public void onFail() {
                            SFLogger.showToast(SFUserSettingsActivity.this, "save settings failed");
                        }
                    });

                } else {
                    SFLogger.showToast(SFUserSettingsActivity.this, "uknown error in saving settings");
                }
            }
        };
        userViewModel.getmUserSettingsUpdatedContainer().observe(this, forumUserObserver);
        userViewModel.getErrorContainer().observe(this, forumUserErrorObserver);
        String token = SFSharedPrefs.getInstance(this).getSessionToken();
        userViewModel.updateUserSettings(request, token);

    }

    @Override
    protected void onPause() {
        super.onPause();

        //TODO: referesh locally stored user object
        if (mSettingsModified) {
            mSettingsModified = false;
            getCurrentUser(null);
        }
    }

    @Override
    public void onBackPressed() {
        finish();

    }
}
