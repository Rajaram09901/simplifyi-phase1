package com.simplifyi.appname.ui.activity;

import android.Manifest;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.model.SFSinchManager;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.ui.viewmodel.SFCallViewModel;
import com.simplifyi.appname.utility.SFDynamicPermissionManager;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

import java.util.ArrayList;
import java.util.List;

public class SFFindingBuddyActivity extends SFBaseActivity {
    public Button mStopSearchingButton;
    public ImageView buddy;
    private SFForum mCallableForum;
    private SFSinchManager.SFIncomingCallListener mSFIncomingCallListener;
    private boolean mInCall = false;
    private List<String> mPendingPermissions = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finding_buddy);
        mCallableForum = (SFForum) getIntent().getSerializableExtra(SFInCallActivity.CALLABLE_FORUM);
        if (mCallableForum != null) {
            initializeUI();
        } else {
            SFLogger.showToast(this, "Callable Forum is null");
        }
        checkCallingPermissions(null);
    }

    private void initializeUI() {
        TextView forumQuestionTv = findViewById(R.id.forum_question);
        forumQuestionTv.setText(mCallableForum.getName());
        TextView primaryTagTv = findViewById(R.id.textViewPrimaryTag);
        if (mCallableForum.getTags() != null && !mCallableForum.getTags().isEmpty()) {
            primaryTagTv.setText(mCallableForum.getTags().get(0));
            TextView secondaryTagTv = findViewById(R.id.textViewSecondaryTag);
            secondaryTagTv.setText(mCallableForum.getTags().get(1));
        }
        ImageView profileIv = findViewById(R.id.owner_profile_pic);
        if (mCallableForum.getForumUsers() != null && !mCallableForum.getForumUsers().isEmpty() && mCallableForum.getForumUsers().get(0) != null) {
            SFForum.ForumUser user = mCallableForum.getForumUsers().get(0);
            if (user != null && !SFUtility.isStringNullOrEmpty(user.getProfileImageUrl())) {
                Glide.with(this).load(user.getProfileImageUrl()).into(profileIv);
            }
            TextView userNameTv = findViewById(R.id.forum_owner_name_tv);
            userNameTv.setText(user.getNickName());
        }
        TextView followTv = findViewById(R.id.follow_tv);
        followTv.setText(mCallableForum.getFollows() + " Follows");

        mStopSearchingButton = findViewById(R.id.stop_searching_button);
        buddy = findViewById(R.id.buddy);
        mStopSearchingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopListeningIncomingCall();
                updateDisconnectStatusInServer();
            }
        });
    }

    public void back(View v) {
        stopListeningIncomingCall();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mInCall) {
            if (mPendingPermissions.isEmpty()) {
                startListeningToIncomingCalls();
            } else {
                checkCallingPermissions(new Runnable() {
                    @Override
                    public void run() {
                        startListeningToIncomingCalls();
                    }
                });
            }
        }
        mInCall = false;
    }

    @Override
    public void onBackPressed() {
        stopListeningIncomingCall();
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!mInCall) {
            stopListeningIncomingCall();
        }
    }

    private void startListeningToIncomingCalls() {
        if (mSFIncomingCallListener == null) {
            mSFIncomingCallListener = new SFSinchManager.SFIncomingCallListener() {
                @Override
                public void onIncomingCall() {
                    Intent intent = new Intent(SFFindingBuddyActivity.this, SFInCallActivity.class);
                    intent.putExtra(SFInCallActivity.CALL_TYPE, SFInCallActivity.CALL_TYPE_INCOMING);
                    intent.putExtra(SFInCallActivity.CALLABLE_FORUM, mCallableForum);
                    mInCall = true;
                    goToNextScreen(intent);
                }
            };
        }
        SFSinchManager.getInstance().registerForIncomingCallNotification(mSFIncomingCallListener, this);
    }

    private void stopListeningIncomingCall() {
        if (mSFIncomingCallListener != null) {
            SFSinchManager.getInstance().unRegisterForIncomingCallNotification(mSFIncomingCallListener);
        }
    }

    private void getPendingPermissions() {
        mPendingPermissions.clear();
        if (!SFDynamicPermissionManager.wasThePermissionGranted(this, Manifest.permission.RECORD_AUDIO)) {
            mPendingPermissions.add(Manifest.permission.RECORD_AUDIO);
        }
        if (!SFDynamicPermissionManager.wasThePermissionGranted(this, Manifest.permission.MODIFY_AUDIO_SETTINGS)) {
            mPendingPermissions.add(Manifest.permission.MODIFY_AUDIO_SETTINGS);
        }
        if (!SFDynamicPermissionManager.wasThePermissionGranted(this, Manifest.permission.ACCESS_NETWORK_STATE)) {
            mPendingPermissions.add(Manifest.permission.ACCESS_NETWORK_STATE);
        }

    }

    private void checkCallingPermissions(final Runnable taskToExecuteAfterPermission) {
        if (SFDynamicPermissionManager.isAndroidMOrAbove()) {
            getPendingPermissions();
            if (mPendingPermissions.isEmpty()) {
                return;
            }
            requestPermission(mPendingPermissions, new PermissionCallback() {
                @Override
                public void onAllowTapped(List<String> permissions) {
                    getPendingPermissions();
                    if (mPendingPermissions.isEmpty()) {
                        if (taskToExecuteAfterPermission != null) {
                            taskToExecuteAfterPermission.run();
                        }
                    } else {
                        SFLogger.showToast(SFFindingBuddyActivity.this, "one or more permissions are still missing");
                    }
                }

                @Override
                public void onDenyTapped(List<String> permissions) {
                    SFLogger.showToast(SFFindingBuddyActivity.this, permissions.size() + " mPendingPermissions denied.You will not receive sinch calls");
                }

                @Override
                public void onDontAskChecked(List<String> permissions) {
                    SFLogger.showToast(SFFindingBuddyActivity.this, permissions.size() + "mPendingPermissions Don't ask checked.You will not receive sinch calls");
                }

                @Override
                public void onNotRequired() {
              //      SFLogger.showToast(SFFindingBuddyActivity.this, "Permission Not required");
                }
            });
        }
    }

    private void updateDisconnectStatusInServer() {
        final SFCallViewModel callViewModel = ViewModelProviders.of(this).get(SFCallViewModel.class);
        Observer<Boolean> disconnectCallObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean hasDisconnectAPISucceeded) {
                if (hasDisconnectAPISucceeded) {
                    SFLogger.showToast(SFFindingBuddyActivity.this, "Disconnect call is successful");
                    finish();
                } else {
                    SFLogger.showToast(SFFindingBuddyActivity.this, "Disconnect call is failed");
                }
            }
        };
        Observer<Integer> forumUserErrorObserver = new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer error) {
                if (error == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    login(new LoginListener() {
                        @Override
                        public void onSuccess() {
                            String token = SFSharedPrefs.getInstance(SFFindingBuddyActivity.this).getSessionToken();
                            callViewModel.disconnectTheCall(token, mCallableForum.getId());
                        }

                        @Override
                        public void onFail() {
                            SFLogger.showToast(SFFindingBuddyActivity.this, "Disconnect call is failed");
                        }
                    });

                } else {
                    SFLogger.showToast(SFFindingBuddyActivity.this, "unknown error while trying to disconnect the call");
                }
            }
        };
        callViewModel.getWasTheCallDisconnectedContainer().observe(this, disconnectCallObserver);
        callViewModel.getErrorContainer().observe(this, forumUserErrorObserver);
        String token = SFSharedPrefs.getInstance(this).getSessionToken();
        if (mCallableForum != null) {
            callViewModel.disconnectTheCall(token, mCallableForum.getId());
        }
    }
}
