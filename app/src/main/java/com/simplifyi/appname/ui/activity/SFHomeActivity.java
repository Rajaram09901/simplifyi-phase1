package com.simplifyi.appname.ui.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.model.SFSinchManager;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.ui.adapter.SFForumItemAdapter;
import com.simplifyi.appname.ui.customview.SFSwipeableForumCallingCard;
import com.simplifyi.appname.ui.customview.SFSwipeableForumCallingCard.UserAction;
import com.simplifyi.appname.ui.viewmodel.SFForumViewModel;
import com.simplifyi.appname.utility.SFAnalytics;
import com.simplifyi.appname.utility.SFDynamicPermissionManager;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUIUtility;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;
import com.skyfishjy.library.RippleBackground;


import java.util.ArrayList;
import java.util.List;

public class SFHomeActivity extends SFBottomNavigationActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    private static final long CALLABLE_FORUM_LIST_REFRESH_DELAY = 10 * 1000;//10 secs


    private RecyclerView mRecyclerView;
    private List<SFForum> mForumList;
    private List<SFForum> mCallableForumList;
    private SFForumViewModel mForumViewModel;
    private SFForumItemAdapter mForumListAdapter;
    private SwipePlaceHolderView mSwipeView;
    private Runnable mRefreshCallableForumsTask;
    private boolean mInCall = false;
    private List<String> mPendingPermissions = new ArrayList<>();
    private Spinner healthcare_spinner, language_spinner;
    private TextView textLocation;
     RippleBackground rippleBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initializeUI();
        initializeSpinners();

        if (SFUtility.isInternetAvailable(this)) {
            loadForums(false);
        } else {
            showNoNetworkScreen();
        }
        //checkCallingPermissions(null);
        SFAnalytics.getInstance(this).setCurrentScreen(this, "Home Screen");
    }


    private void initializeUI() {
        //  initializeBottomNavigationBar();
        rippleBackground= findViewById(R.id.content);
        rippleBackground.startRippleAnimation();
        initializeBottomSheet();
        initializeForumViewModel();
        initializeForumListUI();
         rippleBackground= findViewById(R.id.content);
        rippleBackground.startRippleAnimation();
        findViewById(R.id.settings_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToNextScreen(SFTagSelectionActivity.class);
            }
        });

        ViewCompat.setNestedScrollingEnabled(mRecyclerView, false);
    }

    private void initializeSwipePlaceHolderView() {
        mSwipeView = findViewById(R.id.swipe_view);
        int bottomMargin = SFUIUtility.dpToPx(160);
        Point windowSize = SFUIUtility.getDisplaySize(getWindowManager());
        mSwipeView.getBuilder()
                .setDisplayViewCount(3)
                .setIsUndoEnabled(true)
                .setHeightSwipeDistFactor(10)
                .setWidthSwipeDistFactor(5)
                .setSwipeDecor(new SwipeDecor()
                        .setViewWidth(windowSize.x)
                        .setViewHeight(windowSize.y - bottomMargin)
                        .setViewGravity(Gravity.TOP)
                        .setPaddingTop(20)
                        .setRelativeScale(0.01f)
                        .setSwipeMaxChangeAngle(2f)
                        .setSwipeInMsgLayoutId(R.layout.dashboard_swipe_in_msg_card)
                        .setSwipeOutMsgLayoutId(R.layout.dashboard_swipe_out_msg_card));
    }


    private void initializeBottomSheet() {
        findViewById(R.id.pullup_anchor_layout).setVisibility(View.VISIBLE);
        findViewById(R.id.filter_layout).setVisibility(View.GONE);
        View bottomSheet = findViewById(R.id.bottom_sheet);
        BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bottomSheetBehavior.setHideable(false);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                switch (i) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        // SFLogger.showToast(SFHomeActivity.this, "Bottom sheet hidden");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        //SFLogger.showToast(SFHomeActivity.this, "Bottom sheet expanded");

                        findViewById(R.id.pullup_anchor_layout).setVisibility(View.GONE);
                        findViewById(R.id.filter_layout).setVisibility(View.VISIBLE);
                        findViewById(R.id.settings_button).setVisibility(View.GONE);
                        findViewById(R.id.bottom_sheet).setBackgroundColor(getResources().getColor(R.color.semi_transparent));
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        // SFLogger.showToast(SFHomeActivity.this, "Bottom sheet collapsed");
                        findViewById(R.id.pullup_anchor_layout).setVisibility(View.VISIBLE);
                        findViewById(R.id.filter_layout).setVisibility(View.GONE);
                        findViewById(R.id.settings_button).setVisibility(View.VISIBLE);
                        findViewById(R.id.bottom_sheet).setBackgroundColor(getResources().getColor(R.color.transparent));
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        //SFLogger.showToast(SFHomeActivity.this, "Bottom sheet dragging");
//                        findViewById(R.id.bottom_sheet).setBackground(getResources().getDrawable(R.drawable.all_window_background));
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        //SFLogger.showToast(SFHomeActivity.this, "Bottom sheet setting");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
    }


    private void initializeForumListUI() {
        mRecyclerView = findViewById(R.id.forum_recycler_view);
        findViewById(R.id.collapse_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { //TODO: Do we still need this? Bottom sheet has not replaced it?? Revisit later
                finish();
                overridePendingTransition(R.anim.no_change, R.anim.slide_down_info);
            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mForumList = new ArrayList<>();
        mForumListAdapter = new SFForumItemAdapter(this, mForumList);
        mRecyclerView.setAdapter(mForumListAdapter);
    }

    public void refreshForumList() {
        if (SFUtility.isForumListNullOREmpty(mForumList)) {
            //    SFLogger.showToast(this, "Forum List is empty");
        }
        // SFLogger.showToast(this, "Refreshing Forum List");
        mForumListAdapter.setData(mForumList);
        mForumListAdapter.notifyDataSetChanged();
    }

    private void refreshCallableForumList(List<SFForum> newCallableForums) {

        if (SFUtility.isForumListNullOREmpty(newCallableForums)) {
            SFLogger.debugLog("RefreshDashboard", "New list is empty. so clear dashboard");
            if (!SFUtility.isForumListNullOREmpty(mCallableForumList)) {
                mCallableForumList.clear();
            }
            refreshSwipePlaceHolderView();
            return;
        }
        if (SFUtility.isForumListNullOREmpty(mCallableForumList)) {
            mCallableForumList = newCallableForums;
            refreshSwipePlaceHolderView();
            return;
        }

        List<SFForum> toBeRemoved = new ArrayList<>();
        List<SFForum> toBeAdded = new ArrayList<>();
        for (SFForum newForum : newCallableForums) {
            if (!mCallableForumList.contains(newForum)) {
                toBeAdded.add(newForum);
            }
        }

        for (SFForum oldForum : mCallableForumList) {
            if (!newCallableForums.contains(oldForum)) {
                toBeRemoved.add(oldForum);
            }
        }

        if (toBeRemoved.isEmpty() && toBeAdded.isEmpty()) {//no change
            SFLogger.debugLog("RefreshDashboard", "No change in the new list. so no refresh to dashboard");
            refreshSwipePlaceHolderView();
            return;
        }
        SFLogger.debugLog("RefreshDashboard", "No.of new items to be added to dashboard:" + toBeAdded.size());
        SFLogger.debugLog("RefreshDashboard", "No.of new items to be removed from dashboard:" + toBeRemoved.size());
        if (!toBeRemoved.isEmpty()) {
            mCallableForumList.removeAll(toBeRemoved);
        }
        if (!toBeAdded.isEmpty()) {
            synchronized (SFHomeActivity.this) {
                mCallableForumList.addAll(toBeAdded);
            }
        }
        refreshSwipePlaceHolderView();

    }

    private void refreshSwipePlaceHolderView() {
        if (mSwipeView == null) {
            initializeSwipePlaceHolderView();
        }
        if (SFUtility.isForumListNullOREmpty(mCallableForumList)) {
            mSwipeView.removeAllViews();
            return;
        }
        SFSwipeableForumCallingCard SFSwipeableForumCallingCard;
        mSwipeView.removeAllViews();//is it not working?
        for (final SFForum callableForum : mCallableForumList) {
            SFSwipeableForumCallingCard = new SFSwipeableForumCallingCard(getApplicationContext(), callableForum, mSwipeView);
            SFSwipeableForumCallingCard.setUserActionListener(new SFSwipeableForumCallingCard.UserActionListener() {
                @Override
                public void onUserAction(SFForum forum, final int userAction) {
                    if (userAction == UserAction.SWIPE_RIGHT || userAction == UserAction.BUTTON_ANSWER) {
                        onAnswerTapped(forum);
                    } else {
                        onDeclineCallTapped(forum);
                    }
                }
            });
            mSwipeView.addView(SFSwipeableForumCallingCard);
        }

    }

    private void getPendingPermissions() {
        mPendingPermissions.clear();
        if (!SFDynamicPermissionManager.wasThePermissionGranted(this, Manifest.permission.RECORD_AUDIO)) {
            mPendingPermissions.add(Manifest.permission.RECORD_AUDIO);
        }
        if (!SFDynamicPermissionManager.wasThePermissionGranted(this, Manifest.permission.MODIFY_AUDIO_SETTINGS)) {
            mPendingPermissions.add(Manifest.permission.MODIFY_AUDIO_SETTINGS);
        }
        if (!SFDynamicPermissionManager.wasThePermissionGranted(this, Manifest.permission.ACCESS_NETWORK_STATE)) {
            mPendingPermissions.add(Manifest.permission.ACCESS_NETWORK_STATE);
        }
    }

    public void checkCallingPermissions(final Runnable postPermissionTask) {
        if (SFDynamicPermissionManager.isAndroidMOrAbove()) {
            getPendingPermissions();
            if (mPendingPermissions.isEmpty()) {
                if (postPermissionTask != null) {
                    postPermissionTask.run();
                }
                return;
            }
            requestPermission(mPendingPermissions, new PermissionCallback() {
                @Override
                public void onAllowTapped(List<String> permissions) {
//                    SFLogger.showToast(SFHomeActivity.this, permissions.size() + " permissions granted");
                    getPendingPermissions();
                    if (mPendingPermissions.isEmpty() && postPermissionTask != null) {
                        postPermissionTask.run();
                    }
                }

                @Override
                public void onDenyTapped(List<String> permissions) {
                    SFLogger.showToast(SFHomeActivity.this, permissions.size() + " permissions denied");
                }

                @Override
                public void onDontAskChecked(List<String> permissions) {
                    //        SFLogger.showToast(SFHomeActivity.this, permissions.size() + "permissions Don't ask checked");
                }

                @Override
                public void onNotRequired() {
                    //    SFLogger.showToast(SFHomeActivity.this, "Permission Not required");
                }
            });
        }
    }

    private void onAnswerTapped(final SFForum forum) {
        checkCallingPermissions(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SFHomeActivity.this, SFInCallActivity.class);
                intent.putExtra(SFInCallActivity.CALL_TYPE, SFInCallActivity.CALL_TYPE_OUTGOING);
                intent.putExtra(SFInCallActivity.CALLABLE_FORUM, forum);
                mInCall = true;
                goToNextScreen(intent);
            }
        });

    }

    private void onDeclineCallTapped(SFForum forum) {
        mCallableForumList.remove(forum);
    }

    private void initializeForumViewModel() {
        mForumViewModel = ViewModelProviders.of(this).get(SFForumViewModel.class);
        final Observer<List<SFForum>> forumListObserver = new Observer<List<SFForum>>() {
            @Override
            public void onChanged(@Nullable List<SFForum> forums) {
                mForumList = forums;
                showProgress(false);
                refreshForumList();
            }
        };

        final Observer<List<SFForum>> forumCallableListObserver = new Observer<List<SFForum>>() {
            @Override
            public void onChanged(@Nullable List<SFForum> callableForums) {
                showProgress(false);

                if(callableForums.size()>0){
                    scheduleCallableForumListRefresh(false);
                    refreshCallableForumList(callableForums);
                    rippleBackground.setVisibility(View.GONE);
                    rippleBackground.stopRippleAnimation();
                    findViewById(R.id.parent_view).setBackgroundResource(R.drawable.all_window_background);

                }else{
                    findViewById(R.id.parent_view).setBackgroundResource(R.color.white);
                }

            }
        };

        final Observer<Integer> errorInLoadingForums = new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer errorCode) {
                if (errorCode == SFAPIResponseCode.Error.FETCH_FORUM_ERROR_TOKEN_EXPIRED) {
                    login(new LoginListener() {
                        @Override
                        public void onSuccess() {
                            loadForums(false);
                        }

                        @Override
                        public void onFail() {
                            showProgress(false);
                            //refreshForumList();
                        }
                    });
                } else {
                    showProgress(false);
                    refreshForumList();
                }
            }
        };

        final Observer<Integer> errorInLoadingCallableForums = new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer errorCode) {
                if (errorCode == SFAPIResponseCode.Error.FETCH_FORUM_ERROR_TOKEN_EXPIRED) {
                    login(new LoginListener() {
                        @Override
                        public void onSuccess() {
                            loadForums(true);
                        }

                        @Override
                        public void onFail() {
                            showProgress(false);
                        }
                    });
                } else {
                    showProgress(false);
                    //refreshCallableForumList();
                    scheduleCallableForumListRefresh(false);
                }
            }
        };

        mForumViewModel.getForumListContainer().observe(this, forumListObserver);
        mForumViewModel.getErrorInForumLoading().observe(this, errorInLoadingForums);

        mForumViewModel.getCallableForumLisContainer().observe(this, forumCallableListObserver);
        mForumViewModel.getErrorInCallableForumLoading().observe(this, errorInLoadingCallableForums);
    }

    private void loadForums(final boolean getCallableForums) {
        if (getCallableForums) {
            SFLogger.debugLog("RefreshDashboard", "triggering load callable forums");
        }
        if (!getCallableForums) {
            showProgress(true);
        }
        String token = SFSharedPrefs.getInstance(this).getSessionToken();
        if (SFUtility.isStringNullOrEmpty(token)) {
            login(new SFBaseActivity.LoginListener() {
                @Override
                public void onSuccess() {
                    String token = SFSharedPrefs.getInstance(SFHomeActivity.this).getSessionToken();
                    mForumViewModel.loadForums(getCallableForums, token);
                }

                @Override
                public void onFail() {
                    //TODO:login may file with the error user doesn't exist. in this case, the user may have been removed by other means. how do client know about that?
                    SFLogger.showToast(SFHomeActivity.this, "Login onFail.So, not able to load forums");
                }
            });
        } else {
            mForumViewModel.loadForums(getCallableForums, token);
        }
    }

    private void scheduleCallableForumListRefresh(boolean immediate) {
        SFLogger.debugLog("RefreshDashboard", "Scheduling Load Callable Forums list");
        if (mRefreshCallableForumsTask == null) {
            mRefreshCallableForumsTask = new Runnable() {
                @Override
                public void run() {
                    loadForums(true);
                }
            };
        }
        if (mRecyclerView != null) {
            long delay = immediate ? 0 : CALLABLE_FORUM_LIST_REFRESH_DELAY;
            mRecyclerView.postDelayed(mRefreshCallableForumsTask, delay);
        }
    }

    private void unScheduleCallableForumListRefresh() {
        if (mRecyclerView != null && mRefreshCallableForumsTask != null) {
            mRecyclerView.removeCallbacks(mRefreshCallableForumsTask);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mInCall) {
            getPendingPermissions();
            if (mPendingPermissions.isEmpty()) {
                SFSinchManager.getInstance().enableOutGoingCall(this);
            } else {

                checkCallingPermissions(new Runnable() {
                    @Override
                    public void run() {
                        SFSinchManager.getInstance().enableOutGoingCall(SFHomeActivity.this);
                    }
                });
            }
        }
        mInCall = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!mInCall) {//TODO: remove the second part when the testing completes
            SFSinchManager.getInstance().disableOutGoingCall();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //   bottomNavigationView.setSelectedItemId(R.id.simplify);
        scheduleCallableForumListRefresh(true);

        // List<SFUser> users = SFMockDataProvider.getInstance(this.getApplicationContext()).getAllUsers();//MOck test
    }

    @Override
    protected void onPause() {
        super.onPause();
        unScheduleCallableForumListRefresh();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SFSinchManager.getInstance().shutDownSinch();
    }


    void initializeSpinners() {

        textLocation = findViewById(R.id.text_location);
        textLocation.setOnClickListener(this);
        // Spinner element
        healthcare_spinner = findViewById(R.id.healthcare_spinner);
        language_spinner = findViewById(R.id.language_spinner);

        // Spinner click listener
        healthcare_spinner.setOnItemSelectedListener(this);
        language_spinner.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Travel");
        categories.add("Business Services");
        categories.add("Computers");
        categories.add("Education");
        categories.add("Personal");
        categories.add("Happpy");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        healthcare_spinner.setAdapter(dataAdapter);

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.languages));

        // Drop down layout style - list view with radio button
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        language_spinner.setAdapter(dataAdapter1);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
        //   Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are you sure do you want to exit?").setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        builder.show();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, SFLocationActivity.class);
        startActivityForResult(intent, 9);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 9 && resultCode == RESULT_OK) {
            String latLong = data.getStringExtra(SFLocationActivity.KEY_LATLONG);
            String address = data.getStringExtra(SFLocationActivity.KEY_ADDRESS);
            textLocation.setText(address);
        }
    }
}


