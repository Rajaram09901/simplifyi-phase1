package com.simplifyi.appname.ui.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.ui.adapter.SFForumItemAdapter;
import com.simplifyi.appname.ui.viewmodel.SFForumViewModel;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

import java.util.ArrayList;
import java.util.List;

public class SFForumListActivity extends SFBaseActivity implements AdapterView.OnItemSelectedListener {
    private RecyclerView mRecyclerView;
    private List<SFForum> mForumList;
    private SFForumViewModel mForumListViewModel;
    private SFForumItemAdapter mForumListAdapter;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bottomsheet_forum_list);
        initializeViewModel();
        initializeUI();

        if (SFUtility.isInternetAvailable(this)) {
            loadForumsForCurrentUser();
        } else {
            showNoNetworkScreen();
        }


    }

    private void initializeUI() {

        mRecyclerView = findViewById(R.id.forum_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mForumList = new ArrayList<>();
        mForumListAdapter = new SFForumItemAdapter(this, mForumList);
        mRecyclerView.setAdapter(mForumListAdapter);

    }

       public void refreshUI() {
        if(mForumList==null || mForumList.isEmpty()){
            SFLogger.showToast(this,"Forum List is empty");
        }
        mForumListAdapter.setData(mForumList);
        mForumListAdapter.notifyDataSetChanged();
    }




    private void initializeViewModel() {
        mForumListViewModel = ViewModelProviders.of(this).get(SFForumViewModel.class);
        final Observer<List<SFForum>> forumListObserver = new Observer<List<SFForum>>() {
            @Override
            public void onChanged(@Nullable List<SFForum> forums) {
                mForumList = forums;
                showProgress(false);
                refreshUI();
            }
        };
        final Observer<Integer> errorInLoadingForums = new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer errorCode) {
                if (errorCode == SFAPIResponseCode.Error.FETCH_FORUM_ERROR_TOKEN_EXPIRED) {
                    login(new LoginListener() {
                        @Override
                        public void onSuccess() {
                            loadForumsForCurrentUser();
                        }

                        @Override
                        public void onFail() {
                            showProgress(false);
                            //refreshUI();
                        }
                    });
                } else {
                    showProgress(false);
                    refreshUI();
                }
            }
        };
        mForumListViewModel.getForumListContainer().observe(this, forumListObserver);
        mForumListViewModel.getErrorInForumLoading().observe(this, errorInLoadingForums);
    }

    private void loadForumsForCurrentUser() {
        showProgress(true);
        String token = SFSharedPrefs.getInstance(this).getSessionToken();
        if (SFUtility.isStringNullOrEmpty(token)) {
            login(new SFBaseActivity.LoginListener() {
                @Override
                public void onSuccess() {
                    String token = SFSharedPrefs.getInstance(SFForumListActivity.this).getSessionToken();
                    mForumListViewModel.loadForums(false,token);
                }

                @Override
                public void onFail() {
                    //TODO:login may file with the error user doesn't exist. in this case, the user may have been removed by other means. how do client know about that?
                    SFLogger.showToast(SFForumListActivity.this, "Login onFail.So, not able to load forums");
                }
            });
        } else {
            mForumListViewModel.loadForums(false,token);
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item
     //   Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }
}
