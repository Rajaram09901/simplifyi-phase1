package com.simplifyi.appname.ui.activity;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.chip.Chip;
import android.support.design.chip.ChipGroup;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.model.SFTag;
import com.simplifyi.appname.ui.adapter.SFTagAdapter;
import com.simplifyi.appname.ui.viewmodel.SFTagsViewModel;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

import java.util.ArrayList;
import java.util.List;


public class SFTagSelectionActivity extends SFBaseActivity {
    private List<SFTag> mTags = null;
    private RecyclerView mRecyclerView;
    private SFTagAdapter mTagsAdapter;
    private SFTagsViewModel mTagsViewModel;
    private List<String> mLanguages = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag_selection);
        initializeViewModel();
        initializeObservers();
        initializeUI();
        if (SFUtility.isInternetAvailable(this)) {
            showProgress(true);
            mTagsViewModel.loadTags();
        } else {
            showNoNetworkScreen();
        }
    }

    private void initializeUI() {
        String[] langs = getResources().getStringArray(R.array.languages);
        initializeLanguageButton(R.id.language_one_button, langs[0]);
        initializeLanguageButton(R.id.language_two_button, langs[1]);
        initializeLanguageButton(R.id.language_three_button, langs[2]);
        initializeLanguageButton(R.id.language_four_button, langs[3]);
        initializeLanguageButton(R.id.language_five_button, langs[4]);
        initializeLanguageButton(R.id.language_six_button, langs[5]);
        /* initializeLanguageButton(R.id.add_language_button, "+Add");*/
        findViewById(R.id.btn_continue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SFUtility.isInternetAvailable(SFTagSelectionActivity.this)) {
                    showProgress(true);
                    mTagsViewModel.setLanguage(mLanguages);
                    mTagsViewModel.addTagsToUser();
                } else {
                    showNoNetworkScreen();
                }
            }
        });
    }

    private void initializeLanguageButton(int buttonId, final String label) {
        final Chip languageBtn = findViewById(buttonId);
        languageBtn.setText(label);
        if (label.equals("English")) {
            languageBtn.setTag(true);
            mLanguages.add(label);
        } else {
            languageBtn.setTag(false);//tag will represent selected status
        }
        // updateSelectionStatus(languageBtn);
        languageBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                boolean selected = (boolean) languageBtn.getTag();
                selected = !selected;
                languageBtn.setTag(selected);
                if (selected && !mLanguages.contains(label)) {
                    mLanguages.add(label);
                } else {
                    mLanguages.remove(label);
                }
            }
        });
        languageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean selected = (boolean) languageBtn.getTag();
                selected = !selected;
                languageBtn.setTag(selected);
                if (selected && !mLanguages.contains(label)) {
                    mLanguages.add(label);
                } else {
                    mLanguages.remove(label);
                }
                // updateSelectionStatus(languageBtn);
            }
        });

    }

    private void initializeViewModel() {
        mTagsViewModel = ViewModelProviders.of(this).get(SFTagsViewModel.class);
        mTagsViewModel.setContext(this);
        long phoneNum = SFSharedPrefs.getInstance(this).getPhoneNumber();
        if (phoneNum != SFSharedPrefs.INVALID_PHONE_NUMBER) {
            mTagsViewModel.setMobileNumber(phoneNum);
        }
    }

    private void initializeObservers() {
        final Observer<List<SFTag>> tagListObserver = new Observer<List<SFTag>>() {
            @Override
            public void onChanged(@Nullable List<SFTag> tags) {
                //update the UI
                mTags = tags;
                showProgress(false);
                refreshTagList();
            }
        };

        final Observer<Boolean> addTagToUserCallStatusObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isCallSuccess) {
                showProgress(false);
                if (isCallSuccess) {
                 //   SFLogger.showToast(SFTagSelectionActivity.this, "add Tags to User successful");
                    getCurrentUser(new Runnable() {
                        @Override
                        public void run() {
                            goToNextScreen(SFHomeActivity.class);
                            finish();
                        }
                    });
                } else {
                  //  SFLogger.showToast(SFTagSelectionActivity.this, "add Tags to User unsuccessful");
                }

            }
        };

        //bind observers,lifecycle owner, and observables

        MutableLiveData<List<SFTag>> taglistObservable = mTagsViewModel.getTagListContainer();
        taglistObservable.observe(this, tagListObserver);//now this live data container has all 3 entities with it.

        MutableLiveData<Boolean> TagsCallStatusContainer = mTagsViewModel.getTagsCallStatusContainer();
        TagsCallStatusContainer.observe(this, addTagToUserCallStatusObserver);
    }

    private void refreshTagList() {
        ChipGroup cg = findViewById(R.id.chip_group);
        for (SFTag item : mTags) {
            final Chip chip = new Chip(this);
            chip.setText(item.getName());
            chip.setTag(item.isSelected());
            chip.setChipStrokeWidth(2);
            chip.setChipStrokeColorResource(R.color.blue);
            chip.setChipBackgroundColor(getResources().getColorStateList(R.color.selector_chip_bg));
            chip.setTextColor(getResources().getColorStateList(R.color.selector_chip_text));
            chip.setCheckable(true);
            chip.setClickable(true);
            chip.setFocusable(true);
            chip.setChecked(true);
            chip.setCheckedIconVisible(false);
            chip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                }
            });
            cg.addView(chip);
        }

        if (mRecyclerView == null) {
            mRecyclerView = findViewById(R.id.my_recycler_view);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(SFTagSelectionActivity.this));
            mTagsAdapter = new SFTagAdapter(mTags);
            mRecyclerView.setAdapter(mTagsAdapter);
        } else {
            mTagsAdapter.setData(mTags);
        }
    }

    private void updateSelectionStatus(Button languageButton) {
        if ((boolean) languageButton.getTag()) {
            languageButton.setBackgroundResource(R.drawable.button_background_bluecorners);
        } else {
            languageButton.setBackgroundResource(R.drawable.button_background_grey);/**/
        }
    }
}
