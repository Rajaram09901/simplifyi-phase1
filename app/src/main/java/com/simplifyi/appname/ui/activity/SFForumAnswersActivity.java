package com.simplifyi.appname.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.network.request.SFAnswerRequest;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.network.restclient.SFForumRestClient;
import com.simplifyi.appname.network.restclient.SFTagRestClient;
import com.simplifyi.appname.ui.adapter.SFForumAnswersAdapter;
import com.simplifyi.appname.utility.SFCalendar;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

public class SFForumAnswersActivity extends SFBaseActivity {
    public static final String SELECTED_FORUM = "selected_forum";
    private SFForum mForum;
    private RecyclerView mRecyclerView;
    private SFForumAnswersAdapter mForumAnswersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum_answers);
        mForum = (SFForum) getIntent().getSerializableExtra(SELECTED_FORUM);
        initUI();
    }

    private void initUI() {
        TextView forumUserNameTv = findViewById(R.id.forum_owner_name_tv);
        if (mForum != null) {
            if (mForum.getForumUsers() != null && !mForum.getForumUsers().isEmpty() && mForum.getForumUsers().get(0) != null) {
                forumUserNameTv.setText(mForum.getForumUsers().get(0).getNickName());
            }
            if (mForum.getName() != null && !mForum.getName().isEmpty()) {
                TextView forumNameTv = findViewById(R.id.text_question);
                forumNameTv.setText(mForum.getName());
            }
            mRecyclerView = findViewById(R.id.forum_recycler_view);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mForumAnswersAdapter = new SFForumAnswersAdapter(this, mForum.getAnswers());
            mRecyclerView.setAdapter(mForumAnswersAdapter);
            if (mForum.getTags() != null && !mForum.getTags().isEmpty()) {
                TextView primaryTagTv = findViewById(R.id.textViewPrimaryTag);
                primaryTagTv.setText(mForum.getTags().get(0));
            }
            if (mForum.getTags() != null && !mForum.getTags().isEmpty()) {
                TextView secondaryTagTv = findViewById(R.id.textViewSecondaryTag);
                secondaryTagTv.setText(mForum.getTags().get(1));
            }
        }
        findViewById(R.id.back_arrow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.options_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SFLogger.showToast(SFForumAnswersActivity.this, "Options menu clicked");
            }
        });
        findViewById(R.id.pub_priv_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SFLogger.showToast(SFForumAnswersActivity.this, "Public/Private clicked");
            }
        });

        findViewById(R.id.submit_answer_fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SFUtility.isInternetAvailable(SFForumAnswersActivity.this)) {
                    showNoNetworkScreen();
                    return;
                }
                EditText editText = findViewById(R.id.answer_et);
                String answerText = editText.getText().toString();
                if (!SFUtility.isStringNullOrEmpty(answerText)) {
                    showProgress(false);
                    editText.setText("");
                    SFForum.Answer answer = new SFForum.Answer();
                    SFSharedPrefs prefs = SFSharedPrefs.getInstance(SFForumAnswersActivity.this);
                    answer.setAnswer(answerText);

                    SFForum.ForumUser user = new SFForum.ForumUser();
                    user.setNickName(prefs.getUser().getNick());
                    answer.setUserDetails(user);
                    long elapsedTime = System.currentTimeMillis();
                    String dateAsString = SFCalendar.getDateAsString(elapsedTime);
                    //TODO:we are posting answer with answered date as well to server. what should be the time zone we have to send with?
                    answer.setDate(dateAsString);
                    postAnswerToServer(answer);
                }
            }
        });

        findViewById(R.id.follow_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView followIv = (ImageView) v;
              //  SFLogger.showToast(SFForumAnswersActivity.this, "follow tapped");
                //TODO: how do we know if the current user is following a forum & hence the UI is updated accordingly?"
            }
        });


    }

    private void postAnswerToServer(final SFForum.Answer answer) {
        showProgress(true);
        SFAnswerRequest request = new SFAnswerRequest();
        request.setForumId(mForum.getId());
        String token = SFSharedPrefs.getInstance(this).getSessionToken();
        request.setLoginToken(token);
        request.setAnswer(answer.getAnswer());
        SFForumRestClient manager = new SFForumRestClient();
        manager.postAnswer(request, new SFTagRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    mForum.getAnswers().add(answer);
                    mForumAnswersAdapter.setData(mForum.getAnswers());
                    mForumAnswersAdapter.notifyDataSetChanged();
                    showProgress(false);
                } else if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.FETCH_FORUM_ERROR_TOKEN_EXPIRED) {
                    login(new LoginListener() {
                        @Override
                        public void onSuccess() {
                         //   postAnswerToServer(answer);
                            showProgress(false);

                        }

                        @Override
                        public void onFail() {
                            showProgress(false);
                            //refreshUI();
                        }
                    });

                } else {
                    SFLogger.showToast(SFForumAnswersActivity.this, "Post answer un-successful. error:" + genericResponse.getHeader().getStatusMessage());
                    showProgress(false);
                }

            }

            @Override
            public void onCallFinishedWithError(String error) {
                SFLogger.showToast(SFForumAnswersActivity.this, "Post answer  un-successful. error:");
                showProgress(false);
            }
        });
    }
}
