package com.simplifyi.appname.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.simplifyi.appname.model.SFForum;
import com.simplifyi.simplifyi_phase2.R;

import java.util.List;


public class SFForumSearchBarAdapter extends RecyclerView.Adapter<SFForumSearchBarAdapter.SearchViewHolder> {

    private Context mContext;
    private List<SFForum> mForumList;
    private OnItemClickListener mOnItemClickListener;

    public SFForumSearchBarAdapter(Context context, List<SFForum> forumList) {
        mContext = context;
        mForumList = forumList;
    }

    public void setDataAndRefreshList(List<SFForum> newForumList) {
        if (newForumList != null) {
            mForumList = newForumList;
            notifyDataSetChanged();
        }
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, final int position) {
        SFForum forum = mForumList.get(position);
        holder.nameTv.setText(TextUtils.isEmpty(forum.getName()) ? "" : forum.getName().trim());
        holder.arrowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(position);
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_search_forum_item_template, parent, false);
        return new SearchViewHolder(itemView);
    }

    public void setmOnItemClickListener(OnItemClickListener itemClickListener) {
        mOnItemClickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return mForumList.size();
    }


    class SearchViewHolder extends RecyclerView.ViewHolder {
        TextView nameTv;
        ImageView arrowButton;

        SearchViewHolder(View rootView) {
            super(rootView);
            nameTv = rootView.findViewById(R.id.nameTextView);
            arrowButton = rootView.findViewById(R.id.arrow_button);
        }

    }

    public interface OnItemClickListener {
        void onItemClick(int position);
    }
}
