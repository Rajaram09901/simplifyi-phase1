package com.simplifyi.appname.ui.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.ui.activity.SFActivitiesActivity;
import com.simplifyi.appname.ui.activity.SFBaseActivity;
import com.simplifyi.appname.ui.adapter.SFDailedCallsItemAdapter;
import com.simplifyi.appname.ui.viewmodel.SFCallViewModel;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

import java.util.ArrayList;
import java.util.List;


public class SFReceivedFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private List<SFForum> mForumList;
    private SFCallViewModel mCallsViewModel;
    private SFDailedCallsItemAdapter mDialedCallListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_received_calls, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeViewModel();
        initializeUI();
        if (SFUtility.isInternetAvailable(getActivity())) {
            loadReceivedCalls();
        } else {
            ((SFActivitiesActivity)getActivity()).showNoNetworkScreen();
        }    }

    private void initializeUI() {
        mRecyclerView = getView().findViewById(R.id.forum_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mForumList = new ArrayList<>();
        mDialedCallListAdapter = new SFDailedCallsItemAdapter(getActivity(), mForumList);
        mRecyclerView.setAdapter(mDialedCallListAdapter);
    }

    private void refreshUI() {
        if (mForumList == null || mForumList.isEmpty()) {
            SFLogger.showToast(getActivity(), "Received Call List is empty");
        }
        mDialedCallListAdapter.setData(mForumList);
        mDialedCallListAdapter.notifyDataSetChanged();
    }

    private void initializeViewModel() {
        mCallsViewModel = ViewModelProviders.of(this).get(SFCallViewModel.class);
        final Observer<List<SFForum>> receivedCallsObserver = new Observer<List<SFForum>>() {
            @Override
            public void onChanged(@Nullable List<SFForum> forums) {
                mForumList = forums;
                ((SFActivitiesActivity) getActivity()).showProgress(false);
                refreshUI();
            }
        };
        final Observer<Integer> errorInLoadingForums = new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer errorCode) {
                if (errorCode == SFAPIResponseCode.Error.FETCH_FORUM_ERROR_TOKEN_EXPIRED) {
                    ((SFActivitiesActivity) getActivity()).login(new SFBaseActivity.LoginListener() {
                        @Override
                        public void onSuccess() {
                            loadReceivedCalls();
                        }

                        @Override
                        public void onFail() {
                            ((SFActivitiesActivity) getActivity()).showProgress(false);
                            //refreshUI();
                        }
                    });
                } else {
                    ((SFActivitiesActivity) getActivity()).showProgress(false);
                    refreshUI();
                }
            }
        };
        mCallsViewModel.getReceivedCallListContainer().observe(this, receivedCallsObserver);
        mCallsViewModel.getErrorContainer().observe(this, errorInLoadingForums);
    }

    private void loadReceivedCalls() {
        ((SFActivitiesActivity) getActivity()).showProgress(true);
        String token = SFSharedPrefs.getInstance(getActivity()).getSessionToken();
        if (SFUtility.isStringNullOrEmpty(token)) {
            ((SFActivitiesActivity) getActivity()).login(new SFBaseActivity.LoginListener() {
                @Override
                public void onSuccess() {
                    String token = SFSharedPrefs.getInstance(getActivity()).getSessionToken();
                    mCallsViewModel.getReceivedCalls(token);
                }

                @Override
                public void onFail() {
                    //TODO:login may file with the error user doesn't exist. in this case, the user may have been removed by other means. how do client know about that?
                    SFLogger.showToast(getActivity(), "Login onFail.So, not able to load forums");
                }
            });
        } else {
            mCallsViewModel.getReceivedCalls(token);
        }
    }
}