package com.simplifyi.appname.ui.customview;

import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeCancelState;
import com.mindorks.placeholderview.annotations.swipe.SwipeHead;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeInState;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutState;
import com.mindorks.placeholderview.annotations.swipe.SwipeView;
import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

import de.hdodenhof.circleimageview.CircleImageView;


@Layout(R.layout.layout_swipable_forum_calling_card)
public class SFSwipeableForumCallingCard {
    private UserActionListener mUserActionListener;
    @View(R.id.owner_profile_pic)
    private CircleImageView forumOwnerProfilePic;
    @View(R.id.forum_image)
    private ImageView forumImage;

    @View(R.id.forum_owner_name_tv)
    private TextView forumOwnerNameTv;

    @View(R.id.forum_question_tv)
    private TextView forumQuestionTv;

    @View(R.id.textViewPrimaryTag)
    private TextView primaryTagTv;

    @View(R.id.textViewSecondaryTag)
    private TextView secondaryTagTv;

    @SwipeView
    private android.view.View cardView;

    private SFForum mForum;
    private Context mContext;
    private SwipePlaceHolderView mSwipeView;

    public SFSwipeableForumCallingCard(Context context, SFForum forum, SwipePlaceHolderView swipeView) {
        mContext = context;
        mForum = forum;
        mSwipeView = swipeView;
    }

    @Resolve
    private void onResolved() {// the view are initialized at this point of time. now we can bind the view with data
        //TODO: the below is for transformation
       /* MultiTransformation multi = new MultiTransformation(
                new BlurTransformation(mContext, 30),
                new RoundedCornersTransformation(mContext, SFUIUtility.dpToPx(7), 0,
                        RoundedCornersTransformation.CornerType.TOP));*/
        if (mForum == null) {
            return;
        }
        if (!SFUtility.isStringNullOrEmpty(mForum.getForumImageUrl())) {
            Glide.with(mContext).load(mForum.getForumImageUrl()).into(forumImage);
        }
        if (!SFUtility.isStringNullOrEmpty(mForum.getName())) {
            forumQuestionTv.setText(mForum.getName());
        }
        SFForum.ForumUser user = null;
        if (mForum.getForumUsers() != null && !mForum.getForumUsers().isEmpty()) {
            user = mForum.getForumUsers().get(0);
        }
        if (user != null && !SFUtility.isStringNullOrEmpty(user.getProfileImageUrl())) {
            Glide.with(mContext).load(user.getProfileImageUrl()).into(forumOwnerProfilePic);
        }
        if (user != null && !SFUtility.isStringNullOrEmpty(user.getNickName())) {
            forumOwnerNameTv.setText(user.getNickName());
        }

        if (mForum.getTags() != null && !mForum.getTags().isEmpty()) {
            primaryTagTv.setText(mForum.getTags().get(0));
            secondaryTagTv.setText(mForum.getTags().get(1));
        }
    }

    @SwipeHead
    private void onSwipeHeadCard() {
        /*Glide.with(mContext).load(mForum.getImageUrl())
                .bitmapTransform(new RoundedCornersTransformation(
                        mContext, Utils.dpToPx(7), 0,
                        RoundedCornersTransformation.CornerType.TOP))
                .into(forumOwnerProfilePic);*/
        cardView.invalidate();
    }


    @SwipeOut
    private void onSwipedOut() {
        SFLogger.showToast(mContext, "Swipe Left");
        if (mUserActionListener != null) {
            mUserActionListener.onUserAction(mForum, UserAction.SWIPE_LEFT);
        }
    }

    @SwipeCancelState
    private void onSwipeCancelState() {
        Log.d("EVENT", "onSwipeCancelState");
    }

    @SwipeIn
    private void onSwipeIn() {
        SFLogger.showToast(mContext, "Swipe Right");
        if (mUserActionListener != null) {
            mUserActionListener.onUserAction(mForum, UserAction.SWIPE_RIGHT);
        }

    }

    @SwipeInState
    private void onSwipeInState() {
        Log.d("EVENT", "onSwipeInState");
    }

    @SwipeOutState
    private void onSwipeOutState() {
        Log.d("EVENT", "onSwipeOutState");
    }

    public void setUserActionListener(UserActionListener listener) {
        mUserActionListener = listener;
    }

    public interface UserActionListener {
        void onUserAction(SFForum forum, int userAction);
    }

    public SFForum getForum() {
        return mForum;
    }

    @Click(R.id.call_answer_iv)
    private void onCallClick() {
        SFLogger.showToast(mContext, "Answer button clicked");
        if (mUserActionListener != null) {
            mUserActionListener.onUserAction(mForum, UserAction.BUTTON_ANSWER);
        }
    }

    @Click(R.id.call_hangup_iv)
    private void onHangupClick() {
        SFLogger.showToast(mContext, "Decline button clicked");
        if (mUserActionListener != null) {
            mUserActionListener.onUserAction(mForum, UserAction.BUTTON_DECLINE);
        }
    }

    public static class UserAction {
        public static final int SWIPE_LEFT = 1;
        public static final int SWIPE_RIGHT = 2;
        public static final int BUTTON_ANSWER = 3;
        public static final int BUTTON_DECLINE = 4;
    }
}

