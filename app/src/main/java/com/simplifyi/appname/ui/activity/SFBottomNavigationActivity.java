package com.simplifyi.appname.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;

import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.simplifyi_phase2.R;

public class SFBottomNavigationActivity extends SFBaseActivity {

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener;
    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void setContentView(int childLayout) {
        super.setContentView(R.layout.bottom_navigation_container);
        ViewGroup childContainer = findViewById(R.id.child_bottom_layout_container);
        getLayoutInflater().inflate(childLayout, childContainer);

        initializeBottomNavigationBar();
    }

    private void initializeBottomNavigationBar() {
        bottomNavigationView = findViewById(R.id.bottom_navigation_view);
        int navigationId = SFSharedPrefs.getInstance(SFBottomNavigationActivity.this).currentSideNavigationId;
        if (SFSharedPrefs.getInstance(SFBottomNavigationActivity.this).currentSideNavigationId == -1) {
            bottomNavigationView.setSelectedItemId(R.id.simplify);
        } else {
            bottomNavigationView.setSelectedItemId(navigationId);
        }
        mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                //TODO: Replace all fragments with  Activities as per design.
                switch (item.getItemId()) {
                    case R.id.simplify://do nothing as per design
                        if (SFSharedPrefs.getInstance(SFBottomNavigationActivity.this).currentSideNavigationId == R.id.simplify) {
                            return true;
                        }
                        SFSharedPrefs.getInstance(SFBottomNavigationActivity.this).currentSideNavigationId = R.id.simplify;
                        Intent intent = new Intent(SFBottomNavigationActivity.this, SFHomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        break;
                    case R.id.activity:
                        if (SFSharedPrefs.getInstance(SFBottomNavigationActivity.this).currentSideNavigationId == R.id.activity) {
                            return true;
                        }
                        SFSharedPrefs.getInstance(SFBottomNavigationActivity.this).currentSideNavigationId = R.id.activity;
                        intent = new Intent(SFBottomNavigationActivity.this, SFActivitiesActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        break;
                    case R.id.add:
                        if (SFSharedPrefs.getInstance(SFBottomNavigationActivity.this).currentSideNavigationId == R.id.add) {
                            return true;
                        }
                        SFSharedPrefs.getInstance(SFBottomNavigationActivity.this).currentSideNavigationId = R.id.add;
                        intent = new Intent(SFBottomNavigationActivity.this, SFLevel2TagListActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        break;
                    case R.id.earn:
                        if (SFSharedPrefs.getInstance(SFBottomNavigationActivity.this).currentSideNavigationId == R.id.earn) {
                            return true;
                        }
                        SFSharedPrefs.getInstance(SFBottomNavigationActivity.this).currentSideNavigationId = R.id.earn;
                        intent = new Intent(SFBottomNavigationActivity.this, SFEarnActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                        break;
                    case R.id.profile:
                        if (SFSharedPrefs.getInstance(SFBottomNavigationActivity.this).currentSideNavigationId == R.id.profile) {
                            return true;
                        }
                        SFSharedPrefs.getInstance(SFBottomNavigationActivity.this).currentSideNavigationId = R.id.profile;
                        intent = new Intent(SFBottomNavigationActivity.this, SFProfileActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        goToNextScreen(intent);
                        break;
                }
                return true;
            }

        };
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        //bottomNavigationView.setItemIconTintList(null);
        overridePendingTransition(0, 0);
    }

    @Override
    public void onBackPressed() {
        SFSharedPrefs.getInstance(SFBottomNavigationActivity.this).currentSideNavigationId = R.id.simplify;
        Intent intent = new Intent(SFBottomNavigationActivity.this, SFHomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
    }


}
