package com.simplifyi.appname.ui.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.widget.Switch;

import com.simplifyi.appname.ui.fragment.SFFirstTimeFiveFragment;
import com.simplifyi.appname.ui.fragment.SFFirstTimeFourFragment;
import com.simplifyi.appname.ui.fragment.SFFirstTimeOneFragment;
import com.simplifyi.appname.ui.fragment.SFFirstTimeThreeFragment;
import com.simplifyi.appname.ui.fragment.SFFirstTimeTwoFragment;

public class SFViewPagerAdapter extends FragmentStatePagerAdapter {
    public SFViewPagerAdapter(FragmentManager fm) {

        super(fm);
    }


    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;
        switch (position) {

            case 0:
                fragment = new SFFirstTimeOneFragment();
                break;

            case 1:
                fragment = new SFFirstTimeTwoFragment();
                break;

            case 2:
                fragment = new SFFirstTimeThreeFragment();
                break;
            case 3:
                fragment = new SFFirstTimeFourFragment();
                break;
            case 4:
                fragment = new SFFirstTimeFiveFragment();
                break;

        }
        return fragment;
    }
}
