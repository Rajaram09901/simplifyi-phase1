package com.simplifyi.appname.ui.activity;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.ui.adapter.SFViewPagerAdapter;
import com.simplifyi.simplifyi_phase2.R;

public class SFPagerActivity extends SFBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager_description);
        ViewPager pager = findViewById(R.id.homeViewpager);
        SFViewPagerAdapter pagerAdapter = new SFViewPagerAdapter(this.getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);
        findViewById(R.id.next_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SFSharedPrefs.getInstance(getApplicationContext()).setFirstTimeUser(false);
                resolveNavigateToDestination();

            }
        });
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

                switch (i) {

                    case 0: {
                        ((ImageView) findViewById(R.id.firstDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_highlighted));
                        ((ImageView) findViewById(R.id.secondDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        ((ImageView) findViewById(R.id.thirdDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        ((ImageView) findViewById(R.id.fourthDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        ((ImageView) findViewById(R.id.fifthDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        findViewById(R.id.next_button).setVisibility(View.GONE);
                        findViewById(R.id.skip_button).setVisibility(View.VISIBLE);
                        break;
                    }
                    case 1: {
                        ((ImageView) findViewById(R.id.firstDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        ((ImageView) findViewById(R.id.secondDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_highlighted));
                        ((ImageView) findViewById(R.id.thirdDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        ((ImageView) findViewById(R.id.fourthDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        ((ImageView) findViewById(R.id.fifthDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        findViewById(R.id.next_button).setVisibility(View.GONE);
                        findViewById(R.id.skip_button).setVisibility(View.VISIBLE);
                        break;
                    }
                    case 2: {
                        ((ImageView) findViewById(R.id.firstDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        ((ImageView) findViewById(R.id.secondDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        ((ImageView) findViewById(R.id.thirdDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_highlighted));
                        ((ImageView) findViewById(R.id.fourthDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        ((ImageView) findViewById(R.id.fifthDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        findViewById(R.id.next_button).setVisibility(View.GONE);
                        findViewById(R.id.skip_button).setVisibility(View.VISIBLE);
                        break;
                    }
                    case 3:
                        ((ImageView) findViewById(R.id.firstDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        ((ImageView) findViewById(R.id.secondDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        ((ImageView) findViewById(R.id.thirdDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        ((ImageView) findViewById(R.id.fourthDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_highlighted));
                        ((ImageView) findViewById(R.id.fifthDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        findViewById(R.id.next_button).setVisibility(View.GONE);
                        findViewById(R.id.skip_button).setVisibility(View.VISIBLE);
                        break;

                    case 4:
                        ((ImageView) findViewById(R.id.firstDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        ((ImageView) findViewById(R.id.secondDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        ((ImageView) findViewById(R.id.thirdDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        ((ImageView) findViewById(R.id.fourthDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_normal));
                        ((ImageView) findViewById(R.id.fifthDotImageView)).setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_pager_indicator_highlighted));
                        findViewById(R.id.next_button).setVisibility(View.VISIBLE);
                        findViewById(R.id.skip_button).setVisibility(View.GONE);
                        break;
                }

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

     public void skipButtonClick(View v) {
        SFSharedPrefs.getInstance(getApplicationContext()).setFirstTimeUser(false);
        resolveNavigateToDestination();
    }
}
