package com.simplifyi.appname.ui.activity;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.model.SFTag;
import com.simplifyi.appname.ui.viewmodel.SFTagsViewModel;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;
import com.simplifyi.appname.ui.adapter.SFTagListAdapter;

import java.util.ArrayList;
import java.util.List;


public class SFLevel3TagsListActivity extends SFBaseActivity {
    public static final String LEVEL_TWO_TAG_NAME = "level 2 tag name";
    private RecyclerView mRecyclerView;
    private String mPrimaryTag;
    private List<SFTag> mLevel2Tags = null;
    private SFTagsViewModel mTagsViewModel;
    private SFTagListAdapter mTagListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level3_taglist_new);
        mPrimaryTag = getIntent().getStringExtra(LEVEL_TWO_TAG_NAME);
        initializeViewModel();
        initializeObservers();
        initializeUI();

        if (SFUtility.isInternetAvailable(this)) {
            showProgress(true);
            mTagsViewModel.loadTags(3, mPrimaryTag);//level 2 tags
        } else {
            showNoNetworkScreen();
        }
    }

    private void initializeUI() {
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        refreshUI();
    }

    private void handleTagClickEvent(SFTag tag) {
        // SFLogger.showToast(this, "tapped " + tag.getName());
        Intent intent = new Intent(this, SFCreateForumActivity.class);
        ArrayList<String> tags = new ArrayList<>();
        tags.add(mPrimaryTag);
        tags.add(tag.getName());
        intent.putStringArrayListExtra(SFCreateForumActivity.FORUM_CATEGORY_INFO, tags);
        goToNextScreen(intent);
    }

    void initializeViewModel() {
        mTagsViewModel = ViewModelProviders.of(this).get(SFTagsViewModel.class);
        mTagsViewModel.setContext(this);
        if (SFSharedPrefs.getInstance(this).getPhoneNumber() != SFSharedPrefs.INVALID_PHONE_NUMBER) {
            mTagsViewModel.setMobileNumber(SFSharedPrefs.getInstance(this).getPhoneNumber());
        }
    }

    void initializeObservers() {
        final Observer<List<SFTag>> tagListObserver = new Observer<List<SFTag>>() {
            @Override
            public void onChanged(@Nullable List<SFTag> tags) {
                //update the UI
                mLevel2Tags = tags;
                showProgress(false);
                refreshUI();
            }
        };

        MutableLiveData<List<SFTag>> taglistObservable = mTagsViewModel.getTagListContainer();
        taglistObservable.observe(this, tagListObserver);//now this live data container has all 3 entities with it.
    }

    void refreshUI() {
        if (mRecyclerView == null) {
            mRecyclerView = findViewById(R.id.rv);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mTagListAdapter = new SFTagListAdapter(this, mLevel2Tags);
            mRecyclerView.setAdapter(mTagListAdapter);
            mTagListAdapter.setOnTagItemClickListener(new SFTagListAdapter.OnTagItemClickListener() {
                @Override
                public void onTagItemClick(SFTag tag) {
                    handleTagClickEvent(tag);
                }
            });
        } else {
            mTagListAdapter.setData(mLevel2Tags);
        }
        mTagListAdapter.notifyDataSetChanged();
    }
}