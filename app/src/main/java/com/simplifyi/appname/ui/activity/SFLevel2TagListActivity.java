package com.simplifyi.appname.ui.activity;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.model.SFTag;
import com.simplifyi.appname.ui.adapter.SFTagListAdapter;
import com.simplifyi.appname.ui.viewmodel.SFTagsViewModel;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

import java.util.ArrayList;
import java.util.List;


public class SFLevel2TagListActivity extends SFBottomNavigationActivity {
    public EditText mSearchBoxEt;
    private RecyclerView mRecyclerView;
    private SFTagListAdapter mTagListAdapter;
    /*public static add newInstance() {
        return new add();
    }*/

    private List<SFTag> mLevel2Tags = null;
    private SFTagsViewModel mTagsViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level2_taglist);
        initializeViewModel();
        initializeObservers();
        initializeUI();

        if (SFUtility.isInternetAvailable(this)) {
            showProgress(true);
            mTagsViewModel.loadTags(2);//level 2 tags
         //   mTagsViewModel.loadTags(3, mLevel2Tags.get(0).getName());//level 2 tags
        } else {
            showNoNetworkScreen();
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void initializeUI() {
        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mSearchBoxEt = findViewById(R.id.search_box_et);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            //  mSearchBoxEt.setShowSoftInputOnFocus(false);
            mSearchBoxEt.clearFocus();
        }
        mSearchBoxEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SFLevel2TagListActivity.this, SFSearchForumActivity.class);
                startActivity(intent);
            }
        });
        refreshUI();
    }

    private void handleTagClickEvent(SFTag tag) {
       /* Intent intent = new Intent(this, SFCreateForumActivity.class);
        intent.putExtra(SFLevel3TagsListActivity.LEVEL_TWO_TAG_NAME, tag.getName());
        goToNextScreen(intent)*/

        Intent intent = new Intent(this, SFCreateForumActivity.class);
        ArrayList<String> tags = new ArrayList<>();
       // tags.add(mLevel2Tags);
        tags.add(tag.getName());
        intent.putStringArrayListExtra(SFCreateForumActivity.FORUM_CATEGORY_INFO, tags);
        goToNextScreen(intent);
    }

   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);

    }*/


    //************************** TagSelection class code ***************//
    void initializeViewModel() {
        mTagsViewModel = ViewModelProviders.of(this).get(SFTagsViewModel.class);
        mTagsViewModel.setContext(this);
        if (SFSharedPrefs.getInstance(this).getPhoneNumber() != SFSharedPrefs.INVALID_PHONE_NUMBER) {
            mTagsViewModel.setMobileNumber(SFSharedPrefs.getInstance(this).getPhoneNumber());
        }
    }

    void initializeObservers() {
        final Observer<List<SFTag>> tagListObserver = new Observer<List<SFTag>>() {
            @Override
            public void onChanged(@Nullable List<SFTag> tags) {
                //update the UI
                mLevel2Tags = tags;
                showProgress(false);
                refreshUI();
            }
        };

        MutableLiveData<List<SFTag>> taglistObservable = mTagsViewModel.getTagListContainer();
        taglistObservable.observe(this, tagListObserver);
        // now this live data container has all 3 entities with it.
    }

    void refreshUI() {
        if (mRecyclerView == null) {
            mRecyclerView = findViewById(R.id.category_recycler_view);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            mTagListAdapter = new SFTagListAdapter(this, mLevel2Tags);
            mTagListAdapter.setOnTagItemClickListener(new SFTagListAdapter.OnTagItemClickListener() {
                @Override
                public void onTagItemClick(SFTag tag) {
                    handleTagClickEvent(tag);
                }
            });
            mRecyclerView.setAdapter(mTagListAdapter);
        } else {
            mTagListAdapter.setData(mLevel2Tags);
        }
        mTagListAdapter.notifyDataSetChanged();
    }
}