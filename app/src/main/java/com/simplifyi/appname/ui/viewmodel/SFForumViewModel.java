package com.simplifyi.appname.ui.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.network.request.SFCreateForumRequest;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.network.restclient.SFForumRestClient;
import com.simplifyi.appname.network.restclient.SFTagRestClient;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SFForumViewModel extends ViewModel {
    //Data to be observed. i.e live data that keep changing & hence UI needs to change
    private MutableLiveData<List<SFForum>> mForumLisContainer;
    private MutableLiveData<List<SFForum>> mCallableForumLisContainer;
    private MutableLiveData<Integer> mErrorInForumLoading;
    private MutableLiveData<Integer> mErrorInCallableForumLoading;
    private MutableLiveData<Integer> mErrorInForumCreating;
    private MutableLiveData<SFForum> mNewlyCreatedForumContainer;

    //create the empty container
    public MutableLiveData<List<SFForum>> getForumListContainer() {
        if (mForumLisContainer == null) {
            mForumLisContainer = new MutableLiveData<>();
        }
        return mForumLisContainer;
    }

    public MutableLiveData<List<SFForum>> getCallableForumLisContainer() {
        if (mCallableForumLisContainer == null) {
            mCallableForumLisContainer = new MutableLiveData<>();
        }
        return mCallableForumLisContainer;
    }

    public MutableLiveData<SFForum> getNewlyCreatedForumContainer() {
        if (mNewlyCreatedForumContainer == null) {
            mNewlyCreatedForumContainer = new MutableLiveData<>();
        }
        return mNewlyCreatedForumContainer;
    }

    //create the empty container
    public MutableLiveData<Integer> getErrorInForumLoading() {
        if (mErrorInForumLoading == null) {
            mErrorInForumLoading = new MutableLiveData<>();
        }
        return mErrorInForumLoading;
    }

    //create the empty container
    public MutableLiveData<Integer> getErrorInCallableForumLoading() {
        if (mErrorInCallableForumLoading == null) {
            mErrorInCallableForumLoading = new MutableLiveData<>();
        }
        return mErrorInCallableForumLoading;
    }

    //create the empty container
    public MutableLiveData<Integer> getErrorInForumCreating() {
        if (mErrorInForumCreating == null) {
            mErrorInForumCreating = new MutableLiveData<>();
        }
        return mErrorInForumCreating;
    }

    //call this from activity

    public void loadForums(final boolean getCallableForums, String token) {
        loadForums(getCallableForums, token, null);
    }
    public void loadForums(final boolean getCallableForums, String token, String searchKey) {
        SFForumRestClient manager = new SFForumRestClient();
        Map queryParamsMap = new HashMap();
        queryParamsMap.put("token", token);
        if (searchKey != null) {
            queryParamsMap.put("search", searchKey);
        }
        manager.getForumsForCurrentUser(getCallableForums, queryParamsMap, new SFTagRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    SFGenericResponse.SFBody<SFForum> forumsResponseBody = genericResponse.getBody();
                    List<SFForum> forums = forumsResponseBody.getData();
                    List<SFForum> currentList = getForumListContainer().getValue();
                    if (currentList == null) {
                        currentList = new ArrayList<>();
                    } else {
                        currentList.clear();
                    }
                    if (forums != null && !forums.isEmpty()) {
                        for (SFForum forum : forums) {
                            currentList.add(forum);
                        }
                    }
                    if (getCallableForums) {
                       /* //TODO: delete this patch later. this is to avoid a mistake done at server side: some unknown text is sent inside data[] when callable forum is empty
                        if (currentList.size() == 1 && !SFUtility.isStringNullOrEmpty(currentList.get(0).results) && currentList.get(0).results.equals("No calls ATM")) {
                            currentList.clear();//Make it empty
                        }*/
                        getCallableForumLisContainer().setValue(currentList);
                    } else {
                        getForumListContainer().setValue(currentList);
                    }
                    SFLogger.debugLog("Size of the Forums fetched:", "Size=" + currentList.size());
                    SFLogger.debugLog("Forum fetch successful");
                } else if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.FETCH_FORUM_ERROR_TOKEN_EXPIRED) {
                    if (getCallableForums) {
                        getErrorInCallableForumLoading().setValue(SFAPIResponseCode.Error.FETCH_FORUM_ERROR_TOKEN_EXPIRED);
                    } else {
                        getErrorInForumLoading().setValue(SFAPIResponseCode.Error.FETCH_FORUM_ERROR_TOKEN_EXPIRED);
                    }
                } else {
                    SFLogger.debugLog("Forum fetch un-successful. error:" + genericResponse.getHeader().getStatusMessage());
                    if (getCallableForums) {
                        getErrorInCallableForumLoading().setValue(SFAPIResponseCode.Error.UN_KNOWN);
                    } else {
                        getErrorInForumLoading().setValue(SFAPIResponseCode.Error.UN_KNOWN);
                    }
                }

            }

            @Override
            public void onCallFinishedWithError(String error) {
                if (getCallableForums) {
                    getCallableForumLisContainer().setValue(new ArrayList<SFForum>());
                } else {
                    getForumListContainer().setValue(new ArrayList<SFForum>());
                }
            }
        });

    }


    public void createForum(String token, SFCreateForumRequest createForumRequest) {

        // token = "webfreeforums";
        //showProgress(true);
        SFForumRestClient manager = new SFForumRestClient();
        manager.createForum(token, createForumRequest, new SFTagRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    SFGenericResponse.SFBody<SFForum> forumsResponseBody = genericResponse.getBody();
                    List<SFForum> forums = forumsResponseBody.getData();
                    getNewlyCreatedForumContainer().setValue(forums.get(0));
                    SFLogger.debugLog("Forum create successful");
                } else if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.FETCH_FORUM_ERROR_TOKEN_EXPIRED) {
                    getErrorInForumCreating().setValue(SFAPIResponseCode.Error.CREATE_FORUM_ERROR_TOKEN_EXPIRED);
                } else {
                    SFLogger.debugLog("Forum create un-successful. error:" + genericResponse.getHeader().getStatusMessage());
                    getErrorInForumCreating().setValue(SFAPIResponseCode.Error.UN_KNOWN);
                }

            }

            @Override
            public void onCallFinishedWithError(String error) {
                getErrorInForumCreating().setValue(SFAPIResponseCode.Error.UN_KNOWN);
            }
        });

    }
}
