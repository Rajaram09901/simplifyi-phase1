package com.simplifyi.appname.ui.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import com.simplifyi.appname.SFApplication;

/**
 * Created by developer.dinesh042
 */
public class SFMyAlertDialog extends DialogFragment {
    private Context mContext;
    private String mTitle;
    private String mBody;
    private String mNegativeBtnLabel;
    private String mPositiveBtnLabel;
    private DialogInterface.OnClickListener mNegativeBtnListener;
    private DialogInterface.OnClickListener mPositiveBtnListener;

    public SFMyAlertDialog() {
        //Lets use a global context than activity context to avoid leaks
        mContext = SFApplication.mAppContext;
        mTitle = "Title1";
        mBody = "Body1";
        mNegativeBtnLabel = "NegativeButton";
        mPositiveBtnLabel = "PositiveButton";
        mNegativeBtnListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SFMyAlertDialog.this.dismiss();
            }
        };
    }

    public void setContext(Context context) {
        mContext = context;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // return super.onCreateDialog(savedInstanceState);
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage(mBody);
        builder.setTitle(mTitle);
        builder.setNegativeButton(mNegativeBtnLabel, mNegativeBtnListener);
        if (mPositiveBtnListener != null) {
            builder.setPositiveButton(mPositiveBtnLabel, mPositiveBtnListener);
        }
        return builder.create();
    }

    public void configNegativeButton(String label, DialogInterface.OnClickListener listener) {
        mNegativeBtnLabel = label;
        mNegativeBtnListener = listener;
    }

    public void configPositiveButton(String label, DialogInterface.OnClickListener listener) {
        mPositiveBtnLabel = label;
        mNegativeBtnListener = listener;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setBody(String body) {
        mBody = body;
    }
}
