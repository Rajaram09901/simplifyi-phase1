package com.simplifyi.appname.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

public class SFNoInternetActivity extends SFBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet);
        findViewById(R.id.try_again_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SFUtility.isInternetAvailable(SFNoInternetActivity.this)) {
                    finish();
                }
            }
        });
    }
}
