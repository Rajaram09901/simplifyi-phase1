package com.simplifyi.appname.ui.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class MobileViewModel extends ViewModel {
    private MutableLiveData<String> mMobleNumber;

    public MutableLiveData<String> getMobileNumber() {
        if (mMobleNumber == null) {
            mMobleNumber = new MutableLiveData<>();
        }
        return mMobleNumber;
    }

    void sendValueToObserver(){
        mMobleNumber.setValue("");//calls the observer in activity. (onChanged)
    }
}
