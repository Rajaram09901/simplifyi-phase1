package com.simplifyi.appname.ui.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.network.restclient.SFCallRestClient;
import com.simplifyi.appname.network.restclient.SFUserRestClient;
import com.simplifyi.appname.utility.SFLogger;

import java.util.List;

public class SFCallViewModel extends ViewModel {

    private MutableLiveData<Boolean> mWasTheCallDisconnectedContainer;
    private MutableLiveData<Boolean> mMarkCallInitiatedOrEndedContainer;
    private MutableLiveData<Boolean> mRateUserContainer;
    private MutableLiveData<Integer> mErrorContainer;
    private MutableLiveData<List<SFForum>> mDialedCallListContainer;
    private MutableLiveData<List<SFForum>> mReceivedCallListContainer;
    private MutableLiveData<List<SFForum>> mFollowsCallListContainer;

    public MutableLiveData<Boolean> getWasTheCallDisconnectedContainer() {
        if (mWasTheCallDisconnectedContainer == null) {
            mWasTheCallDisconnectedContainer = new MutableLiveData<>();
        }
        return mWasTheCallDisconnectedContainer;
    }

    public MutableLiveData<List<SFForum>> getDialedCallListContainer() {
        if (mDialedCallListContainer == null) {
            mDialedCallListContainer = new MutableLiveData<>();
        }
        return mDialedCallListContainer;
    }

    public MutableLiveData<List<SFForum>> getReceivedCallListContainer() {
        if (mReceivedCallListContainer == null) {
            mReceivedCallListContainer = new MutableLiveData<>();
        }
        return mReceivedCallListContainer;
    }

    public MutableLiveData<List<SFForum>> getFollowsCallListContainer() {
        if (mFollowsCallListContainer == null) {
            mFollowsCallListContainer = new MutableLiveData<>();
        }
        return mFollowsCallListContainer;
    }

    public MutableLiveData<Boolean> getMarkCallInitiatedOrEndedContainer() {
        if (mMarkCallInitiatedOrEndedContainer == null) {
            mMarkCallInitiatedOrEndedContainer = new MutableLiveData<>();
        }
        return mMarkCallInitiatedOrEndedContainer;
    }

    public MutableLiveData<Boolean> getRateUserContainer() {
        if (mRateUserContainer == null) {
            mRateUserContainer = new MutableLiveData<>();
        }
        return mRateUserContainer;
    }

    public MutableLiveData<Integer> getErrorContainer() {
        if (mErrorContainer == null) {
            mErrorContainer = new MutableLiveData<>();
        }
        return mErrorContainer;
    }

    public void disconnectTheCall(String token, String forumId) {
        final SFCallRestClient callRestClient = new SFCallRestClient();
        callRestClient.disconnectCall(token, forumId, new SFUserRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    getWasTheCallDisconnectedContainer().setValue(true);
                    SFLogger.debugLog("disconnect call  successful");

                } else if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    mErrorContainer.setValue(SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED);
                }

            }

            @Override
            public void onCallFinishedWithError(String error) {
                mErrorContainer.setValue(SFAPIResponseCode.Error.UN_KNOWN);
            }
        });
    }

    public void markCallInitiated(String token, String forumId, String callReceivedUserId) {
        final SFCallRestClient callRestClient = new SFCallRestClient();
        callRestClient.markCallInitiated(token, forumId, callReceivedUserId, new SFUserRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    getMarkCallInitiatedOrEndedContainer().setValue(true);
                    SFLogger.debugLog("markCallInitiated call  successful");

                } else if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    SFLogger.debugLog("markCallInitiated call  failed:token expired error");
                    mErrorContainer.setValue(SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED);
                }
            }

            @Override
            public void onCallFinishedWithError(String error) {
                SFLogger.debugLog("markCallInitiated call  failed:unknown error");
                mErrorContainer.setValue(SFAPIResponseCode.Error.UN_KNOWN);
            }
        });
    }

    public void markCallEnded(String token, String forumId, String callReceivedUserId) {
        final SFCallRestClient callRestClient = new SFCallRestClient();
        callRestClient.markCallInitiated(token, forumId, callReceivedUserId, new SFUserRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    getMarkCallInitiatedOrEndedContainer().setValue(true);
                    SFLogger.debugLog("markCallEnded call  successful");
                } else if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    mErrorContainer.setValue(SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED);
                }

            }

            @Override
            public void onCallFinishedWithError(String error) {
                mErrorContainer.setValue(SFAPIResponseCode.Error.UN_KNOWN);
            }
        });
    }

    public void getDialedCalls(String token) {
        final SFCallRestClient callRestClient = new SFCallRestClient();
        callRestClient.getDialedCalls(token, new SFUserRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    SFGenericResponse.SFBody forumsResponseBody = genericResponse.getBody();
                    List<SFForum> dialedForums = forumsResponseBody.getData();
                    getDialedCallListContainer().setValue(dialedForums);
                    SFLogger.debugLog("getDialedCalls call  successful");
                } else if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    mErrorContainer.setValue(SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED);
                }

            }

            @Override
            public void onCallFinishedWithError(String error) {
                mErrorContainer.setValue(SFAPIResponseCode.Error.UN_KNOWN);
            }
        });
    }

    public void getReceivedCalls(String token) {
        final SFCallRestClient callRestClient = new SFCallRestClient();
        callRestClient.getReceivedCalls(token, new SFUserRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    SFGenericResponse.SFBody forumsResponseBody = genericResponse.getBody();
                    List<SFForum> dialedForums = forumsResponseBody.getData();
                    getReceivedCallListContainer().setValue(dialedForums);
                    SFLogger.debugLog("getReceivedCalls() call  successful");
                } else if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    mErrorContainer.setValue(SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED);
                }

            }

            @Override
            public void onCallFinishedWithError(String error) {
                mErrorContainer.setValue(SFAPIResponseCode.Error.UN_KNOWN);
            }
        });
    }

    public void getFollowsCalls(String token) {
        final SFCallRestClient callRestClient = new SFCallRestClient();
        callRestClient.getFollowsCalls(token, new SFUserRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    SFGenericResponse.SFBody forumsResponseBody = genericResponse.getBody();
                    List<SFForum> dialedForums = forumsResponseBody.getData();
                    getFollowsCallListContainer().setValue(dialedForums);
                    SFLogger.debugLog("getFollowsCalls() call  successful");
                } else if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    mErrorContainer.setValue(SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED);
                }

            }

            @Override
            public void onCallFinishedWithError(String error) {
                mErrorContainer.setValue(SFAPIResponseCode.Error.UN_KNOWN);
            }
        });
    }

    public void rateUser(String forumId, String userId, int rating, String token) {
        final SFCallRestClient callRestClient = new SFCallRestClient();
        callRestClient.rateUser(forumId, userId, rating, token, new SFUserRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    getRateUserContainer().setValue(true);
                    SFLogger.debugLog("rate user call  successful");

                } else if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED) {
                    mErrorContainer.setValue(SFAPIResponseCode.Error.GET_CURRENT_USER_ERROR_TOKEN_EXPIRED);
                }

            }

            @Override
            public void onCallFinishedWithError(String error) {
                mErrorContainer.setValue(SFAPIResponseCode.Error.UN_KNOWN);
            }
        });
    }
}
