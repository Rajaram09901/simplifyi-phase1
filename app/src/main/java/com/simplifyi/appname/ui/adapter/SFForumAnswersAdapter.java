package com.simplifyi.appname.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.utility.SFCalendar;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

import java.util.List;

public class SFForumAnswersAdapter extends RecyclerView.Adapter<SFForumAnswersAdapter.MyViewHolder> {
    private List<SFForum.Answer> mAnswerList;
    private Context mContext;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView forumUserNameTv;
        public TextView answerTv;
        public TextView timeTv;
        View rootView;

        public MyViewHolder(View v) {
            super(v);
            rootView = v;
            forumUserNameTv = rootView.findViewById(R.id.txtSender);
            answerTv = rootView.findViewById(R.id.txtMessage);
            timeTv = rootView.findViewById(R.id.txtDate);

        }
    }

    public void setData(List<SFForum.Answer> answers) {
        mAnswerList = answers;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public SFForumAnswersAdapter(Context context, List<SFForum.Answer> forumUsers) {
        mAnswerList = forumUsers;
        mContext = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SFForumAnswersAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.answer_item_template, parent, false);

        MyViewHolder vh = new MyViewHolder(itemView);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final SFForum.Answer answer = mAnswerList.get(position);
        if (answer != null) {
            if (!SFUtility.isStringNullOrEmpty(answer.getAnswer())) {
                holder.answerTv.setText(answer.getAnswer());
            }
            if (answer.getUserDetails() != null && !SFUtility.isStringNullOrEmpty(answer.getUserDetails().getNickName())) {
                holder.forumUserNameTv.setText(answer.getUserDetails().getNickName());
            }
            if (!SFUtility.isStringNullOrEmpty(answer.getDate())) {
                holder.timeTv.setText(SFCalendar.elapsedTime(answer.getDate()) + " ago");
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mAnswerList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

}
