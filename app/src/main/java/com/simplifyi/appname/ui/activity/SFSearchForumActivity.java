package com.simplifyi.appname.ui.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.ui.adapter.SFForumSearchBarAdapter;
import com.simplifyi.appname.ui.viewmodel.SFForumViewModel;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SFSearchForumActivity extends SFBaseActivity {
    private static final long SEARCH_QUERY_TRIGGER_DELAY_IN_MILLIS = 2 * 1000;
    private List<SFForum> mItemList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private SFForumSearchBarAdapter mFilterableAdapter;
    private SearchView mSearchView;
    private ProgressBar mFilterProgressBar;
    private SFForumViewModel mForumListViewModel;
    private String mCurrentSearchTerm = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_forum);
        initializeUI();
        initializeViewModel();
        loadForumsForCurrentUser();
    }

    private void refreshList() {
        mFilterableAdapter.setDataAndRefreshList(mItemList);
    }

    private void initializeUI() {
        mFilterProgressBar = findViewById(R.id.myProgressBar);
        mSearchView = findViewById(R.id.mySearchView);
        mSearchView.clearFocus();
        mSearchView.setIconified(true);
        mSearchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                mCurrentSearchTerm = query;
                loadForumsForCurrentUser();
                return false;
            }
        });
        mRecyclerView = findViewById(R.id.search_forum_recycler_view);
     //   mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mFilterableAdapter = new SFForumSearchBarAdapter(this, mItemList);
        mRecyclerView.setAdapter(mFilterableAdapter);
        mFilterableAdapter.setmOnItemClickListener(new SFForumSearchBarAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent(SFSearchForumActivity.this, SFCreateForumActivity.class);
                intent.putExtra(SFCreateForumActivity.EXISTING_FORUM, mItemList.get(position));
                goToNextScreen(intent);
            }
        });

    }

    public void backpage(View view) {
        /*Intent intent = new Intent(SFSearchForumActivity.this, SFLevel2TagListActivity.class);
        startActivity(intent);*/
        finish();

    }

    public void back(View view) {
        Intent intent = new Intent(SFSearchForumActivity.this, SFLevel3TagsListActivity.class);
        startActivity(intent);

    }

    private void initializeViewModel() {
        mForumListViewModel = ViewModelProviders.of(this).get(SFForumViewModel.class);
        final Observer<List<SFForum>> forumListObserver = new Observer<List<SFForum>>() {
            @Override
            public void onChanged(@Nullable List<SFForum> forums) {
                mItemList = forums;
                refreshList();
                showSearchInProgress(false);
            }
        };
        final Observer<Integer> errorInLoadingForums = new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer errorCode) {
                if (errorCode == SFAPIResponseCode.Error.FETCH_FORUM_ERROR_TOKEN_EXPIRED) {
                    login(new LoginListener() {
                        @Override
                        public void onSuccess() {
                            loadForumsForCurrentUser();
                        }

                        @Override
                        public void onFail() {
                            showSearchInProgress(false);
                        }
                    });
                } else {
                    showSearchInProgress(false);
                    refreshList();
                }
            }
        };
        mForumListViewModel.getForumListContainer().observe(this, forumListObserver);
        mForumListViewModel.getErrorInForumLoading().observe(this, errorInLoadingForums);
    }

    private void showSearchInProgress(boolean toBeShown) {
        if (toBeShown) {
            mFilterProgressBar.setIndeterminate(true);
            mFilterProgressBar.setVisibility(View.VISIBLE);
        } else {
            mFilterProgressBar.setIndeterminate(false);
            mFilterProgressBar.setVisibility(View.GONE);
        }
    }

    private void loadForumsForCurrentUser() {
        showSearchInProgress(true);
        String token = SFSharedPrefs.getInstance(this).getSessionToken();
        if (SFUtility.isStringNullOrEmpty(token)) {
            login(new SFBaseActivity.LoginListener() {
                @Override
                public void onSuccess() {
                    String token = SFSharedPrefs.getInstance(SFSearchForumActivity.this).getSessionToken();
                    mForumListViewModel.loadForums(false, token);
                }

                @Override
                public void onFail() {
                    //TODO:login may file with the error user doesn't exist. in this case, the user may have been removed by other means. how do client know about that?
                    SFLogger.showToast(SFSearchForumActivity.this, "Login onFail.So, not able to search for forums");
                }
            });
        } else {
            mForumListViewModel.loadForums(false, token, mCurrentSearchTerm);
        }
    }
}
