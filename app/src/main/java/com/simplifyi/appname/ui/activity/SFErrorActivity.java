package com.simplifyi.appname.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.simplifyi.simplifyi_phase2.R;

public class SFErrorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);
    }
}
