package com.simplifyi.appname.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.utility.SFAnalytics;
import com.simplifyi.appname.utility.SFBuildSettings;
import com.simplifyi.appname.utility.SFMockDataProvider;
import com.simplifyi.simplifyi_phase2.R;


public class SFFirstTimeTwoFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_first_time_two, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

}
