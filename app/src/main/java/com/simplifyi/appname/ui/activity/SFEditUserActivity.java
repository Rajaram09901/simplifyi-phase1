package com.simplifyi.appname.ui.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.network.request.SFMultipartUserRequest;
import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.network.restclient.SFUserRestClient;
import com.simplifyi.appname.utility.SFCameraUtility;
import com.simplifyi.appname.utility.SFDynamicPermissionManager;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import pl.aprilapps.easyphotopicker.EasyImage;

public class SFEditUserActivity extends SFBaseActivity {
    public static final String EDIT_PROFILE_FLOW = "edit_profile_flow";
    public static final int EDIT_PROFILE_FLOW_REGISTER = 1;
    public static final int EDIT_PROFILE_FLOW_EDIT = 2;
    private static final int MAX_USER_LENGTH = 6;
    private static final String GENDER_MALE = "Male";
    private static final String GENDER_FEMALE = "Female";
    private Button mContinueButton;
    private EditText mUserNameEditText;
    private String mGender;//Todo: there is no field for it in user json
    private EditText mDobEditText;
    private CircleImageView mProfilePicIv;
    private RadioGroup mGenderRadioGroup;
    private boolean mValidUsername = false;
    private boolean mValidDOB = false;
    private SFUser mCurrentUser;
    private int mEditFlow;
    private boolean mIsTncAccepted;

    private SFCameraUtility mCameraUtility;
    private Bitmap mProfilePicBitmap;
    private String mProfilePicPath = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        mEditFlow = getIntent().getIntExtra(EDIT_PROFILE_FLOW, -1);
        loadUser();
        requestCameraPermission();
        initializeUI();
        mCameraUtility = new SFCameraUtility();
        mCameraUtility.initialize(SFEditUserActivity.this);
    }

    private void loadUser() {
        showProgress(true);
        getCurrentUser(new Runnable() {
            @Override
            public void run() {
                showProgress(false);
                //TODO: load user object from sharedprefs
                mCurrentUser = SFSharedPrefs.getInstance(SFEditUserActivity.this).getUser();
                initializeUI();
            }
        });
    }

    private void initializeUI() {
        mContinueButton = findViewById(R.id.btn_continue);
        mUserNameEditText = findViewById(R.id.username_et);
        mDobEditText = findViewById(R.id.dob_et);
        findViewById(R.id.tnc_tv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToNextScreen(SFTermsAndConditionsActivity.class);
            }
        });
        CheckBox checkBox = findViewById(R.id.accept_tnc_checkbox);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mIsTncAccepted = isChecked;
                onProfileDataEdited();
            }
        });
        if (mEditFlow == EDIT_PROFILE_FLOW_EDIT && mCurrentUser != null) {
            //reset validation flags
            mValidDOB = false;
            mValidUsername = false;
            if (!SFUtility.isStringNullOrEmpty(mCurrentUser.getName())) {
                mUserNameEditText.setText(mCurrentUser.getName());
                mValidUsername = true;
            }
            if (!SFUtility.isStringNullOrEmpty(mCurrentUser.getDob())) {
                mDobEditText.setText(mCurrentUser.getDob());
                mValidDOB = true;
            }
            onProfileDataEdited();
        }

        mDobEditText.setKeyListener(null);
        mDobEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    onDOBFieldTapped();
                }
            }
        });
        mDobEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDOBFieldTapped();
            }
        });
        mProfilePicIv = findViewById(R.id.profile_pic_iv);
        //TODO: add glide
        if (mCurrentUser != null && !SFUtility.isStringNullOrEmpty(mCurrentUser.getProfileImageUrl())) {
            Glide.with(this).load(mCurrentUser.getProfileImageUrl()).into(mProfilePicIv);
        }
        mProfilePicIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPickImageSourceDialog();
            }
        });
        mUserNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mValidUsername = s.length() >= MAX_USER_LENGTH;
                onProfileDataEdited();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        mDobEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mValidDOB = true;
                onProfileDataEdited();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mContinueButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SFUtility.isInternetAvailable(SFEditUserActivity.this)) {
                    updateUser();
                } else {
                    showNoNetworkScreen();
                }
            }
        });
        mGenderRadioGroup = findViewById(R.id.radioGroup);
        mGenderRadioGroup.check(R.id.male);
        mGender = GENDER_MALE;
        mGenderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mGender = (checkedId == R.id.female) ? GENDER_FEMALE : GENDER_MALE;//TODO: currently there is no field in user json for Gender.. can we add it?
            }
        });
    }

    private void onProfileDataEdited() {
        boolean toBeEnabled = mValidDOB && mValidUsername;
        mContinueButton.setEnabled(toBeEnabled);
//        if (toBeEnabled) {
//            mContinueButton.setBackgroundResource(R.drawable.button_background_orange);
//        } else {
//            mContinueButton.setBackgroundResource(R.drawable.button_bg_rounded_corners);
//        }
    }

    private void updateUser() {
        showProgress(true);
        final String currentUserName = mUserNameEditText.getText().toString();
        final String dob = mDobEditText.getText().toString();
        final String nickName = currentUserName; //TODO: How can we capture Nick name from the user? currently there is no UI?
        final SFUser user = new SFUser(currentUserName, nickName, SFSharedPrefs.getInstance(this).getPhoneNumber(), SFUtility.getDeviceIMEI(this));
        if (!SFUtility.isStringNullOrEmpty(dob)) {
            user.setDob(dob);
        }
        SFUserRestClient manager = new SFUserRestClient();
        MediaType type = null;
        if (!SFUtility.isStringNullOrEmpty(mProfilePicPath)) {
            String mime = mCameraUtility.getMimeType(mProfilePicPath);
            type = MediaType.parse(mime);
        }
        SFMultipartUserRequest request = new SFMultipartUserRequest.Builder()
                .addUser(user)
                .addMimeType(type)
                .addProfilePicFilePath(mProfilePicPath)
                .build();

        manager.updateUser(request, new SFUserRestClient.NetworkCallListener() {
            @Override
            public void onCallFinished(SFGenericResponse genericResponse) {
                showProgress(false);
                if (genericResponse != null && genericResponse.getHeader().getStatusCode() == SFAPIResponseCode.SUCCESS) {
                    SFLogger.showToast(SFEditUserActivity.this, "Update user successful");
                    SFSharedPrefs.getInstance(SFEditUserActivity.this).putUser(user);
                    if (mEditFlow == EDIT_PROFILE_FLOW_REGISTER) {
                        goToNextScreen(SFTagSelectionActivity.class);
                    }
                    finish();

                } else {
                    SFLogger.showToast(SFEditUserActivity.this, "Update user unsuccessful");
                }

            }

            @Override
            public void onCallFinishedWithError(String error) {

            }
        });
    }

    private void showPickImageSourceDialog() {
        final Dialog picSourceDialog = new Dialog(this);
        picSourceDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        picSourceDialog.setCancelable(true);
        picSourceDialog.setContentView(R.layout.dialog_choose_image_source);
        picSourceDialog.show();

        picSourceDialog.findViewById(R.id.camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCameraUtility.openCamera(SFEditUserActivity.this);
                picSourceDialog.dismiss();
            }
        });
        picSourceDialog.findViewById(R.id.gallery_rl).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCameraUtility.openGallery(SFEditUserActivity.this);
                picSourceDialog.dismiss();
            }
        });
        picSourceDialog.findViewById(R.id.btn_no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picSourceDialog.cancel();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageCaptureIntent) {
        super.onActivityResult(requestCode, resultCode, imageCaptureIntent);
        mCameraUtility.handleActivityResult(requestCode, resultCode, imageCaptureIntent, this, new SFCameraUtility.ProfilePicCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> list, EasyImage.ImageSource imageSource, int i) {
                if (!list.isEmpty()) {
                    mProfilePicPath = list.get(0).getPath();
                    if (!SFUtility.isStringNullOrEmpty(mProfilePicPath)) {
                        mProfilePicBitmap = BitmapFactory.decodeFile(mProfilePicPath);
                        mProfilePicIv.setImageBitmap(mProfilePicBitmap);
                    }
                }
            }

            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                e.printStackTrace();
            }
        });
    }

    private void onDOBFieldTapped() {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mDobEditText.getWindowToken(), 0);

        DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dateOfMonth) {
                monthOfYear++;
                String day = dateOfMonth < 10 ? "0" + dateOfMonth : dateOfMonth + "";
                String month = monthOfYear < 10 ? "0" + monthOfYear : monthOfYear + "";
                mDobEditText.setText(day + "-" + month + "-" + year);

            }
        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(SFEditUserActivity.this, onDateSetListener, year, month, day);
        datePickerDialog.show();
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorAccent));
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorAccent));
    }

    private void requestCameraPermission() {
        List<String> permissions = new ArrayList<>();
        if (SFDynamicPermissionManager.isAndroidMOrAbove()) {
            if (!SFDynamicPermissionManager.wasThePermissionGranted(this, Manifest.permission.CAMERA)) {
                permissions.add(Manifest.permission.CAMERA);
            }
            if (!SFDynamicPermissionManager.wasThePermissionGranted(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
        }

        requestPermission(permissions, new PermissionCallback() {
            @Override
            public void onAllowTapped(List<String> permissions) {
//                SFLogger.showToast(SFEditUserActivity.this, permissions.size() + " permissions granted");

            }

            @Override
            public void onDenyTapped(List<String> permissions) {
                SFLogger.showToast(SFEditUserActivity.this, permissions.size() + " permissions denied");
            }

            @Override
            public void onDontAskChecked(List<String> permissions) {
                SFLogger.showToast(SFEditUserActivity.this, permissions.size() + "permissions Don't ask checked");
            }

            @Override
            public void onNotRequired() {
                //   SFLogger.showToast(SFEditUserActivity.this, "Permission Not required");
            }
        });
    }


}

