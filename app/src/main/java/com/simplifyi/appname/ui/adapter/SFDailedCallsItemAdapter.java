package com.simplifyi.appname.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.ui.activity.SFForumAnswersActivity;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.simplifyi_phase2.R;

import java.util.List;

public class SFDailedCallsItemAdapter extends RecyclerView.Adapter<SFDailedCallsItemAdapter.MyViewHolder> {
    private List<SFForum> mForumUsers;
    private Context mContext;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView forumUserNameTv;
        public TextView forumNameTv;
        public TextView replyTv;
        public TextView followerTv;
        public TextView primaryTagTv;
        public TextView secondaryTagTv;
        public ImageView reject_iv;
        View rootView;

        public MyViewHolder(View v) {
            super(v);
            rootView = v;
            forumUserNameTv = rootView.findViewById(R.id.forum_owner_name_tv);
            forumNameTv = rootView.findViewById(R.id.forum_question_tv);
            followerTv = rootView.findViewById(R.id.follower_tv);
            replyTv = rootView.findViewById(R.id.reply_tv);
            primaryTagTv = rootView.findViewById(R.id.textViewPrimaryTag);
            secondaryTagTv = rootView.findViewById(R.id.textViewSecondaryTag);
            reject_iv = rootView.findViewById(R.id.reject_iv);
        }
    }

    public void setData(List<SFForum> forumUsers) {
        mForumUsers = forumUsers;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public SFDailedCallsItemAdapter(Context context, List<SFForum> forumUsers) {
        mForumUsers = forumUsers;
        mContext = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public SFDailedCallsItemAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reusable_forum_list_item_template, parent, false);

        MyViewHolder vh = new MyViewHolder(itemView);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final SFForum forum = mForumUsers.get(position);
        if (forum != null) {
            if (forum.getForumUsers() != null && !forum.getForumUsers().isEmpty() && forum.getForumUsers().get(0)!= null) {
                holder.forumUserNameTv.setText(forum.getForumUsers().get(0).getNickName());
            }
            if (forum.getName() != null && !forum.getName().isEmpty()) {
                holder.forumNameTv.setText(forum.getName());
                holder.forumNameTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goToAnswersScreen(forum);
                    }
                });
            }
            holder.followerTv.setText(forum.getFollows() + " Followers");
            holder.replyTv.setText(forum.getAnswers().size() + " Replies");
            holder.replyTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  //  SFLogger.debugLog("SFForumItemAdapter", "Replies Tapped");
                    goToAnswersScreen(forum);
                }
            });
            holder.reject_iv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToAnswersScreen(forum);
                }
            });

            if (forum.getTags() != null && !forum.getTags().isEmpty()) {
                holder.primaryTagTv.setText(forum.getTags().get(0));
                if (forum.getTags().size() > 1) {
                    holder.secondaryTagTv.setText(forum.getTags().get(1));
                }

            }
        }
    }

    private void goToAnswersScreen(SFForum forum) {
        Intent in = new Intent(mContext, SFForumAnswersActivity.class);
        in.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        in.putExtra(SFForumAnswersActivity.SELECTED_FORUM, forum);
        mContext.startActivity(in);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mForumUsers.size();
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

}
