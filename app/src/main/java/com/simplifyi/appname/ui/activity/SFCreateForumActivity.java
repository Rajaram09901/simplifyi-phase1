package com.simplifyi.appname.ui.activity;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.simplifyi.appname.data.SFSharedPrefs;
import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.network.SFAPIResponseCode;
import com.simplifyi.appname.network.request.SFCreateForumRequest;
import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.ui.viewmodel.SFForumViewModel;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUIUtility;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class SFCreateForumActivity extends SFBaseAppBarActivity {
    public static final String FORUM_CATEGORY_INFO = "forum category info";
    public static final String EXISTING_FORUM = "existing_forum";
    private ImageView mCallNowIv;
    private List<String> mTagsInfo;
    private String mQuestion;
    private SFForumViewModel mForumListViewModel;
    private SFForum mSelectedForum;
    private EditText mQuestionEditText;
    private TextView locationTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_forum);
        configureActionBar(true, "Create Forum");
        mTagsInfo = getIntent().getStringArrayListExtra(SFCreateForumActivity.FORUM_CATEGORY_INFO);
        mSelectedForum = (SFForum) getIntent().getSerializableExtra(EXISTING_FORUM);
        if (mSelectedForum != null) {
            SFLogger.showToast(this, "a forum is passed!!");
            mTagsInfo = mSelectedForum.getTags();
        }
        initializeUI();
        initializeViewModel();

    }

    private void initializeUI() {
        SFUser currentUser = SFSharedPrefs.getInstance(this).getUser();
        if (currentUser != null) {
            ImageView profileIv = findViewById(R.id.owner_profile_pic);
            if (currentUser != null && !SFUtility.isStringNullOrEmpty(currentUser.getProfileImageUrl())) {
                Glide.with(this).load(currentUser.getProfileImageUrl()).into(profileIv);
            }
            TextView userNameTv = findViewById(R.id.forum_owner_name_tv);
            userNameTv.setText(currentUser.getNick());
        }
        findViewById(R.id.upload_et).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SFLogger.showToast(SFCreateForumActivity.this, "upload photo clicked");
            }
        });
        findViewById(R.id.language).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> strings = Arrays.asList(getResources().getStringArray(R.array.languages));
                SFUIUtility.showListPop(SFCreateForumActivity.this, "", (TextView) v, new ArrayList<>(strings));
            }
        });
        locationTextView = findViewById(R.id.location);
        locationTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SFCreateForumActivity.this, SFLocationActivity.class);
                startActivityForResult(intent, 9);
            }
        });
        mQuestionEditText = findViewById(R.id.textViewQuestion);
        if (mSelectedForum != null && !SFUtility.isStringNullOrEmpty(mSelectedForum.getName())) {
            mQuestionEditText.setText(mSelectedForum.getName());
        }
        if (mTagsInfo != null && !mTagsInfo.isEmpty()) {
            TextView primaryTag = findViewById(R.id.textViewPrimaryTag);
            TextView secondaryTag = findViewById(R.id.textViewSecondaryTag);
            primaryTag.setText(mTagsInfo.get(0));
    //        secondaryTag.setText(mTagsInfo.get(1));
        }
        mCallNowIv = findViewById(R.id.answercall);
        mCallNowIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mQuestion = mQuestionEditText.getText().toString();
                if (!SFUtility.isStringNullOrEmpty(mQuestion)) {
                    createForum();
                } else {
                    SFLogger.showToast(SFCreateForumActivity.this, "Forum description is empty!");
                }
            }
        });
    }

    private void initializeViewModel() {
        mForumListViewModel = ViewModelProviders.of(this).get(SFForumViewModel.class);
        final Observer<SFForum> observeForumCreateAPI = new Observer<SFForum>() {
            @Override
            public void onChanged(@Nullable SFForum forum) {
                showProgress(false);
                SFLogger.showToast(SFCreateForumActivity.this, "forum created is: " + forum);

                Intent intent = new Intent(SFCreateForumActivity.this, SFFindingBuddyActivity.class);
                //TODO: how do we get the forum object to transfer to next Activity with user info and all?
                intent.putExtra(SFInCallActivity.CALLABLE_FORUM, forum);
                goToNextScreen(intent);
                finish();
            }
        };
        final Observer<Integer> errorInCreatingForums = new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer errorCode) {
                if (errorCode == SFAPIResponseCode.Error.CREATE_FORUM_ERROR_TOKEN_EXPIRED) {
                    login(new LoginListener() {
                        @Override
                        public void onSuccess() {
                            createForum();
                        }

                        @Override
                        public void onFail() {
                            showProgress(false);
                            //refreshUI();
                        }
                    });
                } else {
                    showProgress(false);

                }
            }
        };
        mForumListViewModel.getNewlyCreatedForumContainer().observe(this, observeForumCreateAPI);
        mForumListViewModel.getErrorInForumCreating().observe(this, errorInCreatingForums);
    }

    private void createForum() {
        final SFCreateForumRequest request = new SFCreateForumRequest(mQuestion, new String[]{mTagsInfo.get(0), mTagsInfo.get(0)});
        showProgress(true);
        String token = SFSharedPrefs.getInstance(this).getSessionToken();
        if (SFUtility.isStringNullOrEmpty(token)) {
            login(new SFBaseActivity.LoginListener() {
                @Override
                public void onSuccess() {
                    String token = SFSharedPrefs.getInstance(SFCreateForumActivity.this).getSessionToken();
                    mForumListViewModel.createForum(token, request);
                }

                @Override
                public void onFail() {
                    //TODO:login may file with the error user doesn't exist. in this case, the user may have been removed by other means. how do client know about that?
                    SFLogger.showToast(SFCreateForumActivity.this, "Login onFail.So, not able to create forum");
                }
            });
        } else {
            mForumListViewModel.createForum(token, request);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 9 && resultCode == RESULT_OK) {
            String latLong = data.getStringExtra(SFLocationActivity.KEY_LATLONG);
            String address = data.getStringExtra(SFLocationActivity.KEY_ADDRESS);
            locationTextView.setText(address);
        }
    }
}
