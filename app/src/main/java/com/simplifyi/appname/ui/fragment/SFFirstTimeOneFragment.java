package com.simplifyi.appname.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.simplifyi.simplifyi_phase2.R;


public class SFFirstTimeOneFragment extends Fragment {
   // public static final long TIME_OUT = 3 * 1000;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_first_time_one, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.first_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }



  /*  @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_time_one);
        final Runnable launchActivity = new Runnable() {
            @Override
            public void run() {
                goToNextScreen(SFFirstTimeTwoFragment.class);
                finish();
            }
        };
        final Handler handler = new Handler();
        handler.postDelayed(launchActivity, TIME_OUT);
        findViewById(R.id.first_view).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacks(launchActivity);
                launchActivity.run();
            }
        });
    }
*/
}
