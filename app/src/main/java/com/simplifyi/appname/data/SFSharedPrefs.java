package com.simplifyi.appname.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.utility.SFUtility;
import com.sinch.gson.Gson;

public class SFSharedPrefs {
    private static final String PREFS_NAME = "simplyfy_prefs";
    private static SFSharedPrefs instance;
    private SharedPreferences mPrefs;
    public static final long INVALID_PHONE_NUMBER = -1;
    private static final String MOBILE_NUMBER = "mobile_number";

    private static final String SESSION_TOKEN = "session_token";
    private static final String CURRENT_USER = "current_user";
    private static final String FIRST_TIME_USER_FLAG = "first_time_user_flag";

    public int currentSideNavigationId = -1;

    private SFSharedPrefs(Context context) {
        mPrefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static SFSharedPrefs getInstance(Context context) {
        if (instance == null) {
            instance = new SFSharedPrefs(context);
        }
        return instance;
    }

    public void putPhoneNumber(long number) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putLong(MOBILE_NUMBER, number);
        editor.commit();
    }

    public long getPhoneNumber() {
        return mPrefs.getLong(MOBILE_NUMBER, INVALID_PHONE_NUMBER);
    }

    public void putSessionToken(String token) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(SESSION_TOKEN, token);
        editor.commit();
    }

    public String getSessionToken() {
        return mPrefs.getString(SESSION_TOKEN, null);
    }

    public void clearPrefs() {
        mPrefs.edit().clear().commit();
    }

    public void putUser(SFUser user) {
        if (user != null) {
            Gson gson = new Gson();
            String json = gson.toJson(user);
            SharedPreferences.Editor editor = mPrefs.edit();
            editor.putString(CURRENT_USER, json);
            editor.commit();
        }
    }

    public SFUser getUser() {
        String json = mPrefs.getString(CURRENT_USER, null);
        if (!SFUtility.isStringNullOrEmpty(json)) {
            Gson gson = new Gson();
            SFUser user = gson.fromJson(json, SFUser.class);
            return user;
        }
        return null;
    }

    public void setFirstTimeUser(boolean firstTimeUser) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putBoolean(FIRST_TIME_USER_FLAG, firstTimeUser);
        editor.apply();
    }

    public boolean isFirstTimeUser() {
        return mPrefs.getBoolean(FIRST_TIME_USER_FLAG, true);
    }
}
