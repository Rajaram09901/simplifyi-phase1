package com.simplifyi.appname.network.apiinterface;


import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.network.request.SFUserSettingsRequest;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.utility.SFConstants;
import com.simplifyi.appname.utility.SFEndPoint;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.QueryMap;

public interface SFUserAPIInterface {
    @POST(SFEndPoint.PATH_USERS)
    Call<SFGenericResponse> doPostUser(@Body SFUser SFUser);

    @Multipart
    @PUT(SFEndPoint.PATH_USERS)
    Call<SFGenericResponse> doPutUser(@Part("phone") RequestBody phone, @Part("name") RequestBody name, @Part("nick") RequestBody nick,
                                      @Part("dob") RequestBody dob, @Part MultipartBody.Part photo);
    @DELETE(SFEndPoint.PATH_USERS)
    Call<SFGenericResponse> doDeleteUser(@Body SFUser SFUser);

    @GET(SFEndPoint.PATH_USERS)
    Call<List<SFGenericResponse>> doGetAllUsers(@Body SFUser SFUser);

    @GET(SFEndPoint.PATH_USERS)
    Call<List<SFGenericResponse>> doGetUsers(@Header("limit") int pageSize, @Header("limitstart") int startIndex);//replace Header from QueryMap

    @GET(SFEndPoint.PATH_GET_USER_BY_TOKEN)
    Call<SFGenericResponse<SFUser>> doGetCurrentUser(@Header("Authorization") String token);

    @GET(SFEndPoint.PATH_GET_USER_BY_ID)
    Call<SFGenericResponse<SFUser>> doGetUserById(@QueryMap Map<String, String> queryParams, @Header(SFConstants.Authorization) String token);

    @POST(SFEndPoint.PATH_GET_USER_SETTINGS)
    Call<SFGenericResponse> doPostUserSettings(@Body SFUserSettingsRequest request, @Header(SFConstants.Authorization) String token);
}


/**
 * The snapshot of the Multipart Http request which is used to send files in http
 * OkHttpClient client = new OkHttpClient();
 * <p>
 * MediaType mediaType = MediaType.parse("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
 * RequestBody body = RequestBody.create(mediaType, "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n
 * Content-Disposition: form-data; name=\"phone\"\r\n\r\n1234567801\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n
 * Content-Disposition: form-data; name=\"name\"\r\n\r\nSFUser01\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n
 * Content-Disposition: form-data; name=\"nick\"\r\n\r\nSFUser01\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n
 * Content-Disposition: form-data; name=\"dob\"\r\n\r\n12-06-1985\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\n
 * Content-Disposition: form-data; name=\"photo\"; filename=\"temp-forum-image.png\"\r\nContent-Type: image/png\r\n\r\n\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--");
 * Request request = new Request.Builder()
 * .url("http://18.222.136.228:10010/users")
 * .put(body)
 * .addHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
 * .addHeader("Content-Type", "application/json")
 * .addHeader("cache-control", "no-cache")
 * .addHeader("Postman-Token", "2ce27cc0-a285-4f93-8f25-5bce02ca1c2a")
 * .build();
 * <p>
 * Response response = client.newCall(request).execute();
 * How to implement this in Retrofit ?
 *
 * @Multipart
 * @POST("user/updateprofile") Observable<ResponseBody> updateProfile(@Part("user_id") RequestBody id,
 * @Part("full_name") RequestBody fullName,
 * @Part MultipartBody.Part image,
 * @Part("other") RequestBody other);
 * <p>
 * //pass it like this
 * File file = new File("/storage/emulated/0/Download/Corrections 6.jpg");
 * RequestBody requestFile =
 * RequestBody.create(MediaType.parse("multipart/form-data"), file);
 * <p>
 * // MultipartBody.Part is used to send also the actual file name
 * MultipartBody.Part imagePart =
 * MultipartBody.Part.createFormData("image", file.getName(), requestFile);
 * <p>
 * // add another part within the multipart request
 * RequestBody fullNameBody =
 * RequestBody.create(MediaType.parse("multipart/form-data"), "Your Name");
 * <p>
 * service.updateProfile(id, fullNameBody, imagePart, other);
 * How to implement this in Retrofit ?
 * @Multipart
 * @POST("user/updateprofile") Observable<ResponseBody> updateProfile(@Part("user_id") RequestBody id,
 * @Part("full_name") RequestBody fullName,
 * @Part MultipartBody.Part image,
 * @Part("other") RequestBody other);
 * <p>
 * //pass it like this
 * File file = new File("/storage/emulated/0/Download/Corrections 6.jpg");
 * RequestBody requestFile =
 * RequestBody.create(MediaType.parse("multipart/form-data"), file);
 * <p>
 * // MultipartBody.Part is used to send also the actual file name
 * MultipartBody.Part imagePart =
 * MultipartBody.Part.createFormData("image", file.getName(), requestFile);
 * <p>
 * // add another part within the multipart request
 * RequestBody fullNameBody =
 * RequestBody.create(MediaType.parse("multipart/form-data"), "Your Name");
 * <p>
 * service.updateProfile(id, fullNameBody, imagePart, other);
 */


/** How to implement this in Retrofit ?
 *
 * @Multipart
 * @POST("user/updateprofile")
 * Observable<ResponseBody> updateProfile(@Part("user_id") RequestBody id,
 *                                        @Part("full_name") RequestBody fullName,
 *                                        @Part MultipartBody.Part image,
 *                                        @Part("other") RequestBody other);
 *
 * //pass it like this
 * File file = new File("/storage/emulated/0/Download/Corrections 6.jpg");
 * RequestBody requestFile =
 *         RequestBody.create(MediaType.parse("multipart/form-data"), file);
 *
 * // MultipartBody.Part is used to send also the actual file name
 * MultipartBody.Part imagePart =
 *         MultipartBody.Part.createFormData("image", file.getName(), requestFile);
 *
 * // add another part within the multipart request
 * RequestBody fullNameBody =
 *         RequestBody.create(MediaType.parse("multipart/form-data"), "Your Name");
 *
 * service.updateProfile(id, fullNameBody, imagePart, other);
 */
