package com.simplifyi.appname.network.request;


import com.google.gson.annotations.SerializedName;

public class SFRateUserRequestBody {

    @SerializedName("forumid")
    private String forumId;
    @SerializedName("id")
    private String userId;
    @SerializedName("rating")
    private int rating;

    public SFRateUserRequestBody(String forumId, String userId, int rating) {
        this.userId = userId;
        this.rating = rating;
        this.forumId = forumId;
    }

    public String getUserId() {
        return userId;
    }

    public int getRating() {
        return rating;
    }
}

