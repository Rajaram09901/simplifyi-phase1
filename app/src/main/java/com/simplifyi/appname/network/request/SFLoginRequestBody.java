package com.simplifyi.appname.network.request;


import com.google.gson.annotations.SerializedName;

public class SFLoginRequestBody {


    @SerializedName("phone")
    public long phone;
    @SerializedName("imeiNumber")
    public String imeiNumber;

    public SFLoginRequestBody(String imeiNumber, long phone) {
        this.imeiNumber = imeiNumber;
        this.phone = phone;
    }


}

