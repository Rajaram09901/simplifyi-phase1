package com.simplifyi.appname.network.apiinterface;


import com.simplifyi.appname.network.request.SFLoginRequestBody;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.network.response.SFLoginToken;
import com.simplifyi.appname.utility.SFEndPoint;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SFLoginAPIInterface {

    @POST(SFEndPoint.PATH_LOGIN)
    Call<SFGenericResponse<SFLoginToken>> doPostLogin(@Body SFLoginRequestBody loginRequestBody);

}

