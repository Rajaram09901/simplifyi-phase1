package com.simplifyi.appname.network.request;


import com.google.gson.annotations.SerializedName;

public class SFDisconnectCallRequest {

    @SerializedName("id")
    private String forumId;

    public SFDisconnectCallRequest(String forumId) {
        this.forumId = forumId;
    }

    public String getForumId() {
        return forumId;
    }
}

