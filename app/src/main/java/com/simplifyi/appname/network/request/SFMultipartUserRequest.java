package com.simplifyi.appname.network.request;

import com.simplifyi.appname.utility.SFUtility;

import okhttp3.MediaType;

public class SFMultipartUserRequest {
    private SFUser user;
    private String profilePicPath;
    private MediaType fileMimeType;

    private SFMultipartUserRequest() {

    }

    public SFUser getUser() {
        return user;
    }


    public String getProfilePicPath() {
        return profilePicPath;
    }

    public MediaType getFileMimeType() {
        return fileMimeType;
    }

    public static class Builder {
        SFMultipartUserRequest request;

        public Builder() {
            request = new SFMultipartUserRequest();
        }

        public SFMultipartUserRequest build() {
            if (request.user != null && SFUtility.isStringNullOrEmpty(request.user.getPhone() + "")) {
                return null;
            }
            return request;
        }

        public Builder addUser(SFUser user) {
            request.user = user;
            return this;
        }

        public Builder addProfilePicFilePath(String fileUri) {
            request.profilePicPath = fileUri;
            return this;
        }

        public Builder addMimeType(MediaType type) {
            request.fileMimeType = type;
            return this;
        }
    }
}
