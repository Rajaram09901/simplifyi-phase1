package com.simplifyi.appname.network.restclient;


import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.utility.SFLogger;

import retrofit2.Call;
import retrofit2.Response;

public class SFBaseRestClient {
    protected NetworkCallListener mNetworkCallListener;

    public interface NetworkCallListener {
        void onCallFinished(SFGenericResponse genericResponse);

        void onCallFinishedWithError(String error);
    }

    protected void processResponse(Response response) {
        if (response != null && response.isSuccessful()) {
            SFGenericResponse genericResponse = (SFGenericResponse) response.body();
            if (mNetworkCallListener != null) {
                mNetworkCallListener.onCallFinished(genericResponse);
            }
        } else {
            if (mNetworkCallListener != null) {
                mNetworkCallListener.onCallFinishedWithError("un-known error");
            }
        }
    }

    protected void processError(Call call, Throwable t) {
        call.cancel();
        SFLogger.printErrorStack(t);
        if (mNetworkCallListener != null) {
            mNetworkCallListener.onCallFinishedWithError(t.getMessage());
        }
    }
}
