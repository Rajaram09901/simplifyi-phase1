package com.simplifyi.appname.network.response;

import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class SFGenericResponse<T> {
    SFHeader header;
    SFBody<T> body;


    // Getter Methods

    public SFHeader getHeader() {
        return header;
    }

    public SFBody getBody() {
        return body;
    }

    // Setter Methods

    public void setHeader(SFHeader header) {
        this.header = header;
    }

    public void setBody(SFBody body) {
        this.body = body;
    }


    public static class SFBody<T> {
        private int count;
        ArrayList<T> data = new ArrayList<T>();

        public ArrayList<T> getData() {
            return data;
        }

        public void setData(ArrayList<T> data) {
            this.data = data;
        }

// Getter Methods

        public int getCount() {
            return count;
        }

        // Setter Methods

        public void setCount(int count) {
            this.count = count;
        }

        @Override
        public String toString() {
            return "SFBody{" +
                    "count=" + count +
                    ", data=" + data +
                    '}';
        }
    }


    public static class SFHeader {
        @SerializedName("status-code")
        private int statusCode;
        @SerializedName("status-message")
        private String statusMessage;


        // Getter Methods

        public int getStatusCode() {
            return statusCode;
        }

        public String getStatusMessage() {
            return statusMessage;
        }

        // Setter Methods

        public void setStatusCode(int statusCode) {
            this.statusCode = statusCode;
        }

        public void setStatusMessage(String statusMessage) {
            this.statusMessage = statusMessage;
        }

        @Override
        public String toString() {
            return "SFHeader{" +
                    "statusCode=" + statusCode +
                    ", statusMessage='" + statusMessage + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "SFGenericResponse{" +
                "header=" + header +
                ", body=" + body +
                '}';
    }
}