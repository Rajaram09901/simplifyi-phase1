package com.simplifyi.appname.network.request;


import com.google.gson.annotations.SerializedName;

public class SFCallEndOrInitiatedRequest {

    @SerializedName("receivedBy")
    private String callReceiverUserId;
    @SerializedName("forumid")
    private String callableForumId;

    public SFCallEndOrInitiatedRequest(String callReceiverUserId, String callableForumId) {
        this.callReceiverUserId = callReceiverUserId;
        this.callableForumId = callableForumId;
    }

    public String getCallReceiverUserId() {
        return callReceiverUserId;
    }

    public void setCallReceiverUserId(String callReceiverUserId) {
        this.callReceiverUserId = callReceiverUserId;
    }

    public String getCallableForumId() {
        return callableForumId;
    }

    public void setCallableForumId(String callableForumId) {
        this.callableForumId = callableForumId;
    }
}

