package com.simplifyi.appname.network;


import com.simplifyi.appname.utility.SFBuildSettings;
import com.simplifyi.appname.utility.SFEndPoint;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFMockDataProvider;
import com.simplifyi.appname.utility.SFUtility;
import com.simplifyi.simplifyi_phase2.BuildConfig;

import java.io.IOException;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SFRetrofit {

    private static Retrofit retrofit = null;
    private static Interceptor requestResponseInterceptor = new Interceptor() {
        @Override
        public Response intercept(Interceptor.Chain chain) throws IOException {
            Request originalRequest = chain.request();
            if (SFBuildSettings.BUILD_TYPE == SFBuildSettings.BuildType.DEBUG_WITH_MOCK_BACKEND) {
                return getMockResponse(originalRequest);
            }

            Request finalRequest = addGlobalRequestParams(originalRequest);
            Response response = chain.proceed(finalRequest);

            return response;
        }
    };

    public static Retrofit getClient() {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();//why do we need it?
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        //OkHttp is used as the transport here..retrofit wraps around it.
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(loggingInterceptor).addInterceptor(requestResponseInterceptor).build();


        retrofit = new Retrofit.Builder()
                .baseUrl(SFEndPoint.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit;
    }

    private static Request addGlobalRequestParams(Request original) {
        return original.newBuilder()
                .header("application-id", BuildConfig.APPLICATION_ID)
                .header("Version-name", BuildConfig.VERSION_NAME)
                .header("Version-code", BuildConfig.VERSION_CODE + "")
                .header("Build-type", BuildConfig.BUILD_TYPE)
                .header("Build-flavor", BuildConfig.FLAVOR)
                .method(original.method(), original.body())
                .build();
    }

    private static Response getMockResponse(Request request) {
        List<String> pathSegments = request.url().pathSegments();
        StringBuffer buffer = new StringBuffer();
        for (String item : pathSegments) {
            buffer.append("/");
            buffer.append(item);
        }
        String path = buffer.toString();
        SFLogger.debugLog("PATH", path);
        if (path.equals(SFEndPoint.PATH_USERS)) {
            switch (request.method()) {
                case "POST":
                case "GET":
                case "DELETE":
                case "PUT":
                    path = SFEndPoint.PATH_CREATE_USER;
                    break;
               /* case "GET": path = SFEndPoint.PATH_CREATE_USER; // with paging. we will look at this later
                break;*/
            }
        } else if (path.equals(SFEndPoint.PATH_FORUM)) {
            switch (request.method()) {
                case "POST":
                    path = SFEndPoint.PATH_CREATE_FORUM;
                    break;
                case "GET":
                    String searchKey = request.url().queryParameter("search");
                    if (SFUtility.isStringNullOrEmpty(searchKey)) {
                        path = SFEndPoint.PATH_SEARCH__FORUMS;
                    } else {
                        path = SFEndPoint.PATH_GET_ALL_FORUMS;
                    }
                    break;
            }

        }
        String bodyJson = SFMockDataProvider.getInstance(null).getJsonFor(path);
        Response response = new Response.Builder()
                .code(200)
                .message("")
                .request(request)
                .protocol(Protocol.HTTP_1_0)
                .addHeader("content-type", "application/json")
                .body(ResponseBody.create(MediaType.parse("application/json"), bodyJson.getBytes()))
                .build();
        return response;
    }

}


