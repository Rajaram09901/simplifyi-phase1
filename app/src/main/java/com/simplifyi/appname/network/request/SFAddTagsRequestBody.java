package com.simplifyi.appname.network.request;


import com.google.gson.annotations.SerializedName;

public class SFAddTagsRequestBody {

    @SerializedName("tags")
    public String tags;
    @SerializedName("phone")
    public long phone;
    @SerializedName("languages")
    public String languages;

    public SFAddTagsRequestBody(String tags, String languages, long phone) {
        this.tags = tags;
        this.phone = phone;
        this.languages = languages;
    }
}

