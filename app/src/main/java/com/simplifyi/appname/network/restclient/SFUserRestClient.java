package com.simplifyi.appname.network.restclient;


import com.simplifyi.appname.network.SFRetrofit;
import com.simplifyi.appname.network.apiinterface.SFUserAPIInterface;
import com.simplifyi.appname.network.request.SFMultipartUserRequest;
import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.network.request.SFUserSettingsRequest;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SFUserRestClient extends SFBaseRestClient {
    private SFUserAPIInterface mApiInterface;

    public SFUserRestClient() {
        mApiInterface = SFRetrofit.getClient().create(SFUserAPIInterface.class);
    }

    public void createUser(SFUser SFUser, NetworkCallListener networkCallListener) {
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("createUser:Request", SFUser.toString());
        Call<SFGenericResponse> call = mApiInterface.doPostUser(SFUser);

        call.enqueue(new Callback<SFGenericResponse>() {
            @Override
            public void onResponse(Call<SFGenericResponse> call, Response<SFGenericResponse> response) {
                SFLogger.debugLog("createUser()", "ResponseCode:" + response.code());
                SFLogger.debugLog("createUser()", "ResponseBody:" + response.body());
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse> call, Throwable t) {
                processError(call, t);
            }
        });
    }

    public void updateUser(SFMultipartUserRequest request, NetworkCallListener networkCallListener) {
        mNetworkCallListener = networkCallListener;

        RequestBody namePart = RequestBody.create(okhttp3.MultipartBody.FORM, request.getUser().getName());
        RequestBody nickPart = RequestBody.create(okhttp3.MultipartBody.FORM, request.getUser().getName());
        RequestBody dobPart = RequestBody.create(okhttp3.MultipartBody.FORM, request.getUser().getDob());
        RequestBody phonePart = RequestBody.create(okhttp3.MultipartBody.FORM, request.getUser().getPhone() + "");

        MultipartBody.Part photoPart = null;
        if (!SFUtility.isStringNullOrEmpty(request.getProfilePicPath())) {
            File file = new File(request.getProfilePicPath());
            RequestBody picPart = RequestBody.create(request.getFileMimeType(), file);
            photoPart = MultipartBody.Part.createFormData("photo", file.getName(), picPart);
        }
        //RequestBody.create(okhttp3.MultipartBody.FORM, user.getProfileImageUrl());
        Call<SFGenericResponse> call = mApiInterface.doPutUser(phonePart, namePart,nickPart,dobPart,photoPart );

        call.enqueue(new Callback<SFGenericResponse>() {
            @Override
            public void onResponse(Call<SFGenericResponse> call, Response<SFGenericResponse> response) {
                SFLogger.debugLog("updateUser()", "ResponseCode:" + response.code());
                SFLogger.debugLog("updateUser()", "ResponseBody:" + response.body());
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse> call, Throwable t) {
                processError(call, t);
            }
        });
    }

    public void getCurrentUser(String token, NetworkCallListener networkCallListener) {
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("getCurrentUser()", "Get current user nickname");
        Call<SFGenericResponse<SFUser>> call = mApiInterface.doGetCurrentUser(token);
        call.enqueue(new Callback<SFGenericResponse<SFUser>>() {
            @Override
            public void onResponse(Call<SFGenericResponse<SFUser>> call, Response<SFGenericResponse<SFUser>> response) {
                SFLogger.debugLog("getCurrentUser()", "ResponseCode:" + response.code());
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse<SFUser>> call, Throwable t) {
                processError(call, t);
            }
        });
    }

    public void getUserById(String userId, String token, NetworkCallListener networkCallListener) {
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("getUserById()", "Get current user id= = " + userId);
        Map<String, String> queryParam = new HashMap<>();
        queryParam.put("userid", userId);
        Call<SFGenericResponse<SFUser>> call = mApiInterface.doGetUserById(queryParam, token);
        call.enqueue(new Callback<SFGenericResponse<SFUser>>() {
            @Override
            public void onResponse(Call<SFGenericResponse<SFUser>> call, Response<SFGenericResponse<SFUser>> response) {
                SFLogger.debugLog("getUserById()", "ResponseCode:" + response.code());
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse<SFUser>> call, Throwable t) {
                processError(call, t);
            }
        });
    }

    public void postUserSettings(SFUserSettingsRequest request, String token, NetworkCallListener networkCallListener) {
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("postUserSettings()", "current user settings:" + request);
        Call<SFGenericResponse> call = mApiInterface.doPostUserSettings(request, token);
        call.enqueue(new Callback<SFGenericResponse>() {
            @Override
            public void onResponse(Call<SFGenericResponse> call, Response<SFGenericResponse> response) {
                SFLogger.debugLog("postUserSettings()", "ResponseCode:" + response.code());
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse> call, Throwable t) {
                processError(call, t);
            }
        });
    }
}
