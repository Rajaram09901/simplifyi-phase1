package com.simplifyi.appname.network.apiinterface;


import com.mindorks.placeholderview.annotations.Position;
import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.network.request.SFAnswerRequest;
import com.simplifyi.appname.network.request.SFCreateForumRequest;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.utility.SFConstants;
import com.simplifyi.appname.utility.SFEndPoint;


import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface SFForumAPIInterface{

    @GET(SFEndPoint.PATH_FORUM)
    Call<SFGenericResponse<SFForum>> doGetForumsForCurrentUser(@QueryMap Map<String, String> queryParams,@Header(SFConstants.Authorization) String token);

    /* @GET(SFEndPoint.PATH_FORUM)
    Call<SFGenericResponse<SFForum>> doGetForumsForSearchKey(@QueryMap Map<String, String> queryParams, @Header(Authorization) String token);
*/
    @POST(SFEndPoint.PATH_CREATE_FORUM_ANSWER)
    Call<SFGenericResponse<Void>> doPostAnswer(@Body SFAnswerRequest request);

    @POST(SFEndPoint.PATH_FORUM)
    Call<SFGenericResponse<SFForum>> doPostForum(@Body SFCreateForumRequest request,@Header(SFConstants.Authorization) String token);

    @GET(SFEndPoint.PATH_GET_ALL_CALLABLE_FORUMS)
    Call<SFGenericResponse<SFForum>> doGetForumCalls(@Header(SFConstants.Authorization) String token);

    @GET(SFEndPoint.PATH_FORUM)
    Call<SFGenericResponse<SFForum>> doSearchForums(@QueryMap Map<String, String> queryParams);//"search":"search-key"

    @POST(SFEndPoint.PATH_FORUM_DELETE)
    Call<SFGenericResponse<String>> doDeleteForum(@QueryMap Map<String, String> deleteForum);
}

