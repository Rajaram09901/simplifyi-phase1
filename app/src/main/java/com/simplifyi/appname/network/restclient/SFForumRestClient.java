package com.simplifyi.appname.network.restclient;


import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.network.SFRetrofit;
import com.simplifyi.appname.network.apiinterface.SFForumAPIInterface;
import com.simplifyi.appname.network.request.SFAnswerRequest;
import com.simplifyi.appname.network.request.SFCreateForumRequest;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.utility.SFLogger;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SFForumRestClient extends SFBaseRestClient {
    private SFForumAPIInterface mApiInterface;

    public SFForumRestClient() {
        mApiInterface = SFRetrofit.getClient().create(SFForumAPIInterface.class);
    }


    public void getForumsForCurrentUser(boolean getCallableForums, Map queryParamsMap, NetworkCallListener networkCallListener) {
        mNetworkCallListener = networkCallListener;
        Call<SFGenericResponse<SFForum>> call;
        String token = "";
        if (queryParamsMap != null && queryParamsMap.containsKey("token")) {
            token = queryParamsMap.get("token").toString();
            queryParamsMap.remove("token");
        }
        if (getCallableForums) {
            call = mApiInterface.doGetForumCalls(token);
        } else {
            call = mApiInterface.doGetForumsForCurrentUser(queryParamsMap, token);
        }

        call.enqueue(new Callback<SFGenericResponse<SFForum>>() {
            @Override
            public void onResponse(Call<SFGenericResponse<SFForum>> call, Response<SFGenericResponse<SFForum>> response) {
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse<SFForum>> call, Throwable t) {
                processError(call, t);
            }
        });
    }

    //TODO: remove the below method later...
    public void searchForums(String token, String searchKey, NetworkCallListener networkCallListener) {
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("login:Request", token);
        Map queryParamsMap = new HashMap();
        queryParamsMap.put("token", token);
        queryParamsMap.put("search", searchKey);
        Call<SFGenericResponse<SFForum>> call = mApiInterface.doSearchForums(queryParamsMap);
        call.enqueue(new Callback<SFGenericResponse<SFForum>>() {
            @Override
            public void onResponse(Call<SFGenericResponse<SFForum>> call, Response<SFGenericResponse<SFForum>> response) {
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse<SFForum>> call, Throwable t) {
                processError(call, t);
            }
        });
    }

    public void postAnswer(SFAnswerRequest answerRequest, NetworkCallListener networkCallListener) {
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("answer:Request", answerRequest.toString());
        Call<SFGenericResponse<Void>> call = mApiInterface.doPostAnswer(answerRequest);

        call.enqueue(new Callback<SFGenericResponse<Void>>() {
            @Override
            public void onResponse(Call<SFGenericResponse<Void>> call, Response<SFGenericResponse<Void>> response) {
                SFLogger.debugLog("postAnswer()", "ResponseCode:" + response.code());
                SFLogger.debugLog("postAnswer()", "ResponseBody:" + response.body());
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse<Void>> call, Throwable t) {
                processError(call, t);
            }
        });
    }

    public void createForum(String token, SFCreateForumRequest createForumRequest, NetworkCallListener networkCallListener) {
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("create forum:Request", createForumRequest.toString());
        Call<SFGenericResponse<SFForum>> call = mApiInterface.doPostForum(createForumRequest, token);

        call.enqueue(new Callback<SFGenericResponse<SFForum>>() {
            @Override
            public void onResponse(Call<SFGenericResponse<SFForum>> call, Response<SFGenericResponse<SFForum>> response) {
                SFLogger.debugLog("postAnswer()", "ResponseCode:" + response.code());
                SFLogger.debugLog("postAnswer()", "ResponseBody:" + response.body());
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse<SFForum>> call, Throwable t) {
                processError(call, t);
            }
        });
    }
}
