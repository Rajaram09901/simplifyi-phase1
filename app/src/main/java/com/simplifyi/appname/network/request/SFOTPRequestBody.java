package com.simplifyi.appname.network.request;


import com.google.gson.annotations.SerializedName;

public class SFOTPRequestBody {

    @SerializedName("otp")
    public int otp;
    @SerializedName("phone")
    public long phone;

    public SFOTPRequestBody(int otp, long phone) {
        this.otp = otp;
        this.phone = phone;
    }


}

