package com.simplifyi.appname.network.restclient;


import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.network.SFRetrofit;
import com.simplifyi.appname.network.apiinterface.SFCallAPIInterface;
import com.simplifyi.appname.network.request.SFCallEndOrInitiatedRequest;
import com.simplifyi.appname.network.request.SFDisconnectCallRequest;
import com.simplifyi.appname.network.request.SFRateUserRequestBody;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.utility.SFLogger;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SFCallRestClient extends SFBaseRestClient {
    private SFCallAPIInterface mApiInterface;

    public SFCallRestClient() {
        mApiInterface = SFRetrofit.getClient().create(SFCallAPIInterface.class);
    }

    public void disconnectCall(String token, String forumId, NetworkCallListener networkCallListener) {
        SFDisconnectCallRequest callRequest = new SFDisconnectCallRequest(forumId);
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("disconnectCall:Request.forumId", forumId);
        Call<SFGenericResponse> call = mApiInterface.doDisconnectCall(callRequest, token);
        call.enqueue(new Callback<SFGenericResponse>() {
            @Override
            public void onResponse(Call<SFGenericResponse> call, Response<SFGenericResponse> response) {
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse> call, Throwable t) {
                processError(call, t);
            }
        });
    }

    public void rateUser(String forumId, String userId, int rating, String token, NetworkCallListener networkCallListener) {
        Map<String, String> query = new HashMap<>();
        query.put("Authorization", token);
        SFRateUserRequestBody rateUserRequest = new SFRateUserRequestBody(forumId, userId, rating);
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("rateUser():Request.userId", userId);
        Call<SFGenericResponse> call = mApiInterface.doRateUser(rateUserRequest, token);

        call.enqueue(new Callback<SFGenericResponse>() {
            @Override
            public void onResponse(Call<SFGenericResponse> call, Response<SFGenericResponse> response) {
                SFLogger.debugLog("rateUser()", "ResponseCode:" + response.code());
                SFLogger.debugLog("rateUser()", "ResponseBody:" + response.body());
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse> call, Throwable t) {
                processError(call, t);
            }
        });
    }


    public void markCallInitiated(String token, String forumId, String callReceivedUserId, NetworkCallListener networkCallListener) {
        SFCallEndOrInitiatedRequest callRequest = new SFCallEndOrInitiatedRequest(callReceivedUserId, forumId);
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("markCallInitiated:Request.forumId", forumId);
        Call<SFGenericResponse> call = mApiInterface.doCallInitiated(callRequest, token);
        call.enqueue(new Callback<SFGenericResponse>() {
            @Override
            public void onResponse(Call<SFGenericResponse> call, Response<SFGenericResponse> response) {
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse> call, Throwable t) {
                processError(call, t);
            }
        });
    }

    public void markCallEnded(String token, String forumId, String callReceivedUserId, NetworkCallListener networkCallListener) {
        SFCallEndOrInitiatedRequest callRequest = new SFCallEndOrInitiatedRequest(callReceivedUserId, forumId);
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("markCallEnded:Request.forumId", forumId);
        Call<SFGenericResponse> call = mApiInterface.doCallEnds(callRequest, token);
        call.enqueue(new Callback<SFGenericResponse>() {
            @Override
            public void onResponse(Call<SFGenericResponse> call, Response<SFGenericResponse> response) {
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse> call, Throwable t) {
                processError(call, t);
            }
        });
    }

    public void getDialedCalls(String token, NetworkCallListener networkCallListener) {
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("getDialedCalls:Request.forumId");
        Call<SFGenericResponse<SFForum>> call = mApiInterface.doGetDialedCalls(token);
        call.enqueue(new Callback<SFGenericResponse<SFForum>>() {
            @Override
            public void onResponse(Call<SFGenericResponse<SFForum>> call, Response<SFGenericResponse<SFForum>> response) {
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse<SFForum>> call, Throwable t) {
                processError(call, t);
            }
        });
    }
    public void getReceivedCalls(String token, NetworkCallListener networkCallListener) {
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("getReceivedCalls:Request.forumId");
        Call<SFGenericResponse<SFForum>> call = mApiInterface.doGetReceivedCalls(token);
        call.enqueue(new Callback<SFGenericResponse<SFForum>>() {
            @Override
            public void onResponse(Call<SFGenericResponse<SFForum>> call, Response<SFGenericResponse<SFForum>> response) {
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse<SFForum>> call, Throwable t) {
                processError(call, t);
            }
        });
    }
    public void getFollowsCalls(String token, NetworkCallListener networkCallListener) {
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("getFollowsCalls():Request.forumId");
        Call<SFGenericResponse<SFForum>> call = mApiInterface.doGetFollowsCalls(token);
        call.enqueue(new Callback<SFGenericResponse<SFForum>>() {
            @Override
            public void onResponse(Call<SFGenericResponse<SFForum>> call, Response<SFGenericResponse<SFForum>> response) {
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse<SFForum>> call, Throwable t) {
                processError(call, t);
            }
        });
    }
}
