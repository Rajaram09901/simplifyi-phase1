package com.simplifyi.appname.network.request;


import com.google.gson.annotations.SerializedName;

public class SFUserSettingsRequest {

    @SerializedName("calls")
    private boolean calls;
    @SerializedName("messages")
    private boolean messages;
    @SerializedName("newforums")
    private boolean newforums;
    @SerializedName("samegender")
    private boolean samegender;
    @SerializedName("antiSpam")
    private boolean antiSpam;

    public SFUserSettingsRequest() {

    }

    public SFUserSettingsRequest(boolean calls, boolean messages, boolean newforums, boolean samegender, boolean antiSpam) {
        this.calls = calls;
        this.messages = messages;
        this.newforums = newforums;
        this.samegender = samegender;
        this.antiSpam = antiSpam;
    }

    public boolean isCalls() {
        return calls;
    }

    public void setCalls(boolean calls) {
        this.calls = calls;
    }

    public boolean isMessages() {
        return messages;
    }

    public void setMessages(boolean messages) {
        this.messages = messages;
    }

    public boolean isNewforums() {
        return newforums;
    }

    public void setNewforums(boolean newforums) {
        this.newforums = newforums;
    }

    public boolean isSamegender() {
        return samegender;
    }

    public void setSamegender(boolean samegender) {
        this.samegender = samegender;
    }

    public boolean isAntiSpam() {
        return antiSpam;
    }

    public void setAntiSpam(boolean antiSpam) {
        this.antiSpam = antiSpam;
    }

    @Override
    public String toString() {
        return "SFUserSettingsRequest{" +
                "calls=" + calls +
                ", messages=" + messages +
                ", newforums=" + newforums +
                ", samegender=" + samegender +
                ", antiSpam=" + antiSpam +
                '}';
    }
}

