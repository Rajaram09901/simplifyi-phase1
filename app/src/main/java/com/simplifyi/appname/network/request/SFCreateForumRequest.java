package com.simplifyi.appname.network.request;


import com.google.gson.annotations.SerializedName;

public class SFCreateForumRequest {
/* {
            "name": "The name of the forum",
                "tags": ["Classes", "Art, Music & Dance Classes"]
        }*/

    @SerializedName("name")
    public String name;
    @SerializedName("tags")
    public String[] tags;

    public SFCreateForumRequest(String name, String[] tags) {
        this.name = name;
        this.tags = tags;
    }


}

