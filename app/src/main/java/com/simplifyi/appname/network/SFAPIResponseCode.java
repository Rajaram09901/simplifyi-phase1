package com.simplifyi.appname.network;

public class SFAPIResponseCode {
    public static final int SUCCESS = 200;


    public static class Error {
        public static final int LOGIN_FAIL_USER_NOT_EXIST = 404;
        public static final int CREATE_USER_USER_ALREADY_EXIST = 404;
        public static final int FETCH_FORUM_ERROR_TOKEN_EXPIRED = 404;
        public static final int CREATE_FORUM_ERROR_TOKEN_EXPIRED = 404;
        public static final int GET_CURRENT_USER_ERROR_TOKEN_EXPIRED = 404;
        public static final int VERIFY_OTP_ERROR_TIME_EXCEEDED = 404;
        public static final int UN_KNOWN = -1;
    }
}
