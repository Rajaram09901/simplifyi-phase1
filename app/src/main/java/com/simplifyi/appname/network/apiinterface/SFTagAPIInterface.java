package com.simplifyi.appname.network.apiinterface;


import com.simplifyi.appname.model.SFTag;
import com.simplifyi.appname.network.request.SFAddTagsRequestBody;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.utility.SFEndPoint;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface SFTagAPIInterface {

    @GET(SFEndPoint.PATH_TAG)
    Call<SFGenericResponse<SFTag>> doGetTags(@QueryMap Map<String, String> queryParams);

    @POST(SFEndPoint.PATH_ADD_TAG)
    Call<SFGenericResponse> doPostTags(@Body SFAddTagsRequestBody tagsRequestBody);

}

