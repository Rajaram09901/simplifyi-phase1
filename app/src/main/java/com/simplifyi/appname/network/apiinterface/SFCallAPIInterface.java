package com.simplifyi.appname.network.apiinterface;

import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.network.request.SFCallEndOrInitiatedRequest;
import com.simplifyi.appname.network.request.SFDisconnectCallRequest;
import com.simplifyi.appname.network.request.SFRateUserRequestBody;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.utility.SFConstants;
import com.simplifyi.appname.utility.SFEndPoint;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.QueryMap;

public interface SFCallAPIInterface {
    @POST(SFEndPoint.PATH_CALL_DISCONNECT)
    Call<SFGenericResponse> doDisconnectCall(@Body SFDisconnectCallRequest callRequest,@Header(SFConstants.Authorization) String token);

    @POST(SFEndPoint.PATH_CALL_RATING)
    Call<SFGenericResponse> doRateUser(@Body SFRateUserRequestBody rateUserRequest, @Header(SFConstants.Authorization) String token);

    @POST(SFEndPoint.PATH_CALL_INITIATED)
    Call<SFGenericResponse> doCallInitiated(@Body SFCallEndOrInitiatedRequest request, @Header(SFConstants.Authorization) String token);

    @PUT(SFEndPoint.PATH_CALL_ENDS)
    Call<SFGenericResponse> doCallEnds(@Body SFCallEndOrInitiatedRequest request, @Header(SFConstants.Authorization) String token);

    @GET(SFEndPoint.PATH_CALL_RECEIVED_CALLS)
    Call<SFGenericResponse<SFForum>> doGetReceivedCalls(@Header(SFConstants.Authorization) String token);

    @GET(SFEndPoint.PATH_CALL_DIALED_CALLS)
    Call<SFGenericResponse<SFForum>> doGetDialedCalls(@Header(SFConstants.Authorization) String token);

    @GET(SFEndPoint.PATH_CALL_FOLLOWS_CALLS)
    Call<SFGenericResponse<SFForum>> doGetFollowsCalls(@Header(SFConstants.Authorization) String token);
}

