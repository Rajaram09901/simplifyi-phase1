package com.simplifyi.appname.network.restclient;


import com.simplifyi.appname.network.SFRetrofit;
import com.simplifyi.appname.network.apiinterface.SFOTPAPIInterface;
import com.simplifyi.appname.network.request.SFOTPRequestBody;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.utility.SFLogger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SFOTPRestClient extends SFBaseRestClient {
    private SFOTPAPIInterface mApiInterface;

    public SFOTPRestClient() {
        mApiInterface = SFRetrofit.getClient().create(SFOTPAPIInterface.class);
    }

    public void verifyOTP(SFOTPRequestBody registrationBody, NetworkCallListener networkCallListener) {
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("updateUser:Request", registrationBody.toString());
        Call<SFGenericResponse> call = mApiInterface.doPostOTP(registrationBody);

        call.enqueue(new Callback<SFGenericResponse>() {
            @Override
            public void onResponse(Call<SFGenericResponse> call, Response<SFGenericResponse> response) {
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse> call, Throwable t) {
                processError(call, t);
            }
        });
    }

}
