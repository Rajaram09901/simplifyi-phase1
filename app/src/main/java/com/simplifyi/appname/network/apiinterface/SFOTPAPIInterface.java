package com.simplifyi.appname.network.apiinterface;


import com.simplifyi.appname.network.request.SFOTPRequestBody;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.utility.SFEndPoint;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface SFOTPAPIInterface {

    @POST(SFEndPoint.PATH_VERIFY_OTP)
    Call<SFGenericResponse> doPostOTP(@Body SFOTPRequestBody SFOTPRequestBody);
}


