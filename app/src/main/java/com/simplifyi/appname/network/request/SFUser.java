package com.simplifyi.appname.network.request;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SFUser implements Serializable {

    @SerializedName("_id")
    private String id;
    @SerializedName("name")
    private String name;
   /* @SerializedName("userId")
    private String userId;*/
    @SerializedName("nick")
    private String nick;
    @SerializedName("dob")
    private String dob;
    @SerializedName("karma") //rating
    private int karma;
    @SerializedName("verified")
    private boolean mobileVerified;
    @SerializedName("phone")
    private long phone;
    @SerializedName("tags")
    private List<String> tags;
    @SerializedName("imeiNumber")
    private String imeiNumber;
    @SerializedName("languages")
    private List<String> languages;
    @SerializedName("photo")
    private String profileImageUrl;
    @SerializedName("settings")
    private Settings settings;

    public SFUser() {

    }

    public SFUser(String name, String nick, long phone, String imeiNumber) {
        this.name = name;
        this.nick = nick;
        this.phone = phone;
        this.imeiNumber = imeiNumber;
    }

    @Override
    public String toString() {
        return "SFUser{" +
                "name='" + name + '\'' +
                ", nick='" + nick + '\'' +
                ", phone=" + phone +
                ", imeiNumber='" + imeiNumber + '\'' +
                '}';
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNick() {
        return nick;
    }

    public long getPhone() {
        return phone;
    }

    public String getImeiNumber() {
        return imeiNumber;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public int getKarma() {
        return karma;
    }

    public boolean isMobileVerified() {
        return mobileVerified;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public void setImeiNumber(String imeiNumber) {
        this.imeiNumber = imeiNumber;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setKarma(int karma) {
        this.karma = karma;
    }

    public void setMobileVerified(boolean mobileVerified) {
        this.mobileVerified = mobileVerified;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

   /* public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }*/

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
    }

    public static class Settings implements  Serializable{
        @SerializedName("notifications")
        private Notifications notifications;
        @SerializedName("samegender")
        private boolean samegender;
        @SerializedName("antiSpam")
        private boolean antiSpam;

        public Settings(Notifications notifications, boolean samegender, boolean antiSpam) {
            this.notifications = notifications;
            this.samegender = samegender;
            this.antiSpam = antiSpam;
        }

        public Notifications getNotifications() {
            return notifications;
        }

        public void setNotifications(Notifications notifications) {
            this.notifications = notifications;
        }

        public boolean isSamegender() {
            return samegender;
        }

        public void setSamegender(boolean samegender) {
            this.samegender = samegender;
        }

        public boolean isAntiSpam() {
            return antiSpam;
        }

        public void setAntiSpam(boolean antiSpam) {
            this.antiSpam = antiSpam;
        }
    }

    public static class Notifications implements Serializable{
        @SerializedName("calls")
        private boolean calls;
        @SerializedName("messages")
        private boolean messages;
        @SerializedName("newforums")
        private boolean newforums;

        public Notifications(boolean calls, boolean messages, boolean newforums) {
            this.calls = calls;
            this.messages = messages;
            this.newforums = newforums;
        }

        public boolean isCalls() {
            return calls;
        }

        public void setCalls(boolean calls) {
            this.calls = calls;
        }

        public boolean isMessages() {
            return messages;
        }

        public void setMessages(boolean messages) {
            this.messages = messages;
        }

        public boolean isNewforums() {
            return newforums;
        }

        public void setNewforums(boolean newforums) {
            this.newforums = newforums;
        }
    }
}

