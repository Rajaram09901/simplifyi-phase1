package com.simplifyi.appname.network.restclient;


import com.simplifyi.appname.model.SFTag;
import com.simplifyi.appname.network.SFRetrofit;
import com.simplifyi.appname.network.apiinterface.SFTagAPIInterface;
import com.simplifyi.appname.network.request.SFAddTagsRequestBody;
import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUtility;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SFTagRestClient extends SFBaseRestClient {
    private SFTagAPIInterface mApiInterface;

    public SFTagRestClient() {
        mApiInterface = SFRetrofit.getClient().create(SFTagAPIInterface.class);
    }

    public void getTags(NetworkCallListener networkCallListener, int level, String parentTag) {
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("getTags:Request");
        Map queryMap = new HashMap();
        queryMap.put("level", level);
        if (!SFUtility.isStringNullOrEmpty(parentTag)) {
            queryMap.put("parents", parentTag);
        }
        Call<SFGenericResponse<SFTag>> call = mApiInterface.doGetTags(queryMap);

        call.enqueue(new Callback<SFGenericResponse<SFTag>>() {
            @Override
            public void onResponse(Call<SFGenericResponse<SFTag>> call, Response<SFGenericResponse<SFTag>> response) {
                SFLogger.debugLog("getTags()", "ResponseCode:" + response.code());
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse<SFTag>> call, Throwable t) {
                processError(call, t);
            }
        });
    }

    public void addTagsToUser(List<SFTag> tags, SFUser user, NetworkCallListener networkCallListener) {
        mNetworkCallListener = networkCallListener;
        SFLogger.debugLog("addTagsToUser:Request");

        SFAddTagsRequestBody requestBody = new SFAddTagsRequestBody(getTagsAsString(tags), getLanguageListAsString(user.getLanguages()), user.getPhone());
        Call<SFGenericResponse> call = mApiInterface.doPostTags(requestBody);

        call.enqueue(new Callback<SFGenericResponse>() {
            @Override
            public void onResponse(Call<SFGenericResponse> call, Response<SFGenericResponse> response) {
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse> call, Throwable t) {
                processError(call, t);
            }
        });
    }

    private String getTagsAsString(List<SFTag> tags) {
        StringBuffer buffer = new StringBuffer();
        for (SFTag tag : tags) {
            buffer.append(tag.getName());
            buffer.append(",");
        }
        buffer.deleteCharAt(buffer.length() - 1);// remove last comma ","
        return buffer.toString();
    }

    private String getLanguageListAsString(List<String> languages) {
        StringBuffer buffer = new StringBuffer();
        for (String language : languages) {
            buffer.append(language);
            buffer.append(",");
        }
        buffer.deleteCharAt(buffer.length() - 1);// remove last comma ","
        return buffer.toString();
    }

}
