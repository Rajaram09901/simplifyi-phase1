package com.simplifyi.appname.network.restclient;


import com.simplifyi.appname.network.SFRetrofit;
import com.simplifyi.appname.network.apiinterface.SFLoginAPIInterface;
import com.simplifyi.appname.network.request.SFLoginRequestBody;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.simplifyi.appname.network.response.SFLoginToken;
import com.simplifyi.appname.utility.SFLogger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SFLoginRestClient extends SFBaseRestClient {
    private SFLoginAPIInterface mApiInterface;

    public SFLoginRestClient() {
        mApiInterface = SFRetrofit.getClient().create(SFLoginAPIInterface.class);
    }

    public void login(SFLoginRequestBody loginBody, NetworkCallListener networkCallListener) {
        mNetworkCallListener = networkCallListener;
       // SFLogger.debugLog("login:Request", loginBody.toString());
        Call<SFGenericResponse<SFLoginToken>> call = mApiInterface.doPostLogin(loginBody);

        call.enqueue(new Callback<SFGenericResponse<SFLoginToken>>() {
            @Override
            public void onResponse(Call<SFGenericResponse<SFLoginToken>> call, Response<SFGenericResponse<SFLoginToken>> response) {
                processResponse(response);
            }

            @Override
            public void onFailure(Call<SFGenericResponse<SFLoginToken>> call, Throwable t) {
                processError(call, t);
            }
        });
    }

}
