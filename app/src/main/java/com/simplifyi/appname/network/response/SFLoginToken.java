package com.simplifyi.appname.network.response;

public class SFLoginToken {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
