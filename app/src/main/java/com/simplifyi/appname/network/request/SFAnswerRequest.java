package com.simplifyi.appname.network.request;

import com.google.gson.annotations.SerializedName;

public class SFAnswerRequest {
    @SerializedName("id")
    private String forumId;
    @SerializedName("token")
    private String loginToken;
    @SerializedName("answer")
    private String answer;

    public SFAnswerRequest() {
    }

    public SFAnswerRequest(String forumId, String loginToken, String answer) {
        this.forumId = forumId;
        this.loginToken = loginToken;
        this.answer = answer;
    }

    public String getForumId() {
        return forumId;
    }

    public void setForumId(String forumId) {
        this.forumId = forumId;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "SFAnswerRequest{" +
                "forumId='" + forumId + '\'' +
                ", loginToken='" + loginToken + '\'' +
                ", answer='" + answer + '\'' +
                '}';
    }
}
