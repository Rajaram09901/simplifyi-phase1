package com.simplifyi.appname;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.simplifyi.appname.utility.SFLogger;

import io.fabric.sdk.android.Fabric;

/**
 * Created by dinesh.m
 */
public class SFApplication extends Application {
    /**
     * only use the app context in those objects which remain for entire app lifetime (generally static singleton objects can use it)
     */
    public static Context mAppContext;
    public static boolean isUserAGuest;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppContext = getApplicationContext();
        isUserAGuest = false;

    }


    @Override
    public void onTerminate() {
        super.onTerminate();
        SFLogger.debugLog("SFApplication", "onTerminate(), the app getting terminated...!");
    }

    /**
     * The Android System is running slow due to low memory. release some memory from your app size to avoid your app from being killed
     */
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        SFLogger.debugLog("SFApplication", "onLowMemory() is called.so, The Android System Running slow. avoid your app from killing by releasing some memory.!!");
    }

    /**
     * Clear the image's memory cache when the app goes to background. Release other App level,global resources.
     *
     * @param level
     */
    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (level == TRIM_MEMORY_UI_HIDDEN) {
           /* SFLogger.debugLog("SFApplication", "onTrimMemory() is called.so, App Moved to Background..!!");
            SFCacheManager memCache = SFMemoryCacheManager.getInstance();
            memCache.clearAll();*/
        } else if (level == TRIM_MEMORY_COMPLETE) {//The system is running low on memory and your processResponse is one of the first to be killed if the system does not recover memory now

        }
    }

    @Override
    protected void finalize() throws Throwable {
        SFLogger.debugLog("SFApplication", "finalize() is called.so, this object Can be GCed..!!");
    }
}
