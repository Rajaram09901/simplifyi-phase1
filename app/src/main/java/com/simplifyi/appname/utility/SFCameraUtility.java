package com.simplifyi.appname.utility;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.webkit.MimeTypeMap;

import com.simplifyi.appname.utility.SFLogger;
import com.simplifyi.appname.utility.SFUIUtility;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class SFCameraUtility {
    private String mProfilePicPath;

    public void initialize(Activity activity) {
        EasyImage.configuration(activity)
                .setImagesFolderName("EasyImage sample")
                .setCopyTakenPhotosToPublicGalleryAppFolder(true)
                .setCopyPickedImagesToPublicGalleryAppFolder(true)
                .setAllowMultiplePickInGallery(true);
    }

    public void openGallery(Activity activity) {
        EasyImage.openGallery(activity, 0);
    }

    public void openCamera(AppCompatActivity activity) {
        EasyImage.openCameraForImage(activity, 0);
    }

    public void openDocuments(AppCompatActivity activity) {
        EasyImage.openDocuments(activity, 0);
    }

    public void openDocumentsWithChooser(AppCompatActivity activity) {
        EasyImage.openChooserWithDocuments(activity, "Choose Docs", 0);
    }

    public void openGalleryWithChooser(AppCompatActivity activity) {
        EasyImage.openChooserWithGallery(activity, "Choose Gallery", 0);
    }

    public void handleActivityResult(int requestCode, int resultCode, Intent data, Activity activity, ProfilePicCallback callback) {
        EasyImage.handleActivityResult(requestCode, resultCode, data, activity, callback);
    }

    public Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) throws IOException {

        ExifInterface ei = new ExifInterface(selectedImage.getPath());
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    public String getPathFromUri(Activity activity, Uri uri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = activity.getContentResolver().query(uri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public Bitmap getBitmapFromFileUri(Activity activity, Uri fileUri) {
        String filePath = fileUri.getPath();
        SFLogger.debugLog("PICK_PHOTO", filePath);
        ParcelFileDescriptor parcelFD = null;
        try {
            int scale = SFUIUtility.getImageScalingFactor(activity, fileUri, 1024);
            parcelFD = activity.getContentResolver().openFileDescriptor(fileUri, "r");
            FileDescriptor fileDescriptor = parcelFD.getFileDescriptor();
            BitmapFactory.Options options1 = new BitmapFactory.Options();
            /*Scale down the image by 'scale' .
            i.e original image size= height/scale,width/scale, pixels: vertical/square(scale), horizontal/square(scale). */
            options1.inSampleSize = scale;
            //Create the bitmap from file with scaling down if required
            Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options1);

            Bitmap finalBitmap = SFUIUtility.compressIntoPNG(bitmap);
            finalBitmap = SFUIUtility.getResizedBitmap(finalBitmap, 1);//don't scale?? does it make sense to call with value 1?
            return finalBitmap;
        } catch (FileNotFoundException e) {

        } finally {
            if (parcelFD != null)
                try {
                    parcelFD.close();
                } catch (IOException e) {

                }
        }
        return null;
    }

    /**
     * creates a file in cache directory and returns a URI for it.
     */
    //throws an error described here: https://stackoverflow.com/questions/38200282/android-os-fileuriexposedexception-file-storage-emulated-0-test-txt-exposed
    public Uri createCacheFileAndReturnUri(Activity activity, String fileName) {
        Uri outputFileUri = null;
        File getImage = activity.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), fileName));
        }
        return outputFileUri;
    }


    public Bitmap getBitmapFromUri(Activity activity, Uri uri) {
        if (activity == null || uri == null) return null;
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);
            bitmap = rotateImageIfRequired(bitmap, uri);
            bitmap = getResizedBitmap(bitmap, 500);

               /* CircleImageView croppedImageView = (CircleImageView) findViewById(R.id.img_profile);
                croppedImageView.setImageBitmap(mProfilPicBitmap);*/
            //mProfilePicIv.setImageBitmap(mProfilPicBitmap);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    public List<Intent> getAllIntentsForAction(Activity activity, Intent rootIntent) {
        List<Intent> cameraIntents = new ArrayList<>();
        PackageManager packageManager = activity.getPackageManager();
        List<ResolveInfo> activitiesThatSupportImageCapture = packageManager.queryIntentActivities(rootIntent, 0);
        for (ResolveInfo resolveInfo : activitiesThatSupportImageCapture) {
            String packageName = resolveInfo.activityInfo.packageName;
            String activityName = resolveInfo.activityInfo.name;
            ComponentName componentName = new ComponentName(packageName, activityName);

            Intent captureIntent = new Intent(rootIntent);
            captureIntent.setComponent(componentName);
            captureIntent.setPackage(packageName);
            cameraIntents.add(captureIntent);
        }
        return cameraIntents;
    }

    public Intent getImageFolderPickerIntent(Activity activity) {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<Intent> galleryIntents = getAllIntentsForAction(activity, galleryIntent);
        String documentActivityClassName = "com.android.documentsui.DocumentsActivity";
        // the main intent is the last in the list so pickup the useless one
        Intent mainIntent = galleryIntents.get(galleryIntents.size() - 1);
        for (Intent intent : galleryIntents) {
            if (intent.getComponent().getClassName().equals(documentActivityClassName)) {
                mainIntent = intent;
                break;
            }
        }
        galleryIntents.remove(mainIntent);

        Intent chooserIntent = Intent.createChooser(mainIntent, "Select Image source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, galleryIntents.toArray(new Parcelable[galleryIntents.size()]));
        return chooserIntent;
    }

    public Intent getCameraPickerIntent(Activity activity) {
        Uri fileInCacheDirectory = createCacheFileAndReturnUri(activity, "profile.png");
        List<Intent> cameraIntents = getAllIntentsForAction(activity, new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE));
        for (Intent captureIntent : cameraIntents) {
            captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileInCacheDirectory);
        }
        Intent cameraPickerIntent = Intent.createChooser(new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI), "Select Cameras");
        cameraPickerIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));
        return cameraPickerIntent;
    }

    public Uri getUriForBitmap(Bitmap bmp, String filename, Activity activity) {
        File tempImageFile = null;
        try {
            File appExternalDirectory = activity.getExternalCacheDir();
            tempImageFile = new File(appExternalDirectory.getPath(), filename);
            setProfilePicPath(tempImageFile.getPath());
            FileOutputStream out = new FileOutputStream(tempImageFile);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Uri.fromFile(tempImageFile);

    }

    public String getProfilePicPath() {
        return mProfilePicPath;
    }

    public void setProfilePicPath(String mProfilePicPath) {
        this.mProfilePicPath = mProfilePicPath;
    }

    public abstract static class ProfilePicCallback extends DefaultCallback {
    }

    public String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }
}
