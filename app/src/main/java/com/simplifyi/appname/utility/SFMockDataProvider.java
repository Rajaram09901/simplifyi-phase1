package com.simplifyi.appname.utility;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.gson.reflect.TypeToken;
import com.simplifyi.appname.network.request.SFUser;
import com.simplifyi.appname.network.response.SFGenericResponse;
import com.sinch.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class SFMockDataProvider {
    private static SFMockDataProvider sInstance;
    private static Context mAppContext;

    private SFMockDataProvider() {
    }

    public static SFMockDataProvider getInstance(Context appContext) {
        if (appContext != null) {
            mAppContext = appContext;
        }
        if (sInstance == null) {
            sInstance = new SFMockDataProvider();
        }
        return sInstance;
    }

    public SFGenericResponse<List<SFUser>> getAllUsers() {
        String path = AssetFilePath.GET_ALL_USERS;
        String json = getJson(path);
        Gson gson = new Gson();
        SFGenericResponse<List<SFUser>> response = gson.fromJson(json, new TypeToken<SFGenericResponse<SFUser>>() {
        }.getType());
        List<SFUser> users = response.getBody().getData();
        return response;
    }

    public String getJsonFor(String path) {
        String jsonFilePath = "";
        switch (path) {
            case SFEndPoint.PATH_TAG:
                jsonFilePath = AssetFilePath.GET_ALL_TAGS_LEVEL_1;
                break;
            case SFEndPoint.PATH_ADD_TAG:
                jsonFilePath = AssetFilePath.SET_TAG_TO_USER;
                break;
            case SFEndPoint.PATH_CALL_DISCONNECT:
                jsonFilePath = AssetFilePath.CALL_DISCONNECT;
                break;
            case SFEndPoint.PATH_CALL_RATING:
                //TODO: yet to create json file
                jsonFilePath = "";
            case SFEndPoint.PATH_GET_ALL_CALLABLE_FORUMS:
                jsonFilePath = AssetFilePath.GET_CALLABLE_FORUMS;
                break;
            case SFEndPoint.PATH_GET_ALL_FORUMS:
                jsonFilePath = AssetFilePath.GET_ALL_FORUMS;
                break;
                case SFEndPoint.PATH_SEARCH__FORUMS:
                    jsonFilePath = AssetFilePath.SEARCH_FORUMS;
                break;
            case SFEndPoint.PATH_CREATE_FORUM:
                jsonFilePath = AssetFilePath.CREATE_FORUM;
                break;
            case SFEndPoint.PATH_CREATE_USER:
                jsonFilePath = AssetFilePath.CREATE_USER;
                break;
            case SFEndPoint.PATH_UPDATE_USER:
                jsonFilePath = AssetFilePath.UPDATE_USER;
                break;
            case SFEndPoint.PATH_GET_ALL_USER:
                jsonFilePath = AssetFilePath.GET_ALL_USERS;
                break;
            case SFEndPoint.PATH_GET_USER_BY_TOKEN:
                jsonFilePath = AssetFilePath.GET_USER_ONE_CURRENT;
                break;
            case SFEndPoint.PATH_CREATE_FORUM_ANSWER:
                jsonFilePath = AssetFilePath.CREATE_ANSWER;
                break;
            case SFEndPoint.PATH_VERIFY_OTP:
                jsonFilePath = AssetFilePath.VERIFY_OTP_SUCCESSFUL;
                break;
            case SFEndPoint.PATH_LOGIN:
                jsonFilePath = AssetFilePath.DO_LOGIN;
                break;
        }
        String json = getJson(jsonFilePath);
        return json;
    }

    private String getJson(String path) {
        AssetManager assetManager = mAppContext.getAssets();
        StringBuffer buffer = new StringBuffer();
        BufferedReader bufferedReader = null;
        try {
            InputStream inputStream = assetManager.open(path);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            bufferedReader = new BufferedReader(inputStreamReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                buffer.append(line);
            }
        } catch (IOException exception) {

        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {

            }
        }

        return buffer.toString();
    }

    static class AssetFilePath {
        public static final String ROOT_FOLDER_PATH = "mock_response";

        public static final String GET_ALL_USERS = ROOT_FOLDER_PATH + "/" + "user/users.json";
        public static final String CREATE_USER = ROOT_FOLDER_PATH + "/" + "user/create_user_success.json";
        public static final String GET_USER_ONE_CURRENT = ROOT_FOLDER_PATH + "/" + "user/get_user_one_current.json";
        public static final String UPDATE_USER = ROOT_FOLDER_PATH + "/" + "user/update_user.json";

        public static final String VERIFY_OTP_SUCCESSFUL = ROOT_FOLDER_PATH + "/" + "verify_otp/verify_otp_successful.json";
        public static final String VERIFY_OTP_ERROR_TIME_EXCEEDED = "mock_response/verify_otp/error_time_exceeded.json";
        public static final String VERIFY_OTP_ERROR_USER_NOT_EXIST = "mock_response/verify_otp/error_user_doesnot_exist.json";

        public static final String GET_ALL_TAGS_EMPTY = "mock_response/tags/get_all_tags_empty.json";
        public static final String GET_ALL_TAGS_LEVEL_1 = "mock_response/tags/get_all_tags_level1.json";
        public static final String GET_ALL_TAGS_LEVEL_2 = "mock_response/tags/get_all_tags_level2.json";
        public static final String GET_ALL_TAGS_LEVEL_3 = "mock_response/tags/get_all_tags_level3.json";
        public static final String SET_TAG_TO_USER = "mock_response/tags/set_tags_to_user.json";

        public static final String DO_LOGIN = "mock_response/login/login.json";

        public static final String GET_ALL_FORUMS = "mock_response/forum/get_all_forums.json";
        public static final String SEARCH_FORUMS = "mock_response/forum/forum_search.json";
        public static final String CREATE_FORUM = "mock_response/forum/create_forum.json";

        public static final String CREATE_ANSWER = "mock_response/answer/create_answer.json";

        public static final String CALL_DISCONNECT = "mock_response/call/disconnect_call.json";
        public static final String CALL_GET_NEW = "mock_response/call/get_new_calls.json";
        public static final String GET_CALLABLE_FORUMS = "mock_response/call/get_callable_forums.json";
    }
}
