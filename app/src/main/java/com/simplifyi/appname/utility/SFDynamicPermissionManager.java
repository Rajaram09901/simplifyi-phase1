package com.simplifyi.appname.utility;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

public class SFDynamicPermissionManager {

    //Manifest.permission.WRITE_CALENDAR
    public static boolean wasThePermissionGranted(Context activityContext, String permission) {
        int granted = ContextCompat.checkSelfPermission(activityContext, permission);
        if (granted != PackageManager.PERMISSION_GRANTED) {
            SFLogger.debugLog("SFDynamicPermissionManager.wasThePermissionGranted()", "permission " + permission + " was not granted");
            return false;
        }
        SFLogger.debugLog("SFDynamicPermissionManager.wasThePermissionGranted()", "permission " + permission + " was granted");
        return true;
    }

    /**
     * Permission is not granted. so Should we show an explanation?
     *
     * @param permission - ex:-Manifest.permission.READ_CONTACTS
     **/
    public static boolean canWeShowRational(Activity activity, String permission) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                permission)) {
            SFLogger.debugLog("SFDynamicPermissionManager.canWeShowRational()", "for permission " + permission + ",we can show Rational");

            return true;
        }
        SFLogger.debugLog("SFDynamicPermissionManager.canWeShowRational()", " for permission " + permission + ",we can't show Rational");
        return false;
    }

    /**
     * @param activity
     * @param permissions - ex: new String[]{Manifest.permission.READ_CONTACTS}
     */
    public static void requestPermission(Activity activity, String[] permissions, int requestCode) {
        ActivityCompat.requestPermissions(activity, permissions, requestCode);
    }

    public static boolean isAndroidMOrAbove() {
        return (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M);
    }
}
