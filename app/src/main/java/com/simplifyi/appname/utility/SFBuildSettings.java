package com.simplifyi.appname.utility;

/**
 */
public class SFBuildSettings {
    public static final BuildType BUILD_TYPE = BuildType.DEBUG; //make sure to clear the cache in app settings when u make this changes
    public static final String WEB_SERVICE_BASE_URL = "http://18.191.87.99:10010";
    public static final String APPLICATION_ID_KEY = "application-id";
    public static final String APPLICATION_ID_VALUE = "047143FD-624A-CD93-FFE9-D9CC832E4D00";
    public static final String SECRET_KEY_KEY = "secret-key";
    public static final String SECRET_KEY_VALUE = "C0FEDCBC-52BF-1357-FFB4-F131E6B39000";
    public static final String REST_SECRET_KEY = "REST-Secret-Key";
    public static final String REST_SECRET_KEY_VALUE = "F9D546B6-5766-2191-FF4B-182558521500";
    public static final String APPLICATION_VERSION = "v1";
    public static final String GCM_PUSH_SENDER_ID = "";

    public enum BuildType {DEBUG, DEBUG_DIAGNOSTIC, PROD, DEBUG_WITH_MOCK_BACKEND}
}
