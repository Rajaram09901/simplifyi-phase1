package com.simplifyi.appname.utility;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.simplifyi.appname.model.SFForum;
import com.simplifyi.appname.network.request.SFUser;

import java.net.InetAddress;
import java.util.List;
import java.util.Locale;

/**
 * Created by developer.dinesh042
 */
public class SFUtility {
    /**
     * this might not work. cross check & do google before using this.
     *
     * @param context
     * @return
     */
    public static String getPhoneNumber(Context context) {
        String phoneNumber = "none";
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            phoneNumber = telephonyManager.getLine1Number();
        }
        if (!isStringNullOrEmpty(phoneNumber)) {
            SFLogger.debugLog("SFUtility", "getPhoneNumber(), the phone number =" + phoneNumber);
        } else {
            SFLogger.debugLog("SFUtility", "getPhoneNumber(), the phone number is empty or null");
        }
        return "";//telephonyManager.getLine1Number();
    }

    public static final boolean isStringNullOrEmpty(String str) {
        return str == null || str.isEmpty();
    }

    public static String getDeviceIMEI(Activity activity) {
        String imei = "none";
        if (isEmulator()) {
            return "EmulatorIMEI";
        }
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
            imei = telephonyManager.getDeviceId();
        } else {
            SFLogger.showToast(activity, "READE PHONE STATE permission required");
        }
        SFLogger.debugLog("SFUtility.getDeviceIMEI", "IMEI=" + imei);
        return imei;
    }

    public static boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }

    /**
     * To be double sure, Try to resolve a dns name (ex:google.com)
     * to know that the device is not just connected to wifi, but also wifi connected  to internet.
     *
     * @param context
     * @return true if the internet available
     */

    public static boolean isInternetAvailable(Context context) {
        return connectedToNetwork(context);
       /* if (!connectedToNetwork(context)) {
            return false;
        }*/
        //TODO:currently it throws network on main thread exception. lets see later
       /* try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }*/
    }

    /**
     * this method return true if this device connected to any network.
     * this returns true even the device just connected to wifi but there is no internet service
     *
     * @param context
     * @return
     */
    private static boolean connectedToNetwork(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static boolean isForumListNullOREmpty(List<SFForum> forumList) {
        return forumList == null || forumList.isEmpty();
    }

    public static SFUser mapToSFUser(SFForum.ForumUser forumUser) {
        SFUser user = new SFUser();
        /* user.setId(forumUser.getId());*/
        user.setId(forumUser.getUserId());
        user.setName(forumUser.getNickName());
        user.setNick(forumUser.getNickName());
        user.setProfileImageUrl(forumUser.getProfileImageUrl());
        return user;

    }

    public static String getCompleteAddressString(Context context, double latitude, double longitude) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strAdd;
    }
}
