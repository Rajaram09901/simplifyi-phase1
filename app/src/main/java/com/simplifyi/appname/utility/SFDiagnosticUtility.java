package com.simplifyi.appname.utility;

/**
 * Created by developer.dinesh042
 */
public class SFDiagnosticUtility {
    public static String populateMemSize(long bytes) {
        MemSize memSize = new MemSize();
        memSize.bytes = bytes % 1024;
        memSize.KBs = bytes / 1024;
        memSize.MBs = memSize.KBs / 1024;
        memSize.KBs = memSize.KBs % 1024;
        return memSize.toString();
    }

    public static String populateTimeFromMillis(long millis) {
        long secs = millis / 1000;
        millis = millis % 1000;
        long minits = secs / 1000;
        secs = secs % 1000;
        return minits + "minits," + secs + "secs," + millis + "millis";
    }

    static class MemSize {
        long bytes;
        long KBs;
        long MBs;

        @Override
        public String toString() {
            return MBs + "MBs," + KBs + "KBs," + bytes + "bytes";
        }
    }
}
