package com.simplifyi.appname.utility;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by developer.dinesh042
 */
public class SFLogger {
    public static final String LOG_TAG = "SimplyFyi";

    public static void debugLog(String logText) {
        if (SFBuildSettings.BUILD_TYPE != SFBuildSettings.BuildType.PROD)
            Log.d(LOG_TAG, logText);
    }

    public static void debugLog(String module, String logText) {
        if (SFBuildSettings.BUILD_TYPE != SFBuildSettings.BuildType.PROD)
            Log.d(LOG_TAG, module + ":" + logText);
    }

    public static void errorLog(String logText) {
        if (SFBuildSettings.BUILD_TYPE != SFBuildSettings.BuildType.PROD)
            Log.e(LOG_TAG, logText);
    }

    public static void errorLog(String logText, Throwable exception) {
        if (SFBuildSettings.BUILD_TYPE != SFBuildSettings.BuildType.PROD)
            Log.e(LOG_TAG, logText, exception);
    }

    public static void errorLog(String module, String logText) {
        if (SFBuildSettings.BUILD_TYPE != SFBuildSettings.BuildType.PROD)
            Log.e(LOG_TAG, module + ":" + logText);
    }

    public static void errorLog(String module, String logText, Throwable exception) {
        if (SFBuildSettings.BUILD_TYPE == SFBuildSettings.BuildType.DEBUG_DIAGNOSTIC)
            Log.e(LOG_TAG, module + ":" + logText + ":\n" + exception.toString());
    }

    public static void debugDiagnosticLog(String module, String logText) {
        if (SFBuildSettings.BUILD_TYPE == SFBuildSettings.BuildType.DEBUG_DIAGNOSTIC)
            Log.d(LOG_TAG, module + ":" + logText);
    }

    public static void showToast(Context activity, String message) {
        Toast.makeText(activity, message,
                Toast.LENGTH_SHORT).show();
    }

    public static void printErrorStack(Throwable t) {
        if (t != null) {
            t.printStackTrace();
        }
    }
}
