package com.simplifyi.appname.utility;

import android.os.Handler;

/**
 * Created by developer.dinesh042
 */
public class SFTimer {
    private SFTimerTask mSFTimerTask;
    private OnTimerExpiredCallback mOnTimerExpiredCallback;
    private int mIntervalInSecs;
    public static final int STATE_RUNNING = 1;
    public static final int STATE_CANCELED = 3;
    private int mCurrentState;

    public SFTimer() {
        mSFTimerTask = new SFTimerTask();
    }

    public int getCurrentState() {
        return mCurrentState;
    }

    public boolean start(int intervalInSecs, OnTimerExpiredCallback onPerformSyncCallback) {
        if (intervalInSecs <= 0 || onPerformSyncCallback == null) {
            SFLogger.debugLog("SFTimer", "Invalid Timer interval OR Callback.Timer cannot run....");
            return false;
        }
        mIntervalInSecs = intervalInSecs;
        mOnTimerExpiredCallback = onPerformSyncCallback;
        if (scheduleTimer()) {
            mCurrentState = STATE_RUNNING;
            return true;
        }
        return false;
    }

    private boolean scheduleTimer() {
        if (mSFTimerTask != null && mIntervalInSecs > 0 && mOnTimerExpiredCallback != null) {
            new Handler().postDelayed(mSFTimerTask, mIntervalInSecs * 1000);
            return true;
        }
        return false;
    }

    public void cancel() {
        if (mSFTimerTask != null) {
            new Handler().removeCallbacks(mSFTimerTask);
        }
        mIntervalInSecs = 0;
        mOnTimerExpiredCallback = null;
        mSFTimerTask = null;
        mCurrentState = STATE_CANCELED;
    }

    private class SFTimerTask implements Runnable {
        @Override
        public void run() {
            if (mOnTimerExpiredCallback != null) {
                SFLogger.debugLog("SFTimer", "Triggering Sync...");
                mOnTimerExpiredCallback.onTimerExpired();
                scheduleTimer();
            }
        }
    }

    public interface OnTimerExpiredCallback {
        void onTimerExpired();
    }
}
