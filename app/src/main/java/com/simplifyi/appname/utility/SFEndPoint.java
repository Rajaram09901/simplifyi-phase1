package com.simplifyi.appname.utility;

import com.simplifyi.simplifyi_phase2.BuildConfig;

/**
 * specify all the end points for external servers here.
 */
public class SFEndPoint {
    public static final String BASE_URL = BuildConfig.BASE_URL;

    public static final String PATH_TAG = "/tags";
    public static final String PATH_ADD_TAG = "/user/addTags";

    public static final String PATH_CALL_DISCONNECT = "/calls/disconnect";
    public static final String PATH_CALL_RATING = "/rating";
    public static final String PATH_CALL_INITIATED = "/calls/initiated";
    public static final String PATH_CALL_ENDS = "/calls/ends";
    public static final String PATH_CALL_DIALED_CALLS = "/users/dialedcalls";
    public static final String PATH_CALL_RECEIVED_CALLS = "/users/receivedcalls";
    public static final String PATH_CALL_FOLLOWS_CALLS = "/users/follows";
    public static final String PATH_CALL_REPLYS_CALLS = "/users/follows";

    public static final String PATH_FORUM = "/forum";
    public static final String PATH_GET_ALL_FORUMS = "get_all_forums";
    public static final String PATH_SEARCH__FORUMS = "search_forums";
    public static final String PATH_CREATE_FORUM = "create_forum";
    public static final String PATH_CREATE_FORUM_ANSWER = "/forumNewAnswer";
    public static final String PATH_GET_ALL_CALLABLE_FORUMS = "/forum/calls";
    public static final String PATH_FORUM_DELETE = "/forum/notinterested";

    public static final String PATH_LOGIN = "/login";

    public static final String PATH_VERIFY_OTP = "/users/verifyotp";

    public static final String PATH_USERS = "/users";
    public static final String PATH_CREATE_USER = "create_user";
    public static final String PATH_UPDATE_USER = "update_user";
    public static final String PATH_DELETE_USER = "delete_user";
    public static final String PATH_GET_ALL_USER = "get_all_user";
    public static final String PATH_GET_USERS_WITH_PAGING = "get_all_user_with_paging";
    public static final String PATH_GET_USER_BY_TOKEN = "/user/getOne";
    public static final String PATH_GET_USER_BY_ID = "/user/getuserbyid";
    public static final String PATH_GET_USER_SETTINGS = "/users/settings";


}
