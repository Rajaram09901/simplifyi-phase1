package com.simplifyi.appname.utility;

import android.os.CountDownTimer;

public class SFCallTimer {
    private static final int MAX_ALLOWED_CALL_DURATION_IN_SECS = 5 * 60 * 60;// 5 hrs
    private int mHours;
    private int mMinutes;
    private int mSeconds;
    private int mElapsedSecondsCounter;
    private OnTickerListener mTickerListener;
    private CountDownTimer mCountDownTimer;
    private boolean mIsStarted;

    public SFCallTimer() {
        reset();
    }


    public void start() {
        mIsStarted = true;
        mCountDownTimer.start();
    }

    public boolean isIsStarted() {
        return mIsStarted;
    }

    public void stop() {
        mIsStarted = false;
        mCountDownTimer.cancel();
    }

    public void reset() {
        mIsStarted = false;
        mElapsedSecondsCounter = 0;
        mHours = 0;
        mMinutes = 0;
        mSeconds = 0;
        mCountDownTimer = new CountDownTimer(MAX_ALLOWED_CALL_DURATION_IN_SECS * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mElapsedSecondsCounter++;
                mSeconds = mElapsedSecondsCounter;
                mMinutes = mSeconds / 60;
                mHours = mMinutes / 60;

                mSeconds = mSeconds % 60;
                mMinutes = mMinutes % 60;
                if (mTickerListener != null) {
                    mTickerListener.onTick(mHours, mMinutes, mSeconds);
                }
            }

            @Override
            public void onFinish() {

            }
        };
    }

    public void setTickerListener(OnTickerListener listener) {
        mTickerListener = listener;
    }

    public int getHours() {
        return mHours;
    }

    public int getMinutes() {
        return mMinutes;
    }

    public int getSeconds() {
        return mSeconds;
    }

    public interface OnTickerListener {
        void onTick(int hours, int minutes, int seconds);
    }

    @Override
    public String toString() {
        return "SFCallTimer{" +
                "Hours=" + mHours +
                ", Minutes=" + mMinutes +
                ", Seconds=" + mSeconds +
                '}';
    }
}
