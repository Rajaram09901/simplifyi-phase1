package com.simplifyi.appname.utility;

/**
 * Created by developer.dinesh042
 **/
public class SFStopWatch {
    private String mModuleName;
    private String mTaskName;
    private long mStartTimeInMillis;

    public SFStopWatch(String moduleName) {
        mModuleName = moduleName;
    }

    public void start(String taskName) {
        mTaskName = taskName;
        mStartTimeInMillis = System.currentTimeMillis();
    }

    public void stop() {
        if (SFBuildSettings.BUILD_TYPE == SFBuildSettings.BuildType.PROD) {
            return;
        }
        long elapsed = System.currentTimeMillis() - mStartTimeInMillis;

        SFLogger.debugDiagnosticLog("SFStopWatch", mModuleName + ":" + mTaskName + " took " + SFDiagnosticUtility.populateTimeFromMillis(elapsed));
    }
}
