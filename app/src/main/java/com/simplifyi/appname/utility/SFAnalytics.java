package com.simplifyi.appname.utility;

import android.app.Activity;
import android.content.Context;

import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * to see live data on firebase, run the comomand "adb shell setprop debug.firebase.analytics.app com.simplifyi.simplifyi_phase2.dev"
 * to stop live data, run "adb shell setprop debug.firebase.analytics.app .none"
 */
public class SFAnalytics {
    private FirebaseAnalytics mFirebaseAnalytics;
    private static SFAnalytics sAnalytics;

    private SFAnalytics(Context context) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public static SFAnalytics getInstance(Context context) {
        if (sAnalytics == null) {
            sAnalytics = new SFAnalytics(context);
        }
        return sAnalytics;
    }

    public void setCurrentScreen(Activity activity, String screenName) {
        mFirebaseAnalytics.setCurrentScreen(activity, AnalyticsConstants.SCREEN_NAME, screenName);
    }

    /**
     * user can be any attribute of the user that uniquely identifies the user
     * userId in the user object or nickname can be used. can we use mobile number as well?
     *
     * @param userId
     */
    public void setUserId(String userId) {
        mFirebaseAnalytics.setUserId(userId);
    }

    static class AnalyticsConstants {
        public static final String SCREEN_NAME = "Screen Name";
    }
}
