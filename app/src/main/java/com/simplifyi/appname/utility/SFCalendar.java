package com.simplifyi.appname.utility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by dinesh
 */
public class SFCalendar {
    private static SimpleDateFormat sServerDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
    public static String getDateAsString(long milliSeconds) {


        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return sServerDateFormat.format(calendar.getTime());
    }

    public static String getDateAsDayMonthDate(long millis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(millis);
        int dayOfWeekAsInt = calendar.get(Calendar.DAY_OF_WEEK);
        String dayOfWeekAsString = "";
        switch (dayOfWeekAsInt) {
            case Calendar.SUNDAY:
                dayOfWeekAsString = "Sun";
                break;
            case Calendar.MONDAY:
                dayOfWeekAsString = "Mon";
                break;
            case Calendar.TUESDAY:
                dayOfWeekAsString = "Tue";
                break;
            case Calendar.WEDNESDAY:
                dayOfWeekAsString = "Wed";
                break;
            case Calendar.THURSDAY:
                dayOfWeekAsString = "Thu";
                break;
            case Calendar.FRIDAY:
                dayOfWeekAsString = "Fri";
                break;
            case Calendar.SATURDAY:
                dayOfWeekAsString = "Sat";
                break;
        }
        int month = calendar.get(Calendar.MONTH);
        String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        String monthOfYear = months[month];
        int date = calendar.get(Calendar.DAY_OF_MONTH);

        String finalDate = dayOfWeekAsString + ", " + monthOfYear + " " + date;
        return finalDate;
    }

    public static String getDateAsString(long milliSeconds, String format) {

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return sServerDateFormat.format(calendar.getTime());
    }

    public static String getTimeAsString(long milliSeconds) {
        //String dateFormat = "dd/MM/yyyy hh:mm:ss.SSS";

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return sServerDateFormat.format(calendar.getTime());
    }

    public static String getElapsedTimeIntervalAsString(long timeInMillis) {
        long elapsed = System.currentTimeMillis() - timeInMillis;
        long secs = elapsed / 1000;
        int minutes = (int) secs / 60;
        secs = secs % 60;
        int hours = minutes / 60;
        minutes = minutes % 60;
        int days = hours / 24;
        hours = hours % 24;
        String elapsedDuration = "";
        if (days > 0) {
            elapsedDuration = days + " days";
        } else if (hours > 0) {
            elapsedDuration = hours + " hrs";
        } else if (minutes > 0) {
            elapsedDuration = minutes + " mins";
        } else if (secs > 0) {
            elapsedDuration = secs + " secs";
        }

        return elapsedDuration;
    }

    public static long getTimeInLocalTimeZone(long timeInOtherTimezoneInMillis) {
        if (timeInOtherTimezoneInMillis == -1) {
            return timeInOtherTimezoneInMillis;
        }
        Calendar tempCalendar = Calendar.getInstance();
        tempCalendar.setTimeInMillis(timeInOtherTimezoneInMillis);

        tempCalendar.setTimeZone(TimeZone.getDefault());
        Date localTime = tempCalendar.getTime();

        tempCalendar.setTimeZone(TimeZone.getTimeZone("CST"));
        //Date serverTime = tempCalendar.getTime();
        return localTime.getTime();
    }

    public static String elapsedTime(String dateAsString) {

        //"2019-01-31T18:16:57.983Z
        sServerDateFormat.setTimeZone(TimeZone.getDefault());
        Date dateObj = null;
        try {
            dateObj = sServerDateFormat.parse(dateAsString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long timeAsMillis = dateObj.getTime();
        String elapsedAsString = getElapsedTimeIntervalAsString(timeAsMillis);
        return elapsedAsString;
    }
}
