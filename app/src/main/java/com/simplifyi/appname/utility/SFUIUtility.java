package com.simplifyi.appname.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class SFUIUtility {

    public static void showListPop(Activity activity, String title, TextView textView, ArrayList<String> options) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(title);
        String[] optionsArray = options.toArray(new String[options.size()]);
        builder.setItems(optionsArray, (dialog, which) -> textView.setText(optionsArray[which]));
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public static Point getDisplaySize(WindowManager windowManager) {
        try {
            if (Build.VERSION.SDK_INT > 16) {
                Display display = windowManager.getDefaultDisplay();
                DisplayMetrics displayMetrics = new DisplayMetrics();
                display.getMetrics(displayMetrics);
                return new Point(displayMetrics.widthPixels, displayMetrics.heightPixels);
            } else {
                return new Point(0, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new Point(0, 0);
        }
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context){
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    /**
     * This method converts device specific pixels to density independent pixels.
     *
     * @param px A value in px (pixels) unit. Which we need to convert into db
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent dp equivalent to px value
     */
    public static float convertPixelsToDp(float px, Context context){
        return px / ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static Bitmap compressIntoPNG(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();//utility class that helps in writing byte streams to destination easily.

        //Convert or compress the image into PNG & place resulting bytes into output stream
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
        return BitmapFactory.decodeStream(new ByteArrayInputStream(outputStream.toByteArray()));
    }

    public static int getImageScalingFactor(Context context,Uri fileUri, int requiredSize) throws FileNotFoundException {
        int scale = 1;
        ParcelFileDescriptor parcelFD = null;

        /* Get the file descriptor for the image*/
        parcelFD = context.getContentResolver().openFileDescriptor(fileUri, "r");
        FileDescriptor fileDescriptor = parcelFD.getFileDescriptor();

        /* get the bounds of the image, calculate scale factor & scale it down to REQUIRED_SIZE =1024 */
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
        int tempWidth = options.outWidth;
        int tempHeight = options.outHeight;
        //Calculate scale factor to scale down the image
        while (true) {
            if (tempWidth < requiredSize && tempHeight < requiredSize) {
                break;
            }
            tempWidth /= 2;
            tempHeight /= 2;
            scale *= 2;
        }
        return scale;
    }

    public static Bitmap getResizedBitmap(Bitmap bitmap, int scale) {
        return Bitmap.createScaledBitmap(bitmap, bitmap.getWidth() * scale, bitmap.getHeight() * scale, true);
    }
}
