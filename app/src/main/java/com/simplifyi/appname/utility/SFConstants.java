package com.simplifyi.appname.utility;

/**
 * Created by developer.dinesh042.
 */
public class SFConstants {
    public static final int MAIN_CONFIG_PAGE_SIZE = 50;
    public static final String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
    public static final String SORT_BY_ASCENDING_ORDER = "Order%20asc";
    public static final String Authorization = "Authorization";

}
