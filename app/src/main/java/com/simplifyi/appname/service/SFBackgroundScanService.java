package com.simplifyi.appname.service;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.simplifyi.appname.utility.SFLogger;

/**
 * Created by developer.dinesh042.
 */
public class SFBackgroundScanService extends Service {

    private NotificationManager manager;

    @Override
    public void onCreate() {
        super.onCreate();

        SFLogger.debugLog("SFBackgroundScanService", " OnCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        SFLogger.debugLog("SFBackgroundScanService", " onStartCommand");
        return START_STICKY;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SFLogger.debugLog("SFBackgroundScanService", "Service destroyed!");
    }
}